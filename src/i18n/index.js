import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import i18n_pt from "./pt"
import i18n_es from "./es"
import i18n_en from "./en"

// the translations
// (tip move them in a JSON file and import them)
const resources = {
  pt: {
    translation: i18n_pt
  },
  en: {
    translation: i18n_en
  },
  es: {
    translation: i18n_es
  }
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "pt",

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

  export default i18n;
