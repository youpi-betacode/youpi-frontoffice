import { createMuiTheme } from '@material-ui/core/styles';
import { colors } from './constants';

const defaultTheme = createMuiTheme();
const theme = createMuiTheme({
  palette: {
    primary: {
      main: colors.primary
    },
    background: {
      default: colors.white
    }
  },
  overrides: {
    MuiDialog: {
      "paper": {
        overflow: "visible",
        overflowY: "visible",
        overflowX: "visible"
      },
      paperWidthXs: {
        [defaultTheme.breakpoints.down('sm')]: {
          maxWidth: 270
        }
      }
    },
    MuiTypography: {
      "body1": {
        fontFamily: "ComfortaaRegular"
      },
      "root": {
        fontFamily: "ComfortaaRegular"
      },
      'h4': {
        fontFamily: "ComfortaaRegular"
      },
      "h5": {
        fontFamily: "ComfortaaRegular",
        fontSize: 18
      },
      "h6": {
        fontFamily: "ComfortaaRegular",
        fontSize: 16
      }
    },
    MuiCssBaseline: {
      "@global": {
        body: {
          color: colors.textColor,
          fontSize: 12,
          fontFamily: "ComfortaaRegular"
        }
      }
    },
    MuiFormControl:{
      root:{
        "background-color": "rgb(255, 255, 255)",
        "border-radius": "5px"
      }
    },
    MuiOutlinedInput: {
      input:{
        padding: "15px 10px"
      }
    },
    MuiButton: {
      root: {
        borderRadius: '7.5px',
        fontFamily: "ComfortaaRegular",
        
      },
      textPrimary: {
        padding: '10px 40px',
        '&:hover': {
          backgroundColor: 'rgba(0,0,0,0.8)'
        },
        color:"#fff",
        textTransform: "none",
        fontWeight: 200,
         // eslint-disable-next-line
        background: "#745334", /* Old browsers */
         // eslint-disable-next-line
        background: "-moz-linear-gradient(left,  #745334 0%, #fabc87 51%, #745334 93%, #745334 100%)", /* FF3.6-15 */
         // eslint-disable-next-line
        background: "-webkit-linear-gradient(left,  #745334 0%,#fabc87 51%,#745334 93%,#745334 100%)", /* Chrome10-25,Safari5.1-6 */
         // eslint-disable-next-line
        background: "linear-gradient(to right,  #745334 0%,#fabc87 51%,#745334 93%,#745334 100%)", /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: "progid:DXImageTransform.Microsoft.gradient( startColorstr='#745334', endColorstr='#745334',GradientType=1 )", /* IE6-9 */
      }
    },
    MuiLink: {
      root: {
        color: colors.white,
        transition: '0.3s',
        '&:hover': {
          color: colors.linkDark
        }
      }

    },
    MuiSlider: {
      root: {
        width: '300px'
      },
      rail: {
        color: 'grey'
      },
      track: {
        color: colors.primary,
        height: '3px'
      },
      thumb: {
        color: colors.primary
      }
    },
    MuiExpansionPanel: {
      root: {
        backgroundColor: colors.backgroundGrey,
        boxShadow: 'none',

        '&:before': {
          display: 'none',
        },
        '&$expanded': {
          margin: 'auto',
        },
      },
      expanded: {},
    },
    MuiExpansionPanelSummary: {
      root: {
        border: '1px solid',
        borderColor: colors.primary,
        borderRadius: '5px',
        marginTop: '5px',
        backgroundColor: colors.white,
        minHeight: 56,

        '&:not(:last-child)': {
          marginBottom: '5px',
        },
        '&$expanded': {
          minHeight: 56,
        },
      },
      content: {
        '&$expanded': {
          padding: 0,
          margin: '12px 0',
        },
      },
    },
    MuiExpansionPanelDetails: {
      root: {
        padding: 0,
        backgroundColor: colors.backgroundGrey
      }
    },
    MuiCheckbox: {
      root: {
        color: colors.primary
      },
      colorPrimary: {
        color: colors.primary
      }
    },
    MuiList: {
      root: {
        margin: 0,
        width: '100%'
      },
      padding: {
        paddingTop: '0px',
        paddingBottom: '0px'
      }
    },
    MuiFab: {
      root: {
        height: 40,
        width: 40
      }
    },
    MuiSpeedDialAction: {
      fab: {
        margin: 0
      }
    },
    MuiStepLabel: {
      active: {
        color: colors.blue
      },
      alternativeLabel: {
        color: colors.blue+" !important"
      },
    },
    MuiStepper: {
      root: {
        width: '100%'
      }
    }
  }
})


export default theme;
