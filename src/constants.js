export const colors = {
  primary: "#d29f72",
  darkerPrimary: "#b37b4b",
  lightPrimary: 'rgba(180,140,105,0.8)',
  linkDark: '#624A36',
  textColor: '#828282',
  backgroundGrey: '#f2f2f2',
  lightGrey: '#C0C0C0',
  borderGrey: '#e1e1e1',
  success: '#43a047',
  successBlue: '#32d2c4',
  errorPink: '#f27180',
  htmlGrey: '#808080',
  navbarTransparent: 'rgba(210,166,121,0.5)',

  black: '#000000',
  white: '#ffffff',
  red: '#f20900',
  blue: '#3fd2c4'
};

export const pageSize = 8;
export const defaultStore = 1;
export const defaultProductPhoto = "/images/youpii_default_gif.gif";
export const feeling_default_reference = "YFWH_1";
export const historyPageSize = 10;

// LOCAL
//export const api = 'http://127.0.0.1:5000/v1';

// PROD
export const api = 'https://api.youpii.pt/v1';
export const filterPhotosApi = 'https://api.imgfiltering.betacode.tech/v1';
export const simulationsApi = 'https://api.simulador.youpii.betacode.tech/v1';
export const fileManagerApi = 'https://api.filemanager.betacode.tech/v1';
export const sharePath = 'https://store.youpii.pt/simulator/';

// DEV
// export const api = 'http://dev.api.youpii.betacode.tech/v1';
// export const filterPhotosApi = 'http://dev.api.imgfiltering.youpii.betacode.tech/v1';
// export const simulationsApi = 'http://dev.api.simulador.youpii.betacode.tech/v1';
// export const fileManagerApi = 'http://dev.api.filemanager.youpii.betacode.tech/v1';
// export const sharePath = 'http://dev.store.youpii.betacode.tech/simulator/';

// Regular expressions
export const zipCodeRegex = /^[0-9]{4}[-][0-9]{3}$/;
export const nifRegex = /^[0-9]{9}$/;

// Shipping fee info
export const PAID_DELIVERY = 'YSECASA';
export const FREE_DELIVERY = 'YSECASAGRATUITO';
