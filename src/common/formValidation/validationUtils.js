import * as types from '../../redux/Types';

export const matchPasswords = (pw1,pw2) => {
  return function(dispatch) {
    dispatch({
      type: types.TOGGLE_CONFIRM_PASSWORD_ERROR,
      confirmPasswordError: pw1 === pw2 ? false : true
    })
  }
} 

export const checkPasswordLength = (pw) => {
  return function(dispatch) {
    dispatch({
      type: types.TOGGLE_PASSWORD_ERROR,
      passwordError: pw.length > 0 ? false : true
    })
  }
}

// function matchPasswords(pw1,pw2) {
//   return pw1 === pw2;
// }

// function checkPasswordLength(pw1) {
//   return pw1.length > 0;
// }

function checkEmail(email) {
  const emailRegExp = /\S+@\S+\.\S+/;

  return emailRegExp.test(email);
}

function checkPhone(phone) {
  const phoneRegExp = /^\d+$/;

  return phoneRegExp.test(phone);
}

function checkNif(nif) {
  const nifRegExp = /^\d{9}$/;

  return nifRegExp.test(nif);
}