import React from 'react';
import { Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import { colors } from '../constants';

function PageContainer(props) {
  const classes = useStyles();
  return (
    <Container className={classes.container}>
      {props.children}
    </Container>
  )
}

export default PageContainer;

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 50,
    marginBottom: 50,
    padding: '50px 10px',
    borderRadius: 10,
    border: '2px solid ',
    borderColor: colors.borderGrey,
    minHeight:500
  }
}))
