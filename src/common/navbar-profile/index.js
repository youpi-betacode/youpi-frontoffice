import React from 'react'
import { Grid, ListItem, ListItemText } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { push } from 'connected-react-router';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import 'sweetalert2/dist/sweetalert2.css'
import { logout } from '../../redux/actions/LoginActions';
import { colors } from '../../constants';
import Tooltip from "@material-ui/core/Tooltip";
import {animateScroll as scroll} from "react-scroll/modules";
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import useTheme from "@material-ui/core/styles/useTheme";

function createLink(label, path, icon, disabled=false) {
  return { label, path, icon, disabled};
}

function NavbarProfile() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const pathname = useSelector(state => state.router.location.pathname);
  const { t } = useTranslation();
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));

  const links = [
    createLink(t('your-data'), '/user', '/images/icons/user.png'),
    createLink('YOUPi! Coins', '/user/youpiCoins', '/images/icons/youpiCoins.png', true),
    createLink('Encomendas', '/user/history', '/images/icons/mrcool.jpg'),
    createLink(t('simulations'), '/user/simulations', '/images/icons/simulations.png'),
    createLink('Moradas(s)', '/user/addresses', '/images/icons/addresses.png', true),
    createLink('Wishlist', '/user/wishlist', '/images/icons/wishlist.png', true),
    createLink(t('logout'), '/logout', '/images/icons/mrsad.jpg')
  ];

  const handleOnClick = (path) => {
    if(path === '/logout'){
      Swal.fire({
        imageUrl: '/images/icons/mrsad.jpg',
        imageWidth: 60,
        imageHeight: 60,
        title: t('logout-prompt-title'),
        text: t('logout-prompt-subtitle'),

        showCancelButton: true,
        customClass: {
          confirmButton: 'youpii-confirm-button-class',
          cancelButton: 'youpii-confirm-button-class',
        },
      }).then((result) => {
        if(result.value){
          dispatch(logout());
          dispatch(push("/"));
        }
      });
    } else{
      dispatch(push(path));
      if(isSmallDevice){
        scroll.scrollTo(1150);
      }
    }

  };

  return (
    <Grid container>
      {links.map((link, index) =>
        <Grid item xs={12} key={index}>
          <ListItem button onClick={() => !link.disabled?handleOnClick(link.path):null} className={classes.item}>
            <img src={link.icon} width='40' height='40' alt='icon'/>
            {!link.disabled &&  <ListItemText primary={link.label} primaryTypographyProps={{classes: {root: classes.bold}}} className={pathname === link.path ? classes.activeLink : classes.links}/>}
            {link.disabled &&
            <Tooltip title={t('not-implemented')} aria-label={t('not-implemented')}>
              <ListItemText style={{color:"rgba(130,130,130, 0.5)"}} primary={link.label} primaryTypographyProps={{classes: {root: classes.bold}}} className={pathname === link.path ? classes.activeLink : classes.links}/>
            </Tooltip>}
            </ListItem>
        </Grid>
      )}
    </Grid>
  )
}

export default NavbarProfile;

const useStyles = makeStyles(theme => ({
  links: {
    textDecoration: 'none',
    color: colors.primary,
    fontSize: 18,
    paddingLeft: 15,
    '&:hover':{
      backgroundColor: 'transparent'
    }
  },
  activeLink: {
    textDecoration: 'none',
    color: colors.black,
    fontSize: 18,
    paddingLeft: 15,
    '&:hover':{
      backgroundColor: 'transparent'
    }
  },
  item: {
    '&:hover':{
      backgroundColor: 'transparent'
    }
  },
  bold: {
    fontWeight: 'bold'
  }
}))
