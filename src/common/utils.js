import {enqueueSnackbar} from "../redux/actions/NotificationsActions";
import { useTranslation } from "react-i18next";
var numeral = require('numeral')

export const convertBlobToBase64 = blob => new Promise((resolve, reject) => {
  const reader = new FileReader;
  reader.onerror = reject;
  reader.onload = () => {
    resolve(reader.result);
  };
  reader.readAsDataURL(blob);
});

export const convertURLToBase64 = url => new Promise((resolve, reject) => {
  let img = new Image();
  img.crossOrigin = 'Anonymous';
  img.onload = () => {
    let canvas = document.createElement('canvas'),
      ctx = canvas.getContext('2d');

    canvas.height = img.naturalHeight;
    canvas.width = img.naturalWidth;
    ctx.drawImage(img, 0, 0);

    // Unfortunately, we cannot keep the original image type, so all images will be converted to PNG
    // For this reason, we cannot get the original Base64 string
    const base64 = canvas.toDataURL('image/png');

    resolve(base64);
  };
  img.src = url;
});

export const getImageSize = (url) => new Promise((resolve, reject) => {
  let img = new Image();
  img.onload = function () {
    resolve({width: this.width, height: this.height});
  };
  img.src = url;
});


export const handleHTTPError = (error, message, key=null) => {
  // const message = error &&
  // error.response &&
  // error.response.data &&
  // error.response.data.message ? error.response.data.message : default_message;
  const notification = {
      message: message,
      options: {
        variant: 'error',
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
        },
        autoHideDuration: 3000
      },
    };
  return enqueueSnackbar(
    notification,
    new Date().toString()
  );
};

export const handleHTTPSuccess = (message, key=null) => {
  const notification = {
    message: message,
    options: {
      variant: 'success',
      anchorOrigin: {
        vertical: 'bottom',
        horizontal: 'right',
      },
      autoHideDuration: 3000
    },
  };
return enqueueSnackbar(
  notification,
  key
);
};

export const translation = (text) => {
  return function(){
    useTranslation(text)
  }
}

export const formatNumber = (number) => {
   return numeral(number).format('0.00');
}
