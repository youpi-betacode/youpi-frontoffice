import React from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { colors } from '../../constants';

export default function ShareButton({children}) {
  const classes = useStyles();  

  return (
    <Button variant="outlined" className={classes.root}>
        {children}              
    </Button>
  );
}


const useStyles = makeStyles(theme => ({
  root: {
    borderRadius: 3,
    borderWidth: 2,
    paddingTop: 9,
    borderColor: colors.primary,
    color: 'black',
    height: 48,
    textAlign: 'center'
  }
}));