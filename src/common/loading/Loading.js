import React from 'react';
import { CircularProgress } from '@material-ui/core';
import { useSelector } from 'react-redux';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import './loadingStyle.css';

function Loading(props) {

  const subLoadings = useSelector(state => state.loading.subLoadings);
  const loading = useSelector(state => state.loading.loading);
  let isLoading = false;
  if(props.loading){
    isLoading = subLoadings[props.loading];
  }else{
    isLoading = loading;
  }


  return (
    <BlockUi blocking={isLoading} loader={<CircularProgress/>} zzmessage={props.message}>
      {props.children}
    </BlockUi>
  )
}

export default Loading;
