import React from 'react';
import {Icon, makeStyles} from "@material-ui/core";
import Fab from "@material-ui/core/Fab";

export default function CloseModalButton({handleClose}) {
  const classes = useStyles();

  return (
    <Fab aria-label="close" className={classes.close} onClick={handleClose}>
      <Icon>close</Icon>
    </Fab>
  )
}

const useStyles = makeStyles(theme => ({
  close: {
    margin: theme.spacing(1),
    position: "absolute",
    width: 36,
    height: 36,
    top: -26,
    right: -26,
  }
}));
