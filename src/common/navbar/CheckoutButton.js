import React, {useEffect} from 'react';
import {push} from "connected-react-router";
import clsx from "clsx";
import {Badge, makeStyles} from "@material-ui/core";
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import {useDispatch, useSelector} from "react-redux";
import Sticky from "react-stickynode";
import {colors} from "../../constants";

export default function CheckoutButton() {
    const classes = useStyles();
    const dispatch = useDispatch();

    const shoppingCart = useSelector(state => state.shoppingCart.cart);
    const router = useSelector(state => state.router.location.pathname);

    const isCheckout = router.startsWith("/checkout");

    if (shoppingCart && shoppingCart.total_products > 0) {
        return (
            <a onClick={() => dispatch(push('/checkout/summary'))}
               className={clsx(classes.navBarButton, isCheckout ? classes.navBarButtonActive : "")}>
                <Badge badgeContent={shoppingCart.total_products-1} color="primary"
                       className={clsx("animated infinite tada delay-2s", classes.badge)}>
                    <ShoppingCartIcon/>
                </Badge>
            </a>
        )
    } else {
        return (
            <a onClick={() => dispatch(push('/checkout/summary'))}
               className={clsx(classes.navBarButton, isCheckout ? classes.navBarButtonActive : "")}>
                <Badge badgeContent={0} color="primary"
                       className={classes.badge}>
                    <ShoppingCartIcon/>
                </Badge>
            </a>
        )
    }
}

const useStyles = makeStyles(theme => ({
    badge: {
        marginTop: "20px"
    },
    navBarButton: {
        height: 65,
        float: "right",
        width: 60,
        textAlign: "center",
        marginRight: 10,
        color: `${colors.white} !important`,
        cursor: "pointer",
        transition: "all 0.2 ease",
        '&:hover': {
            background: "#b28a67"
        }
    },
    navBarButtonActive: {
        background: "#b28a67"
    }
}));
