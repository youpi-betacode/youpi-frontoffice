import React, { useEffect } from 'react';
import { makeStyles, createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Grid, Typography, ListItem, ListItemText, List, Badge } from "@material-ui/core";
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { colors } from "../../constants";
import IconButton from '@material-ui/core/IconButton';
import { Icon } from "@material-ui/core";
import Drawer from '@material-ui/core/Drawer';
import { useTranslation } from "react-i18next";
import { useSelector, useDispatch } from 'react-redux';
import { push } from 'connected-react-router';
import CheckoutButton from "./CheckoutButton";
import navigationLinks from './navigation-links';
import { getUserInfo } from '../../redux/actions/UserActions';

function ListItemLink(props) {
   return <ListItem button component="a" {...props} />;
}

function NavBarLinks({ navigationLinks }) {
   const { t } = useTranslation();
   const classes = useStyles();

   return (
      <React.Fragment>
         {
            navigationLinks.map((item, index) => {
               return (
                  <ListItemLink href={item.url} className={classes.link} key={index}>
                     <ListItemText classes={{ primary: classes.linkText }} primary={t(item.label)} />
                  </ListItemLink>
               )
            })
         }
      </React.Fragment>
   )
}

function DrawerMenu({ toggleDrawer }) {
   const classes = useStyles();
   const costumer = useSelector(state => state.user);
   const dispatch = useDispatch();

   const shoppingCart = useSelector(state => state.shoppingCart);
   var token = localStorage.getItem('token');

   const handleUserIconClick = () => {
      token ? dispatch(push('/user')) : dispatch(push('/login'));
      dispatch(toggleDrawer('right', false));
   }

   const handleShoppinCartClick = () => {
      dispatch(push('/checkout/summary'));
      dispatch(toggleDrawer('right', false));
   }

   useEffect(() => {
      dispatch(getUserInfo())
   }, [])

   return (
      <div className={classes.drawerContainer}>
         <MuiThemeProvider theme={theme}>
            <IconButton edge="start" style={{ float: "right", zIndex: 2 }} className={classes.menuButton} color="inherit" aria-label="menu"
               onClick={toggleDrawer('right', false)}>
               <Icon style={{ fontSize: 30 }}>close</Icon>
            </IconButton>
            <Grid container direction='row' justify='space-between' alignItems='center' spacing={1} className={classes.userContainer}>
               <Grid item xs={12} style={{ textAlign: 'center' }} onClick={handleUserIconClick}>
                  <img src='/images/icons/login.png' width='35' height='35' alt='Cliente' />
               </Grid>
               {token && <Grid item xs={12} style={{ textAlign: 'center' }}>
                  <Typography variant='h6' className={classes.linkText}>
                     <b>{costumer.firstName} {costumer.lastName}</b>
                  </Typography>
               </Grid>}
               {/* <Grid item xs={12} style={{ textAlign: 'center' }} onClick={handleShoppinCartClick}>
            <Badge badgeContent={shoppingCart.total_amount} color='primary' style={{ color: colors.white }}>
              <ShoppingCartIcon />
            </Badge>
          </Grid> */}
            </Grid>
            <List component="nav" aria-label="secondary mailbox folders">
               <NavBarLinks navigationLinks={navigationLinks.left} />
               <NavBarLinks navigationLinks={navigationLinks.right} />
            </List>
         </MuiThemeProvider>
      </div>
   )
}

export default function NavNarResponsive() {
   const classes = useStyles();
   const [state, setState] = React.useState({
      right: false
   });

   const toggleDrawer = (side, open) => event => {
      if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
         return;
      }
      setState({ ...state, [side]: open });
   };

   return (
      <React.Fragment>
         <Grid container direction="row" justify="space-between" alignItems="center" id="headerNav"
            className={classes.navbar}>
            {/* <Grid item xs={3} className={classes.navCompoment}>
        </Grid> */}
            <Grid item xs={10} className={classes.title}>
               <img src={'/images/logo_youpi2.png'} alt='YOUPI' className={classes.image}></img>
            </Grid>
            <Grid item xs={1}>
               <CheckoutButton />
            </Grid>
            <Grid item xs={1} className={classes.navCompoment}>
               <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu"
                  onClick={toggleDrawer('right', true)}>
                  <Icon style={{ fontSize: 30 }}>menu</Icon>
               </IconButton>
            </Grid>
         </Grid>
         <Drawer anchor="right" open={state.right} onClose={toggleDrawer('right', false)}>
            <DrawerMenu toggleDrawer={toggleDrawer} />
         </Drawer>
      </React.Fragment>
   );
}

const theme = createMuiTheme({
   overrides: {
      MuiSvgIcon: {
         root: {
            height: '35px !important',
            width: '35px !important'
         }
      }
   }
});

const useStyles = makeStyles(theme => ({
   drawerContainer: {
      width: 250,
      height: "100%",
      backgroundImage: "url('/images/fundo_header.jpg')"
   },
   menuButton: {
      color: colors.white,
   },
   image: {
      width: 180,
      transition: "all 0.3s"
   },
   navbar: {
      backgroundRepeat: "no-repeat",
      backgroundImage: "url('/images/fundo_header.jpg')",
      backgroundSize: "cover",
      opacity: .97,
      visibility: "inherit",
      zIndex: 20,
      height: 80
   },
   title: {
      textAlign: 'left',
      paddingLeft: 20
   },
   navCompoment: {
      textAlign: "center"
   },
   leftNav: {
      flex: 1,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      marginRight: '20px'
   },
   rightNav: {
      flex: 1,
      display: 'flex',
      marginLeft: '20px'
   },
   linkText: {
      textTransform: "uppercase",
      color: colors.white,
      fontSize: 13,
   },
   link: {
      paddingLeft: 20,
      paddingRight: 20,
      borderTop: "1px solid rgba(255, 255, 255, 0.03)"
   },
   userContainer: {
      cursor: 'pointer',
      marginBottom: 10
   }
}));
