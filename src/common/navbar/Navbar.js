import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import useWindowSize from '../WindowSize';
import Sticky from 'react-stickynode';
import Hidden from "@material-ui/core/Hidden";
import navigationLinks from './navigation-links';
import { useTranslation } from 'react-i18next';
import { colors } from "../../constants";
import NavBarResponsive from "./NavBarResponsive";
import clsx from 'clsx'
import {push} from "connected-react-router";
import { Badge } from '@material-ui/core';
import 'animate.css/animate.min.css';
import CheckoutButton from "./CheckoutButton";

const pathnames = ['/user', '/simulations']

export function LinkBar({ navigationLinks }) {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <React.Fragment>
      {navigationLinks.map((item, index) =>
        <a href={item.url} className={classes.link} key={index}>
          {t(item.label)}
        </a>
      )}
    </React.Fragment>
  );
}


export default function Navbar() {

  const dispatch = useDispatch();
  const size = useWindowSize();

  const [stickyStatus, setStickyStatus] = React.useState(false);

  const token = typeof localStorage !== "undefined"?localStorage.getItem('token'): null;
  const classes = useStyles(stickyStatus);
  const router = useSelector(state => state.router.location.pathname);

  const isProfile = router.startsWith("/user");
  const isCheckout = router.startsWith("/checkout");

  const handleStateChange = (status) => {
    setStickyStatus(status.status);
  };

  return (
    <React.Fragment>
      <Hidden mdUp>
        <NavBarResponsive />
      </Hidden>
      <Hidden smDown>
        <Sticky enabled={true} bottomBoundary={1200} onStateChange={handleStateChange} innerZ="6">
          <Grid container direction='row' justify='space-evenly' alignItems='center' className={classes.navbar}>
            <Grid item md={5} className={classes.navCompoment}>
              <LinkBar navigationLinks={navigationLinks.left} />
            </Grid>
            <Grid item md={2} className={classes.navCompoment}>
              <img src={'/images/logo_youpi2.png'} alt='YOUPI' className={classes.image}></img>
            </Grid>
            <Grid item md={3} className={classes.navCompoment}>
              <LinkBar navigationLinks={navigationLinks.right} />
            </Grid>
            <Grid item md={2} className={classes.navComponent}>
              <CheckoutButton/>
              <a onClick={() => dispatch(push(token ? '/user' : '/user/login'))} className={clsx(classes.navBarButton,isProfile?classes.navBarButtonActive:"")} >
                <img alt="profile icon" src='/images/icons/login.png' width='25' height='25' style={{marginTop: 20}}/>
              </a>
            </Grid>
          </Grid>
        </Sticky>
      </Hidden>
    </React.Fragment>
  );
}

const useStyles = makeStyles(theme => ({
  badge: {
    marginTop: "20px"
  },
  image: {
    width: stickyStatus => (stickyStatus === Sticky.STATUS_FIXED) ? 106 : 180,
    transition: "all 0.3s"
  },
  navbar: {
    backgroundRepeat: "no-repeat",
    backgroundImage: "url('/images/fundo_header.jpg')",
    backgroundSize: "cover",
    backgroundPosition: "center top",
    opacity: .97,
    visibility: "inherit",
    zIndex: 20,
    height: 66,
  },
  navCompoment: {
    textAlign: "center"
  },
  leftNav: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginRight: '20px'
  },
  rightNav: {
    flex: 1,
    display: 'flex',
    marginLeft: '20px'
  },
  navBarButton:{
    height: 65,
    float: "right",
    width: 60,
    textAlign: "center",
    marginRight: 10,
    color: `${colors.white} !important`,
    cursor:"pointer",
    transition: "all 0.2 ease",
    '&:hover': {
      background: "#b28a67"
    }
  },
  navBarButtonActive:{
    background: "#b28a67"
  },
  link: {
    marginLeft: '15px',
    marginRight: '15px',
    fontSize: 10,
    textTransform: "uppercase",
    cursor: "pointer",
    textDecoration: "none",
    color: colors.white,
    transition: "all ease 0.3s",
    '&:hover': {
      textDecoration: "none",
      color: colors.linkDark,
    }
  },
  linkIcon: {
    marginLeft: '15px',
    marginRight: '15px',
    fontSize: 10,
    cursor: "pointer",
    textDecoration: "none",
    color: colors.white,
    transition: "all ease 0.3s",
    '&:hover': {
      textDecoration: "none",
      color: colors.linkDark,
    }
  },
  linkIconSelected: {
    marginLeft: '15px',
    marginRight: '15px',
    fontSize: 10,
    cursor: "pointer",
    textDecoration: "none",
    color: colors.successBlue,
    transition: "all ease 0.3s",
    '&:hover': {
      textDecoration: "none",
      color: colors.linkDark,
    }
  }
}));
