import React from 'react';
import { Modal } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';

import RegisterModalContent from './registerModalContent';
import { toggleModal } from '../../redux/actions/RegisterActions';

function RegisterModal() {
  const dispatch = useDispatch();
  const modal = useSelector(state => state.register.modal);
  const ref = React.createRef();

  const handleOnClose = () => {
    dispatch(toggleModal(false));
  };

  return (
    <Modal open={modal} onClose={handleOnClose}>
      <RegisterModalContent ref={ref}/>
    </Modal>
  )
}

export default RegisterModal;
