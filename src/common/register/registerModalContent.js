import React from 'react';
import { Grid } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/styles';

import Form from '../../pages/register-depracated/components/textInputs';
import { colors } from './../../constants';

const RegisterModalContent = React.forwardRef((props, ref) => {
  const dispatch = useDispatch();
  const classes = useStyles();

  return (
    <div className={classes.content}>
      <Form hide={true} />
    </div>
  )
})

export default RegisterModalContent;

const useStyles = makeStyles(theme => ({
  content: {
    //height: '20%',
    backgroundColor: colors.white,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 4),
    outline: 'none',
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    textAlign: 'center'
  }
}))
