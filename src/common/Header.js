import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import { colors } from '../constants';

function Header(props) {
  const classes = useStyles();
  return(
    <div>
      <div className={classes.headerBody}>
          <Typography variant='h5' component="h5" className={classes.title}>{props.title}</Typography>
          <Typography variant='h6' component="h6" className={classes.subTitle}>{props.subTitle}</Typography>
      </div>
    </div>
  )
}

export default Header;

const useStyles = makeStyles(theme => ({
  headerBody: {
    textAlign: 'center',
    backgroundColor: colors.backgroundGrey,
    padding: "40px 20px 40px 20px",
  },
  title: {
    width:"100%",
    marginBottom: 0,
    color: colors.primary,
    fontWeight:"bolder"
  },
  subTitle: {
    width:"100%",
    marginTop: 5,
    color: colors.linkDark,
    fontSize:14
  }
}));
