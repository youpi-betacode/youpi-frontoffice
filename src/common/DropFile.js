import React from 'react';
import Dropzone from 'react-dropzone';
import {Icon} from '@material-ui/core';
import {colors} from '../constants';
import CircularProgress from '@material-ui/core/CircularProgress';
import {useTranslation} from 'react-i18next';
import Fab from '@material-ui/core/Fab';

import {makeStyles} from '@material-ui/core/styles';

function DropFile({handleOnDrop, handleDropReject, maxSize = 25000000, accept = "image/*"}) {

  const [isLoading, setIsLoading] = React.useState(false);
  const classes = useStyles();
  const {t} = useTranslation();

  const onDragEnter = () => {
    setIsLoading(true);
  };

  const onDrop = (files, rejectedFiles, e) => {
    setIsLoading(true);
    handleOnDrop(files, rejectedFiles, e)
  };

  const onDropReject = (files, e) => {
    setIsLoading(false);
    handleDropReject(files, e);
  };

  return (
    <div className={classes.paper}>
      <Dropzone onDragEnter={onDragEnter} onDrop={onDrop} accept={accept} maxSize={maxSize}
                onDropRejected={onDropReject}>
        {({getRootProps, getInputProps}) => (
          <div className={classes.upload}>
            <div {...getRootProps()} className={classes.upload}>
              <div className={classes.aligner}>
                <input {...getInputProps()} />
                {!isLoading && <img src="/images/cloud.png" style={{width:130}} />}
                {isLoading && <CircularProgress className={classes.progress}/>}
                <p>{t('dropfile')}</p>
              </div>
            </div>
          </div>
        )}
      </Dropzone>
    </div>

  )
}

export default DropFile;

const useStyles = makeStyles(theme => ({
  progress: {
    margin: theme.spacing(2),
  },
  icon: {
    color: colors.primary,
    fontSize: 60
  },
  paper: {
    cursor: "pointer",
    maxWidth: 600,
    maxHeight: 400,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  upload: {
    maxWidth: 500,
    maxHeight: 300,
    outline: "none"
  },
  aligner: {
    width: '500px',
    height: '300px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  }
}));
