import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Snackbar, Icon } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';

import { toggleSuccessSimulationUpload } from '../redux/actions/SimulationActions';
import { colors } from '../constants';

function Snackbars() {
  const dispatch = useDispatch();
  const styles = useStyles();
  const successSave = useSelector(state => state.simulation.successSave);

  const handleCloseSuccessSimSave = () => {
    dispatch(toggleSuccessSimulationUpload(false));
  }

  return (
    <React.Fragment>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={successSave}
        autoHideDuration={6000}
        onClose={handleCloseSuccessSimSave}
        message={<span>Guardamos o seu YOUPi! quando a sua àrea pessoal estiver disponivel poderá consultar os seus YOUPis</span>}
        action={[
          <Icon style={{ cursor: 'pointer' }} onClick={handleCloseSuccessSimSave}>close</Icon>
        ]}
        ContentProps={{
          classes: {
            root: styles.successSnack
          }
        }}
      />
    </React.Fragment>
  )
}

export default Snackbars;

const useStyles = makeStyles(theme => ({
  successSnack: {
    backgroundColor: colors.success + '!important',
    fontSize: 16
  }
}));
