import React from 'react';
import FooterLinks from "./FooterLinks"
import FooterNewsletter from "./FooterNewsletter"
import Hidden from "@material-ui/core/Hidden";

export default function Footer() {

  return (
    <React.Fragment>
        <FooterLinks/>
        <FooterNewsletter />
    </React.Fragment>
  );
}
