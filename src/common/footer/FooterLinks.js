import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {colors} from '../../constants';

import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Grid from "@material-ui/core/Grid";
import clsx from 'clsx';
import { useTranslation } from 'react-i18next';

function SocialMedia(){
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <div className={classes.marginBottom}>
      <Typography variant="h6" gutterBottom className={classes.title}>
        {t('title-social-media')}
      </Typography>
      <p className={classes.text}>
         {t('footer-text7')}
      </p>
      <p className={clsx(classes.text, classes.marginBottom)}>
        {t('footer-text8')}
      </p>
      <p className={classes.text}>
        {t('footer-text9')}
      </p>
      <p  className={classes.text}>
        {t('footer-text10')}
      </p>
      <p className={clsx(classes.text, classes.marginBottom)}>
         {t('footer-text11')}
      </p>
      <div>
        <img src={'/images/social/youtube.png'} onClick={() => window.location.href="https://www.youtube.com/channel/UCewpBrwi9zHXSPnQGyWxtqQ"} alt="youtube" className={classes.socialIcon}></img>
        <img src={'/images/social/facebook.png'} onClick={() => window.location.href="https://www.facebook.com/youpii.oficial"} alt="facebook" className={classes.socialIcon}></img>
        <img src={'/images/social/instagram.png'} onClick={() => window.location.href="https://www.instagram.com/youpii.oficial/"} alt="instagram" className={classes.socialIcon}></img>
        <img src={'/images/social/pinterest.png'} onClick={() => window.location.href="https://www.pinterest.pt/youpii_oficial/"} alt="pinterest" className={classes.socialIcon}></img>
      </div>
    </div>
  )
}

function Policy(){
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <div className={classes.marginBottom}>
      <Typography variant="h6" gutterBottom className={classes.title}>
        {t('title-policy')}
      </Typography>
      <p className={classes.text}>
         <Link
            href="https://youpii.pt/termos-condicoes/"
            component="a"
            underline="always"
            className={classes.link}
         >
         {t('terms-and-conditions')}
         </Link>
      </p>
      <p className={classes.text}>
         <Link
            href="https://youpii.pt/politica-privacidade/"
            component="a"
            underline="always"
            className={classes.link}
         >
         {t('privacy-policy')}
         </Link>
      </p>
    </div>
  )
}

function Contacts(){
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <div className={classes.marginBottom}>
      <Typography variant="h6" gutterBottom className={classes.title} >
        {t('title-contacts')}
      </Typography>
      <p className={classes.text} >
        {t('footer-text5')} 
         <Link
            component="a"
            underline="always"
            className={classes.link}
         >
          iamayouper@youpii.pt
         </Link>
      </p>
      <p className={classes.text}>
        {t('footer-text6')}
      </p>
    </div>
  )
}

function WhoWeAre(){
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <div className={classes.marginBottom} style={{
        marginBottom:20
    }}>
      <Typography variant="h6" gutterBottom className={classes.title} style={{
          color:colors.white,
          textTransform:"uppercase",
          fontWeight:700
      }}>
        {t('title-who-we-are')}
      </Typography>
      <p className={classes.text}>
         <Link
            href="https://youpii.pt/quem-somos/"
            component="a"
            underline="always"
            className={classes.link}
         >
          {t('footer-text1')}
         </Link>
      </p>
    </div>
  )
}

function ClientSupport(){
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <div className={classes.marginBottom}>
      <Typography variant="h6" gutterBottom className={classes.title} >
        {t('title-costumer-support')}
      </Typography>
       <p className={clsx(classes.text, classes.marginBottom)} >
         <Link
            href="https://youpii.pt/perguntas-frequentes/"
            component="a"
            underline="always"
            className={classes.link}
         >
          {t('footer-text2')}
         </Link>
       </p>
      <p className={classes.text} >
        {t('footer-text3')}
      </p>
      <p className={classes.text} >
        {t('footer-text4')}
      </p>
    </div>
  )
}

export default function FooterLinks() {
  const classes = useStyles();

  return (
    <div className={classes.footer} >
      <div className={classes.maxWidth} >
        <Grid container className={classes.container} >
          <Grid item xs={12} md={4}>
            <WhoWeAre />
            <ClientSupport/>
          </Grid>
          <Grid item xs={12} md={4}>
            <Contacts />
            <Policy />
          </Grid>
          <Grid item xs={12} md={4}>
            <SocialMedia />
          </Grid>
        </Grid>
      </div>
    </div>
  );
}


const useStyles = makeStyles(theme => ({
  container:{
    padding: "0px 40px 20px 40px"
  },
  maxWidth:{
    maxWidth: 1080,
    margin: "auto"
  },
  socialIcon:{
    cursor: 'pointer',
    marginRight:5
  },
  link:{

  },
  text:{
    color:colors.white,
    margin:0
  },
  marginBottom:{
    marginBottom:20
  },
  title:{
    color:colors.white,
    textTransform:"uppercase",
    fontWeight:700
  },
  footer: {
    backgroundImage: "url('/images/fundo_bloco_de_texto.jpg')",
    backgroundRepeat: "repeat",
    backgroundPosition: "center",
    backgroundSize: "cover",
    paddingTop:30,
    paddingBottom:30,
  }
}));
