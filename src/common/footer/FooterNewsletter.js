import React from 'react';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {makeStyles} from "@material-ui/core";
import {colors} from "../../constants";
import Link from "@material-ui/core/Link";
import { useTranslation } from 'react-i18next';
import clsx from "clsx";

function SubscriptForm() {

  return (
    <Grid container>
      <Grid item xs={12} md={10}>
        <TextField
          id="outlined-full-width"
          label="E-mail"
          style={{margin: 8}}
          fullWidth
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            shrink: true,
          }}
        />
      </Grid>
      <Grid item xs={12} md={2}>

      </Grid>
    </Grid>
  )

}

function CopyRight() {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <p className={clsx(classes.text, classes.alignCenterOnMobile)}>
      {t('copyright')}
    </p>
  );
}

function Languages() {
  const classes = useStyles();
  const { i18n } = useTranslation();

  return (
    <div className={clsx(classes.floatRight, classes.alignCenterOnMobile)}>
      <Link
        onClick={() => i18n.changeLanguage('pt')}
            component="a"
            className={classes.link}
         >
        <img src={'/images/flags/pt.png'} alt="pt"></img> PT
      </Link>
      <Link
          onClick={() => i18n.changeLanguage('en')}
            component="a"
            className={classes.link}
         >
        <img src={'/images/flags/en.png'} alt="en"></img> EN
      </Link>
      <Link
        onClick={() => i18n.changeLanguage('es')}
            component="a"
            className={classes.link}
         >
        <img src={'/images/flags/es.png'} alt="es"></img> ES
      </Link>
    </div>
  )
}

export default function FooterNewsletter() {

  const classes = useStyles();

  return (
    <div style={{padding: "0px 40px 0px 40px"}}>
      <div className={classes.container}>
        <Grid container>
          <Grid item xs={12} md={4}>

          </Grid>
          <Grid item xs={12} md={4}>
            {/*<SubscriptForm/>*/}
          </Grid>
          <Grid item xs={12} md={4}>
            <img src={'/images/logoPortugal2020.jpg'} alt="portugal2020"></img>
          </Grid>
        </Grid>
      </div>
      <div className={classes.container}>
        <Grid container style={{"flexGrow": 1,}}>
          <Grid item xs={12} md={8}>
            <CopyRight/>
          </Grid>
          <Grid item xs={12} md={4}>
            <Languages />
          </Grid>
        </Grid>
      </div>
    </div>
  )
}

const useStyles = makeStyles(theme => ({
  link:{
    color:colors.primary,
    margin:5,
    cursor:"pointer"
  },
  container: {
    marginTop: 40,
    marginBottom: 40,
    maxWidth: 1080,
    margin: "auto"
  },
  text: {
    color: colors.primary
  },
  alignCenterOnMobile:{
    [theme.breakpoints.down('sm')]: {
      textAlign:"center",
      marginRight:"auto",
      marginLeft:"auto"
    }
  },
  floatRight:{
    float:"right",
    [theme.breakpoints.down('sm')]: {
      float:"none"
    }
  }
}));
