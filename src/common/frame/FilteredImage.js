import CircularProgress from "@material-ui/core/CircularProgress";
import BlockUi from "react-block-ui";
import React from "react";
import {setProcessed} from "../../redux/actions/SimulatorActions";
import {useDispatch, useSelector} from "react-redux";
import {makeStyles} from "@material-ui/core";
import {colors} from "../../constants";
import clsx from 'clsx';

export default function FilteredImage({scale=1, size=null, sizeMargin=null, scaleWhiteMargin=1, noWhiteSpace=false, shadowBellow=false, boxShadow=true, image=null, profile=false}) {

  // If size is hardcoded change scaleFactor
  const scaleFactor = size?size-size*0.235474:250;
  const classes = useStyles({scale, scaleWhiteMargin, scaleFactor});

  const marginSpace = sizeMargin?sizeMargin:(scaleWhiteMargin*22);

  const processing = useSelector(state => state.simulator.processing);
  const whiteSpace = useSelector(state => state.simulator.whiteSpace);
  const croppedCanvas = useSelector(state => state.simulator.croppedCanvas);
  const filteredPhotoResized = useSelector(state => state.simulator.filteredPhotoResized);

  let width, height;

  if(profile){
    width = !noWhiteSpace ? scale * scaleFactor-marginSpace: scale * scaleFactor;
    height = !noWhiteSpace ? scale * scaleFactor-marginSpace: scale * scaleFactor;
  }
  else{
    width = whiteSpace && !noWhiteSpace? scale * scaleFactor-marginSpace: scale * scaleFactor;
    height = whiteSpace && !noWhiteSpace? scale * scaleFactor-marginSpace: scale * scaleFactor;
  }

  // User image prop if image != null
  const canvasImage = image ? image:filteredPhotoResized ? filteredPhotoResized: croppedCanvas;

  const divStyle = clsx(boxShadow?classes.shadow:'', whiteSpace && !noWhiteSpace ? classes.whiteMargin : classes.noWhiteMargin);

  return (
    <BlockUi tag="div" blocking={processing} loader={<CircularProgress className={classes.progress}/>}>
      <div className={divStyle}>
        <img src={canvasImage} className={classes.image} width={width} height={height} />

      </div>
      {shadowBellow &&
        <img src='/images/shadow_feeling.png' width={scale*300}/>
      }
    </BlockUi>
  )
}

const useStyles = makeStyles(theme => ({
  progress: {
    margin: theme.spacing(2),
  },
  image: {
    transition: "all 0.3s",
    "-webkit-user-drag": "none",
  },
  shadow:{
    "-webkit-box-shadow": "0px 0px 40px -10px rgba(0,0,0,0.75)"
  },
  noWhiteMargin:{
    width: ({scale, scaleFactor}) => scale * scaleFactor,
    height: ({scale, scaleFactor}) => scale * scaleFactor,
    margin: "auto",
    transition: "ll 0.3s",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    verticalAlign: "middle",
    justifyContent: "center",
    background: colors.white,
  },
  whiteMargin:{
    margin: "auto",
    width: ({scale, scaleFactor})  => scale * scaleFactor,
    height: ({scale, scaleFactor}) => scale * scaleFactor,
    background: colors.white,
    transition: "ll 0.3s",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    verticalAlign: "middle",
    justifyContent: "center"
  }
}));
