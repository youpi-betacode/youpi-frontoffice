import {useSelector} from "react-redux";
import React from "react";
import {makeStyles} from "@material-ui/core";
import FilteredImage from "./FilteredImage";
import {useTheme} from '@material-ui/core/styles';
import frameSizes from './frame-sizes'
import frameSizesMobile from './frame-sizes-mobile'
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";

export default function FrameCanvas({scale = 1, size = null, distance = null, scaleWhiteMargin = 1, shadowBellow = false, pattern = null, image = null, noWhiteSpace=false, profile=false}) {
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));

  const currentFrameSizes = isSmallDevice?frameSizesMobile:frameSizes;

  // Use default framesize dimensions
  const {frameSize, frameMargin} = size && distance ? currentFrameSizes[size]["" + distance] : {frameSize:null, frameMargin:null};

  const scaleFactor = 327;
  const reducerPattern = useSelector(state => state.simulator.pattern);
  const selectedPattern = reducerPattern ? reducerPattern.frame : null;

  // Use pattern property instead of reducer
  const patternUrl = pattern ? `url("${pattern}")` : `url("${selectedPattern}")`;
  const hasPattern = pattern != null || selectedPattern != null;

  const classes = useStyles({scale, hasPattern, scaleFactor, frameSize});

  return (
    <React.Fragment>
      <div style={{backgroundImage: patternUrl}} className={classes.frame}>
        <FilteredImage scale={scale} size={frameSize} sizeMargin={frameMargin} scaleWhiteMargin={scaleWhiteMargin} boxShadow={false}
                       image={image} noWhiteSpace={noWhiteSpace}
                       profile={profile}/>
      </div>
      <div>
        {shadowBellow &&
        <img src='/images/shadow_best.png' style={{marginTop: -20, width: scale * scaleFactor}}/>
        }
      </div>
    </React.Fragment>
  )
}

const useStyles = makeStyles(theme => ({
  frame: {
    "-webkit-user-drag": "none",
    "-webkit-box-shadow": "0px 0px 30px -10px rgba(0,0,0,0.75)",
    "-moz-box-shadow": "0px 0px 30px -10px rgba(0,0,0,0.75)",
    "box-shadow": "0px 0px 30px -10px rgba(0,0,0,0.75)",
    transition: "all 0.3s",
    backgroundSize: ({scale, scaleFactor, frameSize}) => frameSize ? `${frameSize}px ${frameSize}px` : `${scale * scaleFactor}px ${scale * scaleFactor}px`,
    width: ({scale, scaleFactor, frameSize}) => frameSize ? frameSize : scale * scaleFactor,
    height: ({scale, scaleFactor, frameSize}) => frameSize ? frameSize : scale * scaleFactor,
    margin: "auto",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    marginBottom: 20,
    background: "#fff",
    border: ({hasPattern}) => hasPattern ? "" : "1px solid #969696"
  }
}));
