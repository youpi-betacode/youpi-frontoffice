import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import simulatorStepsReducer from './reducers/SimulatorStepsReducer';
import simulatorReducer from './reducers/SimulatorReducer';
import notifications from './reducers/NotificationsReducer';
import registerReducer from './reducers/RegisterReducer';
import loginReducer from './reducers/LoginReducer';
import userReducer from './reducers/UserReducer';
import simulationReducer from './reducers/SimulationReducer';
import loadingReducer from './reducers/LoadingReducer';
import formValidation from './reducers/FormValidationReducer';
import productReducer from './reducers/ProductReducer';
import checkoutStepsReducer from './reducers/CheckoutStepsReducer';
import shoppingCartReducer from './reducers/ShoppingCartReducer';
import store from './reducers/store';
import checkoutAddressesReducer from './reducers/CheckoutAddressesReducer';
import checkoutPaymentsReducer from './reducers/CheckoutPaymentsReducer';

const RootReducer = (history) => combineReducers({
    simulator: simulatorReducer,
    simulatorSteps: simulatorStepsReducer,
    register: registerReducer,
    login: loginReducer,
    user: userReducer,
    simulation: simulationReducer,
    notifications,
    loading: loadingReducer,
    formValidation,
    product: productReducer,
    checkoutSteps: checkoutStepsReducer,
    shoppingCart: shoppingCartReducer,
    store,
    checkoutAddresses: checkoutAddressesReducer,
    checkoutPayments: checkoutPaymentsReducer,
    router: connectRouter(history),
})

export default RootReducer;
