import axios from 'axios';
import * as types from '../Types';
import { toggleSuccessSimulationUpload } from './SimulationActions';
import { submitSimLogin, submitNoSimLogin } from './LoginActions';
import { api, simulationsApi } from '../../constants';
import { enqueueSnackbar } from './NotificationsActions'
import { handleHTTPError, handleHTTPSuccess } from "../../common/utils";
import { push } from 'connected-react-router';

export function setLoading(loading) {
   return {
      type: types.SET_EMAIL_CHECK_LOADING,
      loading: loading
   }
}

export function checkEmail(email, redirect) {
   return function (dispatch) {
      const url = `${api}/check-email/${email}`;

      axios({
         method: 'get',
         url,
         headers: {
            'Content-Type': 'application/json'
         },
      })
         .then(response => {
            dispatch(setLoading(false));
            const hasAccount = response.data.message;
            if (hasAccount) {
               dispatch(handleHTTPError("", 'email-already-registered', 'email-already-registered'));
            } else {
               if (redirect)
                  dispatch(push('/user/register?return=/checkout'))
               else
                  dispatch(push('/user/register'));
            }
         })
         .catch(error => {

         })

   }
}

export function toggleAlreadyResgistered(toggle) {
   return function (dispatch) {
      dispatch({
         type: types.TOGGLE_ALREADY_REGISTERED,
         alreadyRegistered: toggle
      })
   }
}

export function setFirstName(firstName) {
   return function (dispatch) {
      dispatch({
         type: types.SET_FIRST_NAME,
         firstName,
         firstNameFilled: firstName ? true : false
      })
   }
}

export function setLastName(lastName) {
   return function (dispatch) {
      dispatch({
         type: types.SET_LAST_NAME,
         lastName,
         lastNameFilled: lastName ? true : false
      })
   }
}

export function setEmail(email) {
   return function (dispatch) {
      dispatch({
         type: types.SET_EMAIL,
         email,
         emailFilled: email ? true : false
      })
   }
}

export function setCity(city) {
   return function (dispatch) {
      dispatch({
         type: types.SET_CITY,
         city,
         cityFilled: city ? true : false
      })
   }
}

export function setPhone(phone) {
   return function (dispatch) {
      dispatch({
         type: types.SET_PHONE,
         phone
      })
   }
}

export function setPassword(password) {
   return function (dispatch) {
      dispatch({
         type: types.SET_PASSWORD,
         password,
         passwordFilled: password ? true : false,
      })
   }
}

export function setConfirmPassword(confirmPassword) {
   return function (dispatch) {
      dispatch({
         type: types.SET_CONFIRM_PASSWORD,
         confirmPassword,
         //confirmPasswordFilled: confirmPassword ? true : false
      })
   }
}

export function setDateOfBirth(dateOfBirth) {
   return function (dispatch) {
      dispatch({
         type: types.SET_DATE_OF_BIRTH,
         dateOfBirth,
         dateOfBirthFilled: dateOfBirth ? true : false
      })
   }
}

export function setNif(nif) {
   return function (dispatch) {
      dispatch({
         type: types.SET_NIF,
         nif
      })
   }
}

export function setId(id) {
   return function (dispatch) {
      dispatch({
         type: types.SET_USER_ID,
         id
      })
   }
}

export function toggleAcceptConditions(toggle) {
   return function (dispatch) {
      dispatch({
         type: types.TOGGLE_ACCEPT_CONDITIONS,
         acceptConditions: toggle
      })
   }
}

export function toggleModal(toggle) {
   return function (dispatch) {
      dispatch({
         type: types.TOGGLE_REGISTER_MODAL,
         modal: toggle
      })
   }
}

export function toggleFirstNameError(toggle) {
   return function (dispatch) {
      dispatch({
         type: types.TOGGLE_FIRST_NAME_ERROR,
         firstNameError: toggle
      })
   }
}

export function toggleLastNameError(toggle) {
   return function (dispatch) {
      dispatch({
         type: types.TOGGLE_LAST_NAME_ERROR,
         lastNameError: toggle
      })
   }
}

export function toggleEmailError(toggle) {
   return function (dispatch) {
      dispatch({
         type: types.TOGGLE_CITY_ERROR,
         emailError: toggle
      })
   }
}

export function toggleCityError(toggle) {
   return function (dispatch) {
      dispatch({
         type: types.TOGGLE_CITY_ERROR,
         cityError: toggle
      })
   }
}

// export function toggleDateOfBirtError(toggle) {
//   return function(dispatch) {
//     dispatch({
//       type: types.TOGGLE_DATE_OF_BIRTH_ERROR,
//       dateOfBirthError: toggle
//     })
//   }
// }

export function toggleNifError(toggle) {
   return function (dispatch) {
      dispatch({
         type: types.TOGGLE_NIF_ERROR,
         nifError: toggle
      })
   }
}

export function togglePhoneError(toggle) {
   return function (dispatch) {
      dispatch({
         type: types.TOGGLE_PHONE_ERROR,
         phoneError: toggle
      })
   }
}

export function togglePasswordError(toggle) {
   return function (dispatch) {
      dispatch({
         type: types.TOGGLE_PASSWORD_ERROR,
         passwordError: toggle
      })
   }
}

export function toggleConfirmPasswordError(toggle) {
   return function (dispatch) {
      dispatch({
         type: types.TOGGLE_PASSWORD_ERROR,
         confirmPasswordError: toggle
      })
   }
}

export function toggleValidPassword(toggle) {
   return function (dispatch) {
      dispatch({
         type: types.TOGGLE_VALID_PASSWORD,
         validPassword: toggle
      })
   }
}

export function setStreet(street) {
   return function (dispatch) {
      dispatch({
         type: types.SET_STREET_REGISTER,
         street,
         streetFilled: street ? true : false
      })
   }
}

export function setZipCode(zipCode) {
   return function (dispatch) {
      dispatch({
         type: types.SET_ZIP_CODE_REGISTER,
         zipCode,
         zipCodeFilled: zipCode ? true : false
      })
   }
}

export function submitRegister(data, simData, login, redirect, store = 1) {
   return function (dispatch) {
      const url = `${api}/store/${store}/signup/costumer`;

      axios({
         method: 'post',
         url,
         data,
         headers: {
            'Content-Type': 'application/json'
         },
      })
         .then(response => {
            simData.info.costumer_id = response.data.id;
            // dispatch(submitSimulation(simData));
            dispatch(submitSimLogin(login, simData, redirect));
         })
         .catch(error => {
            dispatch(handleHTTPError(error, 'email-already-registered', 'email-already-registered'));
         })
   }
}

export function submitNoSimRegister(data, login, redirect, store = 1) {
   return function (dispatch) {
      const url = `${api}/store/${store}/signup/costumer`;

      axios({
         method: 'post',
         url,
         data,
         headers: {
            'Content-Type': 'application/json'
         },
      })
         .then(response => {
            dispatch(submitNoSimLogin(login, redirect));
         })
         .catch(error => {
            dispatch(handleHTTPError(error, 'email-already-registered', 'email-already-registered'));
         })
   }
}

export function submitSimulation(data) {
   return function (dispatch) {

      const url = `${simulationsApi}/simulation`;


      axios({
         method: 'post',
         url,
         data,
         headers: {
            'Content-Type': 'application/json'
         }
      })
         .then(response => {
            dispatch(handleHTTPSuccess('success-youpi-save', 'success-youpi-save'))
            //dispatch(toggleSuccessSimulationUpload(true));
            //dispatch(toggleModal(false));
            // dispatch(setId(response.data.costumer));
         })
         .catch(error => {
            dispatch(handleHTTPError(error, "youpi-save-error", 'youpi-save-error'));
         })
   }
}
