import { SET_LOADING } from '../Types';

export function toggleLoading(toggle, key=null) {
  return function (dispatch) {
    dispatch({
      type: SET_LOADING,
      loading: toggle,
      key:key
    })
  }
}
