import { NEXT_STEP_CHECKOUT, PREVIOUS_STEP_CHECKOUT, SET_CHECKOUT_STEP } from '../Types';

export function nextCheckoutStep() {
  return function (dispatch) {
    dispatch({
      type: NEXT_STEP_CHECKOUT,
    })
  }
}

export function previousCheckoutStep() {
  return {
    type: PREVIOUS_STEP_CHECKOUT,
  }
}

export function setCheckoutStep(step) {
  return {
    type: SET_CHECKOUT_STEP,
    step
  }
}