import * as types from '../Types';

export function setStep(step){
  return function (dispatch) {
    dispatch({
      type: types.SET_STEP,
      step: step
    })
  }
}