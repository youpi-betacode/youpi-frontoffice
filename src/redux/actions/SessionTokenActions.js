import { SET_SESSION } from '../Types';

export function toggleSession(toggle) {
  return {
    type: SET_SESSION,
    sessionToken: toggle
  }
}