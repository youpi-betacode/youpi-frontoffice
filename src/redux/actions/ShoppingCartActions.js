import { api } from "../../constants";
import axios from "axios";
import { SET_SHOPPING_CART } from "../Types";
import { handleHTTPError, handleHTTPSuccess } from "../../common/utils";

export function addProductOnShoppingCart(reference, extra = null, amount = 1) {
   return function (dispatch) {
      const cart_uuid = localStorage.getItem('cart');

      const data = {
         "extra": extra,
         "amount": amount
      };

      const url = `${api}/store/1/cart/${cart_uuid}/${reference}`;
      axios({
         method: 'post',
         url,
         data
      }).then(response => {
         const cart = response.data;
         localStorage.setItem('cart', cart.uuid);
         dispatch({
            type: SET_SHOPPING_CART,
            cart: cart
         })
         dispatch(handleHTTPSuccess('added-product-to-cart', 'added-product-to-cart'))
      }).catch(error => {
         dispatch(handleHTTPError(error, 'unable-add-product-on-cart', 'unable-add-product-on-cart'))
      })
   }
}

export function removeProductOnShoppingCart(reference) {
   return function (dispatch) {
      const cart_uuid = localStorage.getItem('cart');

      const url = `${api}/store/1/cart/${cart_uuid}/${reference}`;
      axios({
         method: 'delete',
         url
      }).then(response => {
         const cart = response.data;
         localStorage.setItem('cart', cart.uuid);
         dispatch({
            type: SET_SHOPPING_CART,
            cart: cart
         })
         dispatch(handleHTTPSuccess('removed-product-from-cart', 'removed-product-from-cart'))
      }).catch(error => {
         dispatch(handleHTTPError(error, 'unable-delete-product-on-cart', 'unable-delete-product-on-cart'))
      })
   }
}

export function incrementProductOnShoppingCart(reference, extra = {}) {
   return function (dispatch) {
      const cart_uuid = localStorage.getItem('cart');

      const data = {
         "extra": extra
      };
      const url = `${api}/store/1/cart/${cart_uuid}/${reference}/increment`;
      axios({
         method: 'post',
         url,
         data
      }).then(response => {
         const cart = response.data;
         localStorage.setItem('cart', cart.uuid);
         dispatch({
            type: SET_SHOPPING_CART,
            cart: cart
         });
         dispatch(handleHTTPSuccess('added-product-to-cart', 'added-product-to-cart'))
         console.log("producto incrementado no cart")
      }).catch(error => {
         dispatch(handleHTTPError(error, 'unable-increment-product-on-cart', 'unable-increment-product-on-cart'))
      })
   }
}

export function decrementProductOnShoppingCart(reference) {
   return function (dispatch) {
      const cart_uuid = localStorage.getItem('cart');

      const url = `${api}/store/1/cart/${cart_uuid}/${reference}/decrement`;
      axios({
         method: 'post',
         url
      }).then(response => {
         const cart = response.data;
         localStorage.setItem('cart', cart.uuid);
         dispatch({
            type: SET_SHOPPING_CART,
            cart: cart
         });
         dispatch(handleHTTPSuccess('removed-product-from-cart', 'removed-product-from-cart'))
      }).catch(error => {
         dispatch(handleHTTPError(error, 'unable-decrement-product-on-cart', 'unable-decrement-product-on-cart'))
      })
   }
}

function createCart() {
   return function (dispatch) {
      const url = `${api}/store/1/cart`;
      axios({
         method: 'get',
         url
      }).then(response => {
         const cart = response.data;
         localStorage.setItem('cart', cart.uuid);
         dispatch({
            type: SET_SHOPPING_CART,
            cart: cart
         })
      }).catch(error => {
         localStorage.removeItem('cart')
         dispatch(handleHTTPError(error, 'unable-create-cart', 'unable-create-cart'))
      })
   }
}

function getCart(cart_uuid, jwt_token = null) {
   return function (dispatch) {
      let headers = {};
      if (jwt_token) {
         headers = {
            'Authorization': `Bearer ${jwt_token}`
         };
      }
      const url = `${api}/store/1/cart/${cart_uuid}`;
      axios({
         method: 'get',
         url,
         headers
      }).then(response => {
         const cart = response.data;
         localStorage.setItem('cart', cart.uuid);
         dispatch({
            type: SET_SHOPPING_CART,
            cart: cart
         })
      }).catch(error => {
         localStorage.removeItem('cart')
         dispatch(handleHTTPError(error, 'unable-get-cart', 'unable-get-cart'))
      })
   }
}

export function resetShoppingCart() {
   localStorage.removeItem('cart')
   return createCart();
}

export function loadShoppingCart() {
   return function (dispatch) {
      if (typeof localStorage !== 'undefined') {
         const token = localStorage.getItem('token');
         const cart_uuid = localStorage.getItem('cart');

         if (!cart_uuid) {
            dispatch(createCart())
         } else {
            dispatch(getCart(cart_uuid, token))
         }
      }
   }
}
