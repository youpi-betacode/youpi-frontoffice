import axios from 'axios';
import { push } from 'connected-react-router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {animateScroll as scroll} from "react-scroll/modules";
import useTheme from "@material-ui/core/styles/useTheme";

import * as types from '../Types';
import { colors } from '../../constants';
import { api } from '../../constants';
import { submitSimulation, toggleAlreadyResgistered } from './RegisterActions';
import { resetSimulator } from './SimulatorActions';
import { setStep } from './SimulatorStepsActions';
import { translation, handleHTTPError, handleHTTPSuccess } from '../../common/utils';
// import { toggleSession } from './sessionActions';


export function setEmail(email){
  return function(dispatch) {
    dispatch({
      type: types.SET_EMAIL_LOGIN,
      email
    })
  }
}


export function setPassword(password){
  return function(dispatch) {
    dispatch({
      type: types.SET_PASSWORD_LOGIN,
      password
    })
  }
}

export function submitSimLogin(data,simData,redirect) {
  return function(dispatch) {
    const url = `${api}/login`;

    axios({
      method: 'post',
      url,
      data,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(response => {
      localStorage.setItem('token', response.data.token);
      simData.info.costumer_id = response.data.id;
      dispatch(submitSimulation(simData))
      if(redirect)
        dispatch(push('/checkout'));
      else
        dispatch(push('/user/login'));
      scroll.scrollTo(0);
      if(redirect){
        dispatch(push(redirect));
      }
    })
    .catch(error => {
      dispatch(handleHTTPError("",'invalid-login-credentials','invalid-login-credentials'))
    })
  }
}

export function submitNoSimLogin(data, redirect){
  return function(dispatch){
    const url = `${api}/login`;

    axios({
      method: 'post',
      url,
      data,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(response => {
      localStorage.setItem('token', response.data.token);
      //dispatch(submitEmailConfirmation(response.data.token));
      //dispatch(toggleSession(true));
      if(redirect){
        dispatch(push('/checkout/summary'))
      }else{
        dispatch(push('/user'));
        scroll.scrollTo(0);
      }
    })
    .catch(error => {
      dispatch(handleHTTPError("",'invalid-login-credentials','invalid-login-credentials'))
      //dispatch(toggleInvalidLogin(true));
    })
  }
}

export function toggleInvalidLogin(toggle){
  return function(dispatch) {
    dispatch({
      type: types.TOGGLE_INVALID_LOGIN,
      invalidLogin: toggle
    })
  }
}

export function logout(){
  return function(dispatch){
    const url = `${api}/logout`;
    const token = localStorage.getItem('token');
    localStorage.removeItem('token');
    localStorage.removeItem('shopping-cart');
    dispatch({
      type: types.RESET_SHOPPING_CART
    });
    axios({
      method: 'post',
      url,
      headers: {
        'Content-Type' : 'application/json',
        'Authorization' : `Bearer ${token}`
      }
    })
    .then(response => {
      dispatch(resetSimulator());
      dispatch(setStep(0));
      dispatch(push('/'));
    })
    .catch(error => {
        dispatch(handleHTTPError(error,'no-response-error','no-response-error'))
    })
  }
}

export function recoverPassword(email) {
  return function(dispatch) {
    const url = `${api}/recover_password/${email}`;
        axios({
          method:'post',
          url,
          headers: {
            'Content-Type' : 'application/json'
          }
        }).then(response => {
          dispatch(handleHTTPSuccess(`Foi enviada uma nova palavra-passe para o seu e-mail`,`Foi enviada uma nova palavra-passe para o seu e-mail`))
        }).catch(error => {
         dispatch(handleHTTPError('','O e-mail introduzido não existe','O e-mail introduzido não existe'))
        })

    // axios({
    //   method:'post',
    //   url,
    //   headers: {
    //     'Content-Type' : 'application/json'
    //   }
    // }).then(response => {
    //   Swal.fire({
    //     title: 'Foi enviada uma nova palavra-passe para o seu e-mail',
    //     confirmButtonText: 'Ok',
    //     customClass: {
    //       confirmButton: 'youpii-confirm-button-class'
    //     }
    //   })
    // }).
    // catch(error => {
    //   Swal.fire({
    //     title: 'O e-mail introduzido já se encontra registado',
    //     confirmButtonText: 'Ok',
    //     customClass: {
    //       confirmButton: 'youpii-confirm-button-class'
    //     }
    //   })
    // })
  }
}
