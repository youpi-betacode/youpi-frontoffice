import axios from 'axios'
import { GET_PRODUCTS, SET_SUB_FILTER, SET_STORE_PAGE, SET_STORE_DESC, SET_STORE_ORDER_BY, SET_STORE_MAIN_FILTER} from '../Types';
import { api } from '../../constants';
import { toggleLoading } from './LoadingActions';
import { pageSize } from '../../constants';

export function getProducts(data) {
  return function (dispatch) {
    const url = `${api}/store/1/product/list`;

    dispatch(toggleLoading(true, "storeProducts"));

    data.page_size = pageSize

    axios({
      method: 'post',
      url,
      data,
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => {
        dispatch({
          type: GET_PRODUCTS,
          products: response.data.products,
          count: response.data.count,
          nrPages: response.data.count%pageSize === 0 ? (response.data.count/pageSize) : Math.ceil(response.data.count/pageSize)
        })
        dispatch(toggleLoading(false, "storeProducts"));
      })
      .catch(error => {

      })
  }
}

export function setSubfilter(subFilter) {
  return function (dispatch) {
    dispatch({
      type: SET_SUB_FILTER,
      subFilter
    })
  }
}


export function setDesc(desc) {
  return function (dispatch) {
    dispatch({
      type: SET_STORE_DESC,
      desc
    })
  }
}

export function setStorePage(page) {
  return function(dispatch) {
    dispatch({
      type: SET_STORE_PAGE,
      page
    })
  }
}

export function setOrderBy(orderBy) {
  return function(dispatch) {
    dispatch({
      type: SET_STORE_ORDER_BY,
      orderBy
    })
  }
}

export function setStoreMainFilter(display, category, subCategory) {
  return function(dispatch) {
    dispatch({
      type: SET_STORE_MAIN_FILTER,
      displaying: display,
      category: category,
      subFilter: subCategory
    })
  }
}
