import * as types from '../Types';
import axios from 'axios';
import { api } from '../../constants';
import { submitSimulation } from './RegisterActions';

export function submitLoggedSim(data) {
  return function(dispatch){
    const url =`${api}/user`;
    const token = localStorage.getItem('token');

    axios({
      method: 'get',
      url,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
      .then(response => {
        dispatch({
          type: types.GET_USER_INFO,
          firstName: response.data.first_name,
          lastName: response.data.last_name,
          email: response.data.email,
          city: response.data.local,
          dateOfBirth: response.data.extra.date_of_birth,
          nif: response.data.extra.nif === 0 ? '' : response.data.extra.nif,
          phone: response.data.contact === 0 ? '' : response.data.contact,
          role: response.data.role,
          id: response.data.id
        });
        data.info.costumer_id = response.data.id;
        dispatch(submitSimulation(data));
      })
      .catch(error => {
      })
  }
}
