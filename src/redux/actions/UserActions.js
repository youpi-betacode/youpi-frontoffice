import axios from 'axios';

import * as types from '../Types';
import { api } from '../../constants';
import { simulationsApi, historyPageSize } from '../../constants';
import { toggleLoading } from './LoadingActions';
import {handleHTTPError, handleHTTPSuccess} from "../../common/utils";

export function getUserInfo() {
  return function (dispatch) {
    const url = `${api}/user`;
    const token = localStorage.getItem('token');

    dispatch(toggleLoading(true, "userInfo"));

    axios({
      method: 'get',
      url,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
      .then(response => {
        dispatch({
          type: types.GET_USER_INFO,
          firstName: response.data.first_name,
          lastName: response.data.last_name,
          email: response.data.email,
          city: response.data.local,
          dateOfBirth: response.data.extra.date_of_birth,
          nif: response.data.extra.nif === 0 ? '' : response.data.extra.nif,
          phone: response.data.contact === '0' ? '' : response.data.contact,
          role: response.data.role,
          id: response.data.id,
          zipCode: response.data.extra.zipCode,
          street: response.data.extra.street
        });
        dispatch(toggleLoading(false, "userInfo"));
      })
      .catch(error => {
        //dispatch(handleHTTPError(error,'unable-fetch-user-data', 'unable-fetch-user-data'))
      })
  }
}

export function submit(data) {
  return function (dispatch) {
    const url = `${api}/costumer`;
    const token = localStorage.getItem('token');

    axios({
      method: 'put',
      url,
      data,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
      .then(response => {
        dispatch(handleHTTPSuccess('success-user-data-change', 'success-user-data-change'))
      })
      .catch(error => {
          dispatch(handleHTTPError(error,'email-already-registered', 'email-already-registered'))
      })
  }
}

export function changeStreet(street) {
  return function(dispatch) {
    dispatch({
      type: types.CHANGE_STREET_USER,
      street
    })
  }
}

export function changeZipCode(zipCode) {
  return function(dispatch) {
    dispatch({
      type: types.CHANGE_ZIP_CODE_USER,
      zipCode
    })
  }
}

export function changeFirstName(firstName) {
  return function (dispatch) {
    dispatch({
      type: types.CHANGE_FIRST_NAME_USER,
      firstName
    })
  }
}


export function changeLastName(lastName) {
  return function (dispatch) {
    dispatch({
      type: types.CHANGE_LAST_NAME_USER,
      lastName
    })
  }
}

export function changeEmail(email) {
  return function (dispatch) {
    dispatch({
      type: types.CHANGE_EMAIL_USER,
      email
    })
  }
}

export function changeCity(city){
  return {
    type: types.CHANGE_CITY_USER,
    city
  }
}

export function changeDateOfBirth(dateOfBirth){
  return {
    type: types.CHANGE_DATE_OF_BIRTH_USER,
    dateOfBirth
  }
}

export function changeNif(nif){
  return {
    type: types.CHANGE_NIF_USER,
    nif
  }
}

export function changePhone(phone){
  return {
    type: types.CHANGE_PHONE_USER,
    phone
  }
}

export function getUserSimulations(customerId) {
  return function (dispatch) {
    const url = `${simulationsApi}/simulations`;

    dispatch(toggleLoading(true, "simulations"));

    //console.log("customerID", customerId)

    const data = {
      "info": {
        "costumer_id": customerId
      }
    };

    axios({
      method: 'post',
      url,
      data,
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => {
        dispatch({
          type: types.GET_USER_SIMULATIONS,
          simulations: response.data
        });
        dispatch(toggleLoading(false, "simulations"));
      })
      .catch(error => {
        dispatch(toggleLoading(false, "simulations"));
        if(error && error.response && error.response.status === 404){
          dispatch({
            type: types.GET_USER_SIMULATIONS,
            simulations: []
          });
        }else{
           dispatch(handleHTTPError(error, "get-simulations-error", "get-simulations-error"));
        }
      })
  }
}

export function getUserLastSimulation(costumerID){
  return function (dispatch) {
    const url = `${simulationsApi}/costumer/${costumerID}/latest`;

    axios({
      method: 'get',
      url,
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => {
        console.log(response)
      })
      .catch(error => {
        console.log(error)
      })
  }
}

export function changePassword(data) {
  return function(dispatch) {
    const url = `${api}/user/reset_password`;
    const token = localStorage.getItem('token');

    axios({
      method: 'put',
      url,
      data,
      headers: {
        'Content-Type' : 'application/json',
        'Authorization' : `Bearer ${token}`
      }
    })
    .then(response => {
      dispatch(handleHTTPSuccess('success-password-change', 'success-password-change'));
    })
    .catch(error => {
      dispatch(handleHTTPError(error,'unable-to-change-password','unable-to-change-password'))
    })
  }
}

export function deleteSimulation(simulationId, costumerId){
  return function(dispatch) {
    const url = `${simulationsApi}/simulation/${simulationId}`;
    const token = localStorage.getItem('token');

    axios({
      method: 'delete',
      url,
      headers: {
        'Content-Type' : 'application/json',
        'Authorization' : `Bearer ${token}`
      }
    })
    .then(response => {
      dispatch(getUserSimulations(costumerId))
      dispatch(handleHTTPSuccess('success-simulation-delete', 'success-simulation-delete'));
    })
    .catch(error => {
      dispatch(handleHTTPError(error,'error-simulation-delete','error-simulation-delete'))
    })
  }
}


export function getUserPurchaseHistory(page=1){
  return function(dispatch){
    const url = `${api}/costumer/sales`;
    const token = localStorage.getItem('token');

    dispatch(toggleLoading(true, "userHistory"));

    const data = {
      'page': page,
      'page_size': historyPageSize
    }

    axios({
      method: 'post',
      url,
      data,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'}
    })
    .then(response => {
      console.log(response.data.sales);
      dispatch({
        type: types.GET_USER_HISTORY,
        purchaseHistory: response.data.sales,
        historyCount: response.data.count,
        historyNrPages: response.data.count%historyPageSize === 0 ? (response.data.count/historyPageSize) : Math.ceil(response.data.count/historyPageSize)
      })
      dispatch(toggleLoading(false, "userHistory"));
    })
    .catch(error => {

    })
  }
}

export function setHistoryPage(page) {
  return function(dispatch) {
    dispatch({
      type: types.SET_USER_HISTORY_PAGE,
      historyPage: page
    })
  }
}