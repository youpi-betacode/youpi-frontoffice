import axios from 'axios';
import { push } from 'connected-react-router';
import { SET_PAYMENT_TYPE, GET_PAYMENT_INFO, LOAD_SALE, RESET_SHOPPING_CART } from '../Types';
import { api } from '../../constants';
import { handleHTTPError } from "../../common/utils";
import { resetShoppingCart } from "./ShoppingCartActions";
import { toggleLoading } from './LoadingActions';

export function getSale(uuid) {
    return function (dispatch) {
        const url = `${api}/sale/${uuid}`
        const token = localStorage.getItem('token');

        axios({
            method: 'get',
            url,
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
            .then(response => {
                dispatch(loadSale(response.data))
            })
            .catch(error => {

            })
    }
}

export function loadSale(sale) {
    return {
        type: LOAD_SALE,
        sale: sale
    }
}

export function setPaymentType(paymentType) {
    return function (dispatch) {
        dispatch({
            type: SET_PAYMENT_TYPE,
            paymentType: paymentType
        })
    }
}

export function createSale(shoppingCart, paymentType, addresses) {
    return function (dispatch) {
        const url = `${api}/costumer/sale`;
        const token = localStorage.getItem('token');

        dispatch(toggleLoading(true, "checkoutPayment"));
        
        axios({
            method: 'post',
            url: url,
            data: {
                payment: paymentType,
                shopping_cart:shoppingCart,
                extra: {
                    addresses: addresses
                }
            },
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        .then(response => {
            dispatch(resetShoppingCart());
            dispatch(toggleLoading(true, "checkoutPayment"));
            const sale = response.data;
            if (paymentType === "mastercard" || paymentType === "visa" || paymentType === "maestro") {
              window.location.href = sale.extra.payment_info
            } else {
              dispatch(loadSale(sale));
              dispatch(push(`/checkout/status/${sale.uuid}`));
            }
          // After the sale is done create a new shopping cart
        })
        .catch(error => {
            dispatch(handleHTTPError(error, 'unable-create-sale', 'unable-create-sale'))
            dispatch(toggleLoading(false, "checkoutPayment"));
        })
    }
}

export function getPaymentInfo(reference) {
    return function (dispatch) {
        const url = `${api}/hipay/reference_info`;
        // const token = localStorage.getItem('token');

        axios({
            method: 'post',
            url,
            data: {
                mb_reference: reference
            },
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Bearer ${token}`
            }
        })
            .then(response => {
                dispatch({
                    type: GET_PAYMENT_INFO,
                    payment: response.data
                })
            })
            .catch(error => {
            })
    }
}
