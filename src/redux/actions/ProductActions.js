import axios from 'axios';

import { SET_PRODUCT, SET_PRODUCT_SIZE, SET_RELATED_PRODUCTS, SET_CURRENT_PRICE, SET_CURRENT_REFERENCE, SET_CURRENT_PRODUCT } from '../Types';
import { api } from '../../constants';
import { toggleLoading } from './LoadingActions';
import {defaultStore} from "../../constants";
import { func } from 'prop-types';

export function getProduct(id) {
  return function(dispatch) {
    const url = `${api}/store/${defaultStore}/product/${id}`;

    dispatch(toggleLoading(true, "product"));

    axios({
      method: 'get',
      url,
    })
    .then(response => {
      dispatch({
        type: SET_PRODUCT,
        product: response.data,
      })
      dispatch(toggleLoading(false, "product"));
    })
    .then(error => {

    })
  }
}

export function getRelatedProducts(reference) {
  return function(dispatch) {
    const url = `${api}/store/${defaultStore}/product/${reference}/related`;
    //const token = localStorage.getItem("token");

    axios({
      method: 'get',
      url,
    })
    .then(response => {
      dispatch(setRelatedProducts(response.data.products))
    })
    .then(error => {

    })
  }
}

export function setRelatedProducts(relatedProducts) {
  return function(dispatch) {
    dispatch({
      type: SET_RELATED_PRODUCTS,
      relatedProducts: relatedProducts
    })
  }
}

export function setSize(size) {
  return function (dispatch) {
    dispatch({
      type: SET_PRODUCT_SIZE,
      size: size
    })
  }
}

export function setCurrentPrice(price) {
  return function (dispatch) {
    dispatch({
      type: SET_CURRENT_PRICE,
      price
    })
  }
}

export function setCurrentRef(reference) {
  return function (dispatch) {
    dispatch({
      type: SET_CURRENT_REFERENCE,
      reference
    })
  }
}

export function setCurrentProduct(currentProduct) {
   return function (dispatch) {
     dispatch({
       type: SET_CURRENT_PRODUCT,
       currentProduct: currentProduct
     })
   }
 }