import axios from 'axios';

import { api } from '../../constants';
import {
  GET_CHECKOUT_SHIPPING_ADDRESSES, GET_CHECKOUT_BILLING_ADDRESSES, SET_CHECKOUT_SHIPPING_ADDRESS,
  SET_CHECKOUT_SHIPPING_CITY, SET_CHECKOUT_SHIPPING_ZIP_CODE,
  SET_CHECKOUT_BILLING_ADDRESS, SET_CHECKOUT_BILLING_CITY,
  SET_CHECKOUT_BILLING_ZIP_CODE, SET_VALID_FORM,
  SET_CHECKOUT_SHIPPING_NAME, SET_CHECKOUT_BILLING_NAME, SET_CHECKOUT_SHIPPING_PHONE_NUMBER,
  SET_CHECKOUT_BILLING_NIF, GET_BILLING_NAME_NIF, GET_SHIPPING_NAME_CONTACT
} from '../Types';

export function getUserAddresses(address) {
  return function (dispatch) {
    const url = `${api}/user`;
    const token = localStorage.getItem('token');

    axios({
      method: 'get',
      url,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }).then(response => {
      if (address === 'billing')
        dispatch({
          type: GET_CHECKOUT_BILLING_ADDRESSES,
          billingAddress: response.data.extra.street,
          billingCity: response.data.local,
          billingZipCode: response.data.extra.zipCode,
        });
      if (address === 'shipping')
        dispatch({
          type: GET_CHECKOUT_SHIPPING_ADDRESSES,
          //shippingName: response.data.first_name + ' ' + response.data.last_name,
          shippingAddress: response.data.extra.street,
          shippingCity: response.data.local,
          shippingZipCode: response.data.extra.zipCode,
          //shippingPhoneNumber: response.data.contact
        });
    })
      .catch(error => {

      })
  }
}

export function getUserCheckoutInfo(address) {
  return function(dispatch) {
    const url = `${api}/user`
    const token = localStorage.getItem('token')
    
    axios({
      method: 'get',
      url,
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
    .then(response => {
      if(address === 'billing')
        dispatch({
          type: GET_BILLING_NAME_NIF,
          billingName: `${response.data.first_name} ${response.data.last_name}`,
          billingNIF: response.data.extra.nif == 0 ? '' : response.data.extra.nif
      })
      if(address === 'shipping')
      dispatch({
        type: GET_SHIPPING_NAME_CONTACT,
        shippingName: `${response.data.first_name} ${response.data.last_name}`,
        shippingPhoneNumber: response.data.contact == 0 ? '' : response.data.contact
      });
    })
    .catch(error => {

    })
  }
}

export function setShippingName(shippingName) {
  return function(dispatch) {
    dispatch({
      type: SET_CHECKOUT_SHIPPING_NAME,
      shippingName
    })
  }
}

export function setShippingAddress(shippingAddress) {
  return function (dispatch) {
    dispatch({
      type: SET_CHECKOUT_SHIPPING_ADDRESS,
      shippingAddress
    })
  }
}

export function setShippingZipCode(shippingZipCode) {
  return function (dispatch) {
    dispatch({
      type: SET_CHECKOUT_SHIPPING_ZIP_CODE,
      shippingZipCode
    })
  }
}

export function setShippingCity(shippingCity) {
  return function (dispatch) {
    dispatch({
      type: SET_CHECKOUT_SHIPPING_CITY,
      shippingCity
    })
  }
}

export function setShippingPhoneNumber(shippingPhoneNumber) {
  return function (dispatch) {
    dispatch({
      type: SET_CHECKOUT_SHIPPING_PHONE_NUMBER,
      shippingPhoneNumber
    })
  }
}

export function setBillingName(billingName) {
  return function(dispatch) {
    dispatch({
      type: SET_CHECKOUT_BILLING_NAME,
      billingName
    })
  }
}

export function setBillingAddress(billingAddress) {
  return function (dispatch) {
    dispatch({
      type: SET_CHECKOUT_BILLING_ADDRESS,
      billingAddress
    })
  }
}

export function setBillingZipCode(billingZipCode) {
  return function (dispatch) {
    dispatch({
      type: SET_CHECKOUT_BILLING_ZIP_CODE,
      billingZipCode
    })
  }
}

export function setBillingCity(billingCity) {
  return function (dispatch) {
    dispatch({
      type: SET_CHECKOUT_BILLING_CITY,
      billingCity
    })
  }
}

export function setBillingNIF(billingNIF) {
  return function (dispatch) {
    dispatch({
      type: SET_CHECKOUT_BILLING_NIF,
      billingNIF
    })
  }
}

export function setValidForm(validForm) {
  return function (dispatch) {
    dispatch({
      type: SET_VALID_FORM,
      validForm
    })
  }
}
