import { TOGGLE_VALID_FORM } from '../Types';

export function toggleValidForm(toggle) {
  return function (dispatch) {
    dispatch({
      type: TOGGLE_VALID_FORM,
      valid: toggle
    })
  }
}