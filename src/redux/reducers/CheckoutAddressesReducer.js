import { GET_CHECKOUT_BILLING_ADDRESSES, GET_CHECKOUT_SHIPPING_ADDRESSES,
        SET_CHECKOUT_SHIPPING_ADDRESS,
        SET_CHECKOUT_SHIPPING_CITY, SET_CHECKOUT_SHIPPING_ZIP_CODE,
        SET_CHECKOUT_BILLING_ADDRESS, SET_CHECKOUT_BILLING_CITY,
        SET_CHECKOUT_BILLING_ZIP_CODE, SET_CHECKOUT_BILLING_NAME,
        SET_CHECKOUT_SHIPPING_NAME, SET_CHECKOUT_BILLING_NIF,
        SET_CHECKOUT_SHIPPING_PHONE_NUMBER, SET_VALID_FORM,
        GET_BILLING_NAME_NIF, GET_SHIPPING_NAME_CONTACT} from '../Types';

const initialState = {
  shippingName: '',
  shippingAddress: '',
  shippingCity: '',
  shippingZipCode: '',
  shippingPhoneNumber: '',
  billingName: '',
  billingAddress: '',
  billingCity: '',
  billingZipCode: '',
  billingNIF: '',
  validForm: true
}

const checkoutAddressesReducer = (state = initialState, action) => {
  const newState = { ...state };
  switch (action.type) {
    case GET_CHECKOUT_SHIPPING_ADDRESSES:
      newState.shippingAddress = action.shippingAddress;
      newState.shippingCity = action.shippingCity;
      newState.shippingZipCode = action.shippingZipCode;
      return newState;
    case GET_CHECKOUT_BILLING_ADDRESSES:
        newState.billingAddress = action.billingAddress;
        newState.billingCity = action.billingCity;
        newState.billingZipCode = action.billingZipCode;
        return newState;
    case SET_CHECKOUT_SHIPPING_NAME:
      newState.shippingName = action.shippingName;
      return newState;
    case SET_CHECKOUT_SHIPPING_ADDRESS:
      newState.shippingAddress = action.shippingAddress;
      return newState;
    case SET_CHECKOUT_SHIPPING_CITY:
      newState.shippingCity = action.shippingCity;
      return newState;
    case SET_CHECKOUT_SHIPPING_ZIP_CODE:
      newState.shippingZipCode = action.shippingZipCode;
      return newState;
    case SET_CHECKOUT_SHIPPING_PHONE_NUMBER:
      newState.shippingPhoneNumber = action.shippingPhoneNumber;
      return newState;
    case SET_CHECKOUT_BILLING_NAME:
        newState.billingName = action.billingName;
        return newState;
    case SET_CHECKOUT_BILLING_ADDRESS:
      newState.billingAddress = action.billingAddress;
      return newState;
    case SET_CHECKOUT_BILLING_CITY:
      newState.billingCity = action.billingCity;
      return newState;
    case SET_CHECKOUT_BILLING_ZIP_CODE:
      newState.billingZipCode = action.billingZipCode;
      return newState;
    case SET_CHECKOUT_BILLING_NIF:
      newState.billingNIF = action.billingNIF;
      return newState
    case SET_VALID_FORM:
      newState.validForm = action.validForm;
      return newState;
    case GET_BILLING_NAME_NIF:
      newState.billingName = action.billingName;
      newState.billingNIF = action.billingNIF;
      return newState;
    case GET_SHIPPING_NAME_CONTACT:
      newState.shippingName = action.shippingName;
      newState.shippingPhoneNumber = action.shippingPhoneNumber;
      return newState;
    default:
      return newState;
  }
}

export default checkoutAddressesReducer;
