import { SET_SHOPPING_CART, RESET_SHOPPING_CART } from '../Types';

const initialState = {
   cart: null
};

const shoppingCartReducer = (state = initialState, action) => {
   let newState = { ...state };

   switch (action.type) {
      case SET_SHOPPING_CART:
         newState.cart = action.cart;
         return newState;
      case RESET_SHOPPING_CART:
         return initialState;
      default:
         return newState;
   }
};

export default shoppingCartReducer;
