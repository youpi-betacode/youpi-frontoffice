import * as types from '../Types';
//import {convertBlobToBase64} from "../../common/utils";
import simulationOptions from './simulator-options'

const initialState = {
  picture: null,
  backgroundPicture: null,
  backgroundPictureRotation:0,
  croppedCanvas: null,
  filteredPhoto: null,
  croppedCanvasResized: null,
  filteredPhotoResized: null,
  options: simulationOptions,
  pattern: null,
  gama: null,
  size: null,
  whiteSpace: false,
  phrase: "",
  contrast: 0.5,
  brightness: 0.5,
  saturate: 0.5,
  posterize: 0.5,
  processing: false,
  error: null,
  distance: 3,
  preview: {
    deltaX:0,
    deltaY:-100
  },
  previewImage: null,
  shareLoading: false
};

const simulatorReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_SHARE_LOADING:
      let loadingState = {...state};
      loadingState.shareLoading = action.shareLoading
      return loadingState;

    case types.SET_CROPPED_CANVAS_RESIZED:
      let resizedCropState = {...state};
      resizedCropState.croppedCanvasResized = action.croppedCanvasResized;
      return resizedCropState;

    case types.SET_FILTERED_PHOTO_RESIZED:
      let resizedFilterState = {...state};
      resizedFilterState.filteredPhotoResized = action.filteredPhotoResized;
      resizedFilterState.processing = false;
      return resizedFilterState;

    case types.ROTATE_BACKGROUND_PICTURE:
      let rotateBackgroundState = {...state};
      rotateBackgroundState.backgroundPictureRotation = state.backgroundPictureRotation + 90;
      return rotateBackgroundState;

    case types.SET_PREVIEW_IMG:
      let setPreviewImgState = {...state};
      setPreviewImgState.previewImage = action.previewImg;
      return setPreviewImgState;

    case types.SET_PREVIEWS_PARAMS:
      let setPreviewParamState = {...state};
      const previewData = {
        deltaX: action.deltaX,
        deltaY: action.deltaY,
        whiteMargin: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD//gATQ3JlYXRlZCB3aXRoIEdJTVD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAABAAEDAREAAhEBAxEB/8QAFAABAAAAAAAAAAAAAAAAAAAACP/EABQBAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhADEAAAAVSf/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABBQJ//8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAwEBPwF//8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAgEBPwF//8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQAGPwJ//8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPyF//9oADAMBAAIAAwAAABCf/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAwEBPxB//8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAgEBPxB//8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxB//9k="
      };
      setPreviewParamState["preview"] = previewData;
      return setPreviewParamState;

    case types.SET_DISTANCE:
      let setDistanceState = {...state};
      setDistanceState.distance = action.distance;
      return setDistanceState;

    case types.SET_FILTERED_PHOTO:
      let setFilteredPhoto = {...state};
      setFilteredPhoto.filteredPhoto = action.filteredPhoto;
      setFilteredPhoto.processing = false;
      return setFilteredPhoto;

    case types.RESET_FILTER:
      let setResetFilterState = {...state};
      setResetFilterState.contrast = initialState.contrast;
      setResetFilterState.brightness = initialState.brightness;
      setResetFilterState.saturate = initialState.saturate;
      setResetFilterState.posterize = initialState.posterize;
      setResetFilterState.filteredPhoto = setResetFilterState.croppedCanvas;
      setResetFilterState.filteredPhotoResized = setResetFilterState.croppedCanvasResized;
      setResetFilterState.processing = false;
      return setResetFilterState;

    case types.SET_ERROR:
      let setErrorState = {...state};
      setErrorState.error = action.error;
      return setErrorState;

    case types.SET_PROCESSED:
      let setProcessedState = {...state};
      setProcessedState.processing = false;
      return setProcessedState;

    case types.SET_BRIGHTNESS:
      let setBrightnessState = {...state};
      setBrightnessState.brightness = action.brightness;
      setBrightnessState.processing = true;
      return setBrightnessState;

    case types.SET_POSTORIZE:
      let setPostorizeState = {...state};
      setPostorizeState.posterize = action.posterize;
      setPostorizeState.processing = true;
      return setPostorizeState;

    case types.SET_SATURATE:
      let setSaturateState = {...state};
      setSaturateState.saturate = action.saturate;
      setSaturateState.processing = true;
      return setSaturateState;

    case types.SET_CONTRAST:
      let setContrastState = {...state};
      setContrastState.contrast = action.contrast;
      setContrastState.processing = true;
      return setContrastState;

    case types.SET_PHRASE:
      let setPhraseState = {...state};
      setPhraseState.phrase = action.phrase;
      return setPhraseState;

    case types.SET_WHITE_SPACE:
      let setWhiteSpaceState = {...state};
      setWhiteSpaceState.whiteSpace = action.whiteSpace;
      return setWhiteSpaceState;

    case types.SET_SIZE:
      let setSizeState = {...state};
      setSizeState.size = action.size;
      return setSizeState;

    case types.SET_GAMA:
      let setGamaState = {...state};
      setGamaState.gama = action.gama;
      setGamaState.pattern = state.options.patterns[action.gama][0];
      return setGamaState;

    case types.SET_PATTERN:
      let setPatternState = {...state};
      setPatternState.pattern = action.pattern;
      return setPatternState;

    case types.LOAD_OPTIONS:
      let loadOptionsState = {...state};
      loadOptionsState.options = action.options;
      return loadOptionsState;

    case types.SET_SIMULATOR_PICTURE:
      let newState = {...state};
      newState.picture = action.picture;
      return newState;

    case types.SET_CROPPED_CANVAS:
      let newStateCanvas = {...state};
      newStateCanvas.croppedCanvas = action.canvas;
      newStateCanvas.filteredPhoto = action.canvas;
      newStateCanvas.processing = true;
      return newStateCanvas;

    case types.SET_BACKGROUND_PICTURE:
      let newBackgroundPictureCanvas = {...state};
      newBackgroundPictureCanvas.backgroundPicture = action.backgroundPicture;
      return newBackgroundPictureCanvas;

    case types.RESET_SIMULATOR:
      return initialState;
    default:
      return state;
  }
};

export default simulatorReducer;
