import { NEXT_STEP_CHECKOUT, PREVIOUS_STEP_CHECKOUT, SET_CHECKOUT_STEP } from '../Types';

const initialState = {
  step: 0
};

const checkoutStepsReducer = (state = initialState, action) => {
  let newState = {...state};

  switch(action.type){
    case NEXT_STEP_CHECKOUT:
      newState.step = state.step + 1;
      return newState;
    case PREVIOUS_STEP_CHECKOUT:
      newState.step = state.step - 1;
      return newState;
    case SET_CHECKOUT_STEP:
      newState.step = action.step;
      return newState;
    default:
      return newState;
  }
};

export default checkoutStepsReducer;
