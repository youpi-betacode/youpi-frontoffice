import * as types from '../Types';

const initialState = {
  firstName: '',
  lastName: '',
  email: '',
  city: '',
  dateOfBirth: '',
  nif: '',
  phone: '',
  newPassword: '',
  role: 0,
  simulations: null,
  zipCode: '',
  street: '',
  id: -1,
  purchaseHistory: [],
  historyCount: 0,
  historyPage: 1,
  historyPages: [],
  historyNrPages: 0
}

const userReducer = (state = initialState, action) => {
  const newState = { ...state };
  switch (action.type) {
    case types.GET_USER_INFO:
      newState.firstName = action.firstName;
      newState.lastName = action.lastName;
      newState.email = action.email;
      newState.city = action.city;
      newState.dateOfBirth = action.dateOfBirth;
      newState.nif = action.nif;
      newState.phone = action.phone;
      newState.role = action.role
      newState.id = action.id;
      newState.zipCode = action.zipCode;
      newState.street = action.street;
      return newState;
    case types.CHANGE_FIRST_NAME_USER:
      newState.firstName = action.firstName;
      return newState;
    case types.CHANGE_LAST_NAME_USER:
      newState.lastName = action.lastName;
      return newState;
    case types.CHANGE_EMAIL_USER:
      newState.email = action.email;
      return newState;
    case types.GET_USER_SIMULATIONS:
      newState.simulations = action.simulations;
      return newState;
    case types.CHANGE_CITY_USER:
      newState.city = action.city;
      return newState;
    case types.CHANGE_DATE_OF_BIRTH_USER:
      newState.dateOfBirth = action.dateOfBirth;
      return newState;
    case types.CHANGE_NIF_USER:
      newState.nif = action.nif;
      return newState;
    case types.CHANGE_PHONE_USER:
      newState.phone = action.phone;
      return newState;
    case types.CHANGE_STREET_USER:
      newState.street = action.street;
      return newState;
    case types.CHANGE_ZIP_CODE_USER:
      newState.zipCode = action.zipCode;
      return newState;
    case types.GET_USER_HISTORY:
      newState.purchaseHistory = action.purchaseHistory;
      newState.historyCount = action.historyCount;
      newState.historyNrPages = action.historyNrPages;
      newState.historyPages = [];
      for(let i = 1;i <= newState.historyNrPages;i++)
        newState.historyPages.push(i)
      return newState;
    case types.SET_USER_HISTORY_PAGE:
      newState.historyPage = action.historyPage;
      return newState;
    default:
      return state;
  }
}

export default userReducer;
