import * as types from '../Types';

const initialState = {

};

const simulationReducer = (state = initialState, action) => {
  let newState = {...state};

  switch(action.type){

    default:
      return newState;
  }
}

export default simulationReducer;
