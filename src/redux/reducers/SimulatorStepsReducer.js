import * as types from '../Types';

const initialState = {
  step: 0
};

const simulatorStepsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_STEP:
      let newState = { ...state };
      newState.step = action.step;
      return newState;
    default:
      return state;
  }
};

export default simulatorStepsReducer;
