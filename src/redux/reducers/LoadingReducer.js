import { SET_LOADING } from '../Types';

const initialState = {
  loading: false,
  subLoadings:{}
};

const loadingReducer = (state = initialState, action) => {
  let newState = {...state};

  switch(action.type){
    case SET_LOADING:
      if(action.key){
        let subLoadings = {...state.subLoadings};
        subLoadings[action.key] = action.loading;
        newState.subLoadings = subLoadings;
      }else{
        newState.loading = action.loading;
      }
      return newState;
    default:
      return newState;
  }
};

export default loadingReducer;
