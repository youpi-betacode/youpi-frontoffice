import { GET_PRODUCTS, SET_SUB_FILTER, SET_STORE_PAGE, SET_STORE_DESC, SET_STORE_ORDER_BY, SET_STORE_MAIN_FILTER} from '../Types';
import simulationOptions from './simulator-options';
import { pageSize } from '../../constants';

const initialState = {
  products: [],
  subFilter: [simulationOptions.gamas[1]].toString(),
  count: 0,
  pageSize: pageSize,
  page: 1,
  nrPages: 0,
  total_amount: 0,
  orderBy: 'category',
  pages: [],
  desc: false,
  displaying: 'YOUPi!',
  category: 'YOUPi!',
}

const store = (state = initialState, action) => {
  const newState = {...state};

  switch(action.type) {
    case GET_PRODUCTS:
      newState.products = action.products;
      newState.count = action.count;
      newState.nrPages = action.nrPages;
      newState.pages = [];
      for(let i = 1;i<=newState.nrPages;i++)
        newState.pages.push(i)
      return newState;
    case SET_SUB_FILTER:
      newState.subFilter = action.subFilter;
      return newState;
    case SET_STORE_PAGE:
      newState.page = action.page;
      return newState;
    case SET_STORE_DESC:
      newState.desc = action.desc;
      return newState;
    case SET_STORE_ORDER_BY:
      newState.orderBy = action.orderBy;
      return newState;
    case SET_STORE_MAIN_FILTER:
      newState.displaying = action.displaying;
      newState.category = action.category;
      newState.subFilter = action.subFilter;
      newState.page = 1;
      return newState;
    default:
      return newState
  }
}

export default store;