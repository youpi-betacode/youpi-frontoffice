import { TOGGLE_VALID_FORM } from '../Types';

const initialState = {
  valid: true
};

const validForm = (state = initialState, action) => {
  let newState = {...state};

  switch(action.type){
    case TOGGLE_VALID_FORM:
      newState.valid = action.valid;
      return newState;
    default:
      return newState;
  }
};

export default validForm;