import { SET_PAYMENT_TYPE, GET_PAYMENT_INFO, SET_PAYMENT, LOAD_SALE } from '../Types';

const initialState = {
  payment: {},
  paymentInfo: {},
  paymentType: '',
  sale: null
}

const checkoutPaymentsReducer = (state = initialState, action) => {
  const newState = {...state};

  switch(action.type) {
    case SET_PAYMENT:
      newState.payment = action.payment;
      return newState;
    case GET_PAYMENT_INFO:
      newState.paymentInfo = action.paymentInfo;
      return newState;
    case SET_PAYMENT_TYPE:
      newState.paymentType = action.paymentType;
      return newState;
    case LOAD_SALE:
      newState.sale = action.sale
      return newState;
    default:
      return newState;
  }
}

export default checkoutPaymentsReducer;
