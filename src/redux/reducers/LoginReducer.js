import * as types from '../Types';

const initialState = {
  email: '',
  password: '',
  invalidLogin: false
}

const loginReducer = (state = initialState, action) => {
  let newState = {...state};
  switch(action.type){
    case types.SET_EMAIL_LOGIN:
      newState.email = action.email;
      return newState;
    case types.SET_PASSWORD_LOGIN:
      newState.password = action.password;
      return newState;
    case types.TOGGLE_INVALID_LOGIN:
      newState.invalidLogin = action.invalidLogin;
      return newState;
    default:
      return state;
  }
}

export default loginReducer;
