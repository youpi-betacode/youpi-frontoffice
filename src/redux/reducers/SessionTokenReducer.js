import { SET_SESSION} from '../Types';


const initialState = {
  sessionToken: false
}

const sessionTokenReducer = (state = initialState, action) => {
  const newState = {...state};
  switch(action.type){
    case SET_SESSION:
      newState.sessionToken = action.sessionToken;
      return newState;
    default:
      return newState;
  }
}

export default sessionTokenReducer;
