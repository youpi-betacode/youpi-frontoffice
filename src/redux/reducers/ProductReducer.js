import { SET_PRODUCT, SET_PRODUCT_SIZE, SET_RELATED_PRODUCTS, SET_CURRENT_PRICE, SET_CURRENT_REFERENCE, SET_CURRENT_PRODUCT } from '../Types';

const initialState = {
   product: null,
   sizes: [],
   size: null,
   relatedProducts: [],
   //price: 0,
  // reference: '',
   currentProduct: null
};

const productReducer = (state = initialState, action) => {
   let newState = { ...state };

   switch (action.type) {
      case SET_CURRENT_PRICE:
         newState.price = action.price;
         return newState;
      case SET_CURRENT_REFERENCE:
         newState.reference = action.reference;
         return newState;
      case SET_PRODUCT:
         newState.product = action.product;
         return newState
      case SET_PRODUCT_SIZE:
         newState.size = action.size;
         return newState;
      case SET_RELATED_PRODUCTS:
         newState.relatedProducts = action.relatedProducts;
         return newState;
      case SET_CURRENT_PRODUCT: 
         newState.currentProduct = action.currentProduct;
         return newState;
      default:
         return newState;
   }
}

export default productReducer;
