import * as types from '../Types';

const initialState = {
  firstName: '',
  lastName: '',
  email: '',
  city: '',
  phone: '',
  password: '',
  confirmPassword: '',
  dateOfBirth: null,
  nif: '',
  id: 0,
  modal: false,
  firstNameFilled: false,
  lastNameFilled: false,
  emailFilled: false,
  cityFilled: false,
  dateOfBirthFilled: false,
  nifFilled: false,
  phoneFilled: false,
  passwordFilled: false,
  confirmPasswordFilled: false,
  validPassword: false,
  street: '',
  zipCode: '',
  streetFilled: false,
  zipCodeFilled: false,
  acceptConditions: false,
  alreadyRegistered: false,
  loading: false
}

const registerReducer = (state = initialState, action) => {
  let newState = {...state};

  switch(action.type){
    case types.SET_FIRST_NAME:
      newState.firstName = action.firstName;
      newState.firstNameFilled = action.firstNameFilled;
      return newState;
    case types.SET_LAST_NAME:
      newState.lastName = action.lastName;
      newState.lastNameFilled = action.lastNameFilled;
      return newState;
    case types.SET_EMAIL:
      newState.email = action.email;
      newState.emailFilled = action.emailFilled;
      return newState;
    case types.SET_CITY:
      newState.city = action.city;
      newState.cityFilled = action.cityFilled;
      return newState;
    case types.SET_PHONE:
      newState.phone = action.phone;
      return newState;
    case types.SET_PASSWORD:
      newState.password = action.password;
      newState.passwordFilled = action.passwordFilled;
      return newState;
    case types.SET_CONFIRM_PASSWORD:
      newState.confirmPassword = action.confirmPassword;
      newState.confirmPasswordFilled = action.confirmPassword === newState.password;
      return newState;
    case types.SET_DATE_OF_BIRTH:
      newState.dateOfBirth = action.dateOfBirth
      newState.dateOfBirthFilled = action.dateOfBirthFilled;
      return newState;
    case types.SET_NIF:
      newState.nif = action.nif;
      return newState;
    case types.SET_USER_ID:
      newState.id = action.id;
      return newState;
    case types.TOGGLE_REGISTER_MODAL:
      newState.modal = action.modal;
      return newState;
    case types.TOGGLE_VALID_PASSWORD:
      newState.validPassword = action.validPassword;
      return newState;
    case types.SET_STREET_REGISTER:
      newState.street = action.street;
      newState.streetFilled = action.streetFilled;
      return newState;
    case types.SET_ZIP_CODE_REGISTER:
      newState.zipCode = action.zipCode;
      newState.zipCodeFilled = action.zipCodeFilled;
      return newState;
    case types.TOGGLE_ACCEPT_CONDITIONS:
      newState.acceptConditions = action.acceptConditions;
      return newState;
    case types.TOGGLE_ALREADY_REGISTERED:
      newState.alreadyRegistered = action.alreadyRegistered;
      return newState;
    case types.SET_EMAIL_CHECK_LOADING:
      newState.loading = action.loading;
      return newState;
    default:
      return state;
  }
}

export default registerReducer;
