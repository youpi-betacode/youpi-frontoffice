import React from 'react';
import { Redirect } from 'react-router-dom';

import Routes from './routes';

export default function Main() {
  if(!localStorage.getItem('token'))
    return <Redirect to ='/user/login'/>

  return <Routes/>
}
