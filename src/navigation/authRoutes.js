import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Login from '../pages/login';
import Main from './mainRoutes';

const routes = (
  <Switch>
    <Route path='/user/login' component={Login} />
    <Route component={Main} />
  </Switch>
)

export default routes
