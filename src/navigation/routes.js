import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

const Simulator = Loadable({
    loader: () => import(/* webpackChunkName: "simulador" */ '../pages/simulator'),
    loading: () => null,
    modules: ['simulator'],
    webpack: () => [require.resolveWeak('../pages/simulator')]
});

const Register = Loadable({
    loader: () => import(/* webpackChunkName: "registerReducer" */ '../pages/register'),
    loading: () => null,
    modules: ['register'],
    webpack: () => [require.resolveWeak('../pages/register')]
});

const Login = Loadable({
    loader: () => import(/* webpackChunkName: "loginReducer" */ '../pages/login'),
    loading: () => null,
    modules: ['login'],
    webpack: () => [require.resolveWeak('../pages/login')]
});

const Profile = Loadable({
    loader: () => import(/* webpackChunkName: "profile" */ '../pages/user/profile'),
    loading: () => null,
    modules: ['profile'],
    webpack: () => [require.resolveWeak('../pages/user/profile')]
});

const Simulations = Loadable({
    loader: () => import(/* webpackChunkName: "simulations" */ '../pages/user/simulations'),
    loading: () => null,
    modules: ['simulations'],
    webpack: () => [require.resolveWeak('../pages/user/simulations')]
});

const Store = Loadable({
  loader: () => import(/* webpackChunkName: "store" */ '../pages/store/store'),
  loading: () => null,
  modules: ['store'],
  webpack: () => [require.resolveWeak('../pages/store/store')]
})

const ProductDetail = Loadable({
  loader: () => import(/* webpackChunkName: "productDetail" */ '../pages/store/productDetail'),
  loading: () => null,
  modules: ['productDetail'],
  webpack: () => [require.resolveWeak('../pages/store/productDetail')]
})

const CheckoutSummary = Loadable({
  loader: () => import(/* webpackChunkName: "checkout" */ '../pages/store/checkout/checkoutSummary'),
  loading: () => null,
  modules: ['checkout'],
  webpack: () => [require.resolveWeak('../pages/store/checkout/checkoutSummary')]
})

const CheckoutAddresses = Loadable({
  loader: () => import(/* webpackChunkName: "checkout" */ '../pages/store/checkout/checkoutAddresses'),
  loading: () => null,
  modules: ['checkout'],
  webpack: () => [require.resolveWeak('../pages/store/checkout/checkoutAddresses')]
})

const CheckoutPayment = Loadable({
  loader: () => import(/* webpackChunkName: "checkout" */ '../pages/store/checkout/checkoutPayment'),
  loading: () => null,
  modules: ['checkout'],
  webpack: () => [require.resolveWeak('../pages/store/checkout/checkoutPayment')]
})

const CheckoutFinal = Loadable({
  loader: () => import(/* webpackChunkName: "checkout" */ '../pages/store/checkout/checkoutFinal'),
  loading: () => null,
  modules: ['checkout'],
  webpack: () => [require.resolveWeak('../pages/store/checkout/checkoutFinal')]
})

const History = Loadable({
  loader: () => import(/* webpackChunkName: "profile" */ '../pages/user/history'),
  loading: () => null,
  modules: ['history'],
  webpack: () => [require.resolveWeak('../pages/user/history')]
});

export default () => (
    <Switch>
        <Route exact path="/" component={Store} />
        <Route exact path='/simulator' component={Simulator}></Route>
        <Route exact path="/simulator/:uid" component={Simulator} />
        <Route exact path='/user' component={Profile}/>
        <Route path='/user/history' component={History}/>
        <Route path='/user/simulations' component={Simulations}/>
        <Route path='/user/login' component={Login}/>
        <Route path='/user/register' component={Register}/>
        <Route exact path='/store' component={Store}/>
        <Route exact path='/store/product/:id' component={ProductDetail}/>
        <Route path='/checkout/summary' component={CheckoutSummary}/>
        <Route path='/checkout/addresses' component={CheckoutAddresses}/>
        <Route path='/checkout/payment' component={CheckoutPayment}/>
        <Route path='/checkout/status/:reference' component={CheckoutFinal}/>
    </Switch>
);
