import React, { useState } from 'react';
import {
  Grid, Typography, Button, Container, TextField, Modal,
  InputAdornment, IconButton
} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import clsx from 'clsx';
import { useSelector, useDispatch } from 'react-redux';

import { useTranslation } from 'react-i18next'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { Helmet } from 'react-helmet';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import ModalContent from './modalContent';
import { colors } from '../../constants';
import {
  setEmail,
  setPassword,
  submitSimLogin,
  toggleInvalidLogin,
  submitNoSimLogin,
  recoverPassword
} from '../../redux/actions/LoginActions';
import RegisterModal from '../../common/register/registerModal';
import { setEmail as setEmailRegister, checkEmail, setLoading } from '../../redux/actions/RegisterActions';
import Footer from "../../common/footer/Footer";
import CircularProgress from "@material-ui/core/CircularProgress";

function CreateAccount() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const registerEmail = useSelector(state => state.register.email);
  const [emailError, setEmailError] = useState(false);
  const { t } = useTranslation();
  const routerParams = useSelector(state => state.router.location.search);
  const redirect = routerParams.replace("?return=", "");
  const loading = useSelector(state => state.register.loading);

  const handleOnCreateClick = () => {
    const mailRegExp = /\S+@\S+\.\S+/;
    if (!mailRegExp.test(registerEmail))
      setEmailError(true);
    else {
      setEmailError(false);
      dispatch(setLoading(true))
      dispatch(checkEmail(registerEmail,redirect))
      //dispatch(push('/registerReducer'));
      //dispatch(toggleModal(true));
    }
  }

  const handleChange = (event) => {
    dispatch(setEmailRegister(event.target.value));
  }

  const handleOnKeyUp = (event) => {
    if (event.key === 'Enter')
      handleOnCreateClick();
  }

  return (
    <div className={classes.gridDiv} style={{ width: '80%', height: '63%' }}>
      <Helmet>
        <title>YOUPi! - Your Best Feeling - {t('login-tab-title')}</title>
      </Helmet>
      <Grid item xs={12}>
        <Typography variant='h4' className={classes.title} style={{ marginBottom: 20 }}>
          <strong>{t('dont-have-an-acount-header')}</strong>
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <div>
          <Typography variant='body2' className={classes.labels}>
            {t('mail-label-no-*')}
          </Typography>
          <TextField
            fullWidth
            //className={classes.textField}
            value={registerEmail}
            onChange={handleChange}
            onKeyUp={handleOnKeyUp}
            margin="normal"
            variant="outlined"
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: classes.notchedOutline
              }
            }}
          />
        </div>
        {emailError && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('insert-valid-email')}
          </Typography>
        </div>}
      </Grid>
      <Grid item xs={12}>
        <Grid container>
          <Grid item xs={12}>
            <Button size='large' className={classes.createBtn} onClick={handleOnCreateClick} color="primary"
              disabled={loading}
            >
              <b>{t('create-account')}</b>
            </Button>
          </Grid>

            {
              loading &&
              <Grid item xs={12} style={{marginTop: 20}}>
                <CircularProgress></CircularProgress>
              </Grid>
            }
          </Grid>
      </Grid>
    </div>
  )
}

function LoginForm() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const login = useSelector(state => state.login);
  const simulator = useSelector(state => state.simulator);
  const { t } = useTranslation();
  const [showPassword, setShowPassword] = useState(false);
  const routerParams = useSelector(state => state.router.location.search);
  const redirect = routerParams.replace("?return=", "");
  const hasSim = simulator.filteredPhoto ? true : false;

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleChangeEmail = (event) => {
    dispatch(setEmail(event.target.value));
  };

  const handleChangePassword = (event) => {
    dispatch(setPassword(event.target.value))
  };

  const handleOnSubmit = () => {
    const data = new FormData;
    data.append('identifier', login.email);
    data.append('password', login.password);

    if (hasSim) {

      const simData = {
        'info': {
          'url': simulator.filteredPhoto,
          'pattern': simulator.pattern,
          'gama': simulator.gama,
          'size': simulator.size,
          'margin': simulator.whiteSpace
        }
      };

      dispatch(submitSimLogin(data, simData, redirect));
    } else {
      dispatch(submitNoSimLogin(data, redirect));
    }
  };

  const handleOnKeyUp = (event) => {
    if (event.key === 'Enter')
      handleOnSubmit();
  }

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  const handleForgotPassword = () => {
    Swal.fire({
      title: 'O seu e-mail',
      input: 'email',
      inputAttributes: {
        autocapitalizae: 'off'
      },
      confirmButtonText: t('submit'),
      showLoaderOnConfirm: true,
      customClass: {
        confirmButton: 'youpii-confirm-button-class',
        input: 'youpii-input-text-class'
      },
      inputValidator: (value) => {
        if (!value) {
          return t('invalid-email')
        }
      },
      preConfirm: (email) => {
        dispatch(recoverPassword(email))
    //       .then(response => {
    //         if(!response.ok)
    //           throw new Error(response.statusText);
    //         return response.json()
    //       })
    //       .catch(error => {
    //         return error
    //       })
    //   },
    //   allowOutsideClick: () => !Swal.isLoading()
    // }).then(result => {
    //   if(result.value){
    //     Swal.fire({
    //       title: 'Foi enviada uma nova palavra-passe para o seu e-mail',
    //       confirmButtonText: 'Ok'
    //     })
      }
    })
  }

  return (
    <div className={classes.gridDiv} style={{ width: "80%" }}>
      <Grid item xs={12}>
        <Typography variant='h4' className={classes.title} style={{ marginBottom: 20 }}>
          <strong>{t('login-header')}</strong>
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Typography variant='body2' className={classes.labels}>
          {t('mail-label-no-*')}
        </Typography>
        <TextField
          fullWidth
          value={login.email}
          onKeyUp={handleOnKeyUp}
          onChange={handleChangeEmail}
          margin="normal"
          variant="outlined"
          InputProps={{
            classes: {
              root: classes.text,
              notchedOutline: classes.notchedOutline
            }
          }}
        />
      </Grid>
      <Grid item xs={12}>
        <Typography variant='body2' className={classes.labels}>
          {t('password-label')}
        </Typography>
        <TextField
          fullWidth
          type={showPassword ? 'text' : 'password'}
          value={login.password}
          onKeyUp={handleOnKeyUp}
          onChange={handleChangePassword}
          margin="normal"
          variant="outlined"
          InputProps={{
            classes: {
              root: classes.text,
              notchedOutline: classes.notchedOutline
            },
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  edge="end"
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            )
          }}
        />
      </Grid>
      <Grid item xs={12} className={classes.forgotPassword}>
        <Typography variant='body2' className={classes.forgotText} onClick={handleForgotPassword}>
          {t('forgot-password')}
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Button size='large' className={classes.loginBtn} onClick={handleOnSubmit} color="primary">
          <b>{t('login-button')}</b>
        </Button>
      </Grid>
    </div>
  )
}

export default function Login() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const login = useSelector(state => state.login);
  const ref = React.createRef();
  const from = useSelector(state => state.router.location.search);

  const handleOnCloseModal = () => {
    dispatch(toggleInvalidLogin(false));
  };

  const redirect = from === '?return=/checkout' ? true : false;

  return (
    <div>
      <div className={classes.banner}>
        <h2 className={classes.bannerText}>Enquadre o seu <b className={classes.bannerTextBold}>coração</b></h2>
      </div>
      {/* <Header title='A NOSSA LOJA' subTitle='Não existe um caminho para a felicidade. A felicidade é o caminho' /> */}
      <Container className={classes.container}>
        <Grid container>
          <Grid item xs={12} md={6} className={clsx(classes.item, classes.left)}>
            <Grid container style={{ height: '100%' }} direction='column' justify='space-around' alignItems='center'>
              <CreateAccount redirect={redirect}/>
            </Grid>
          </Grid>
          <Grid item xs={12} md={6} className={classes.item} style={{ backgroundColor: colors.backgroundGrey }}>
            <Grid container style={{ height: '100%' }} direction='column' justify='center' alignItems='center'>
              <LoginForm redirect={redirect}/>
            </Grid>
          </Grid>
        </Grid>
      </Container>
      <Modal open={login.invalidLogin} onClose={handleOnCloseModal}>
        <ModalContent ref={ref} />
      </Modal>
      <RegisterModal />
      <Footer />
    </div>
  )
}

const useStyles = makeStyles(theme => ({
  bannerTextBold: {
    display: "block",
    fontWeight: 800
  },
  bannerText: {
    color: "#fff",
    position: "absolute",
    right: 0,
    top: 147,
    height: 100,
    width: 460,
    padding: 20,
    textTransform: "uppercase",
    fontSize: 22,
    fontWeight: 100,
    /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#7a5e44+0,e4ae81+100 */
    background: "#7a5e44", /* Old browsers */
    background: "-moz-linear-gradient(left,  #7a5e44 0%, #e4ae81 100%)", /* FF3.6-15 */
    background: "-webkit-linear-gradient(left,  #7a5e44 0%,#e4ae81 100%)", /* Chrome10-25,Safari5.1-6 */
    background: "linear-gradient(to right,  #7a5e44 0%,#e4ae81 100%)", /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: "progid:DXImageTransform.Microsoft.gradient( startColorstr='#7a5e44', endColorstr='#e4ae81',GradientType=1 )", /* IE6-9 */
    [theme.breakpoints.down('sm')]: {
      width: 300,
    }
  },
  banner: {
    backgroundImage: "url('/images/banner_youpinow.jpg')",
    height: 300,
    backgroundPosition: "center",
    backgroundSize: "cover"
  },

  item: {
    border: '1px solid ' + colors.borderGrey,
    borderRadius: 10,
    height: 400
  },
  createBtn: {
    marginTop: 20,
    color: colors.white,
    padding: '10px 40px',
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.8)'
    }
  },
  container: {
    marginTop: 50,
    marginBottom: 50,
  },
  left: {
    backgroundColor: colors.white,
  },
  title: {
    color: colors.primary,
    fontSize: 18,
    marginBottom: 20
  },
  subtitle: {
    color: "#86eed5",
    fontFamily: "ComfortaaBold",
    fontSize: 15,
    marginBottom: 20,
    marginLeft: 5,
    marginRight: 5
  },
  gridDiv: {
    textAlign: 'center',
  },
  notchedOutline: {
    borderWidth: '1px',
    borderColor: colors.primary + ' !important',
    borderRadius: 7,
  },
  loginBtn: {
    marginTop: 20,
    color: colors.white,
    padding: '10px 40px',
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.8)'
    }
  },
  text: {
    color: `${colors.htmlGrey} !important`
  },
  labels: {
    color: colors.primary,
    marginBottom: '-10px',
    marginTop: 10
  },
  errorGrid: {
    textAlign: 'left',
    color: `${colors.errorPink} !important`,
    marginBottom: 10
  },
  labels: {
    color: colors.primary,
    marginBottom: '-10px',
    marginTop: 10,
    textAlign: 'left'
  },
  forgotPassword: {
    marginTop: 10
  },
  forgotText: {
    cursor: 'pointer',
    '&:hover':{
      color: colors.successBlue
    }
  }
}));
