import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Typography, Grid, Button } from '@material-ui/core';
import { useDispatch } from 'react-redux';

import { colors } from '../../constants';
import { toggleInvalidLogin } from '../../redux/actions/LoginActions';

const ModalContent = React.forwardRef((props,ref) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const handleOnClick = () => {
    dispatch(toggleInvalidLogin(false));
  };

  return (
    <div className={classes.content}>
      <Grid container className={classes.gridContainer} direction='column' justify='center' alignItems='center'>
        <Typography variant='h4'>
          Credenciais inválidas
      </Typography>
      <Button size='large' onClick={handleOnClick} className={classes.btn}>
        <b>Ok</b>
      </Button>
      </Grid>
    </div>
  )
})

export default ModalContent;

const useStyles = makeStyles(theme => ({
  content: {
    //width: '30%',
    height: '20%',
    backgroundColor: colors.white,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 4),
    outline: 'none',
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    textAlign: 'center'
  },
  gridContainer: {
    height: '100%'
  },
  btn: {
    padding: '10px 40px',
    marginTop: 40,
    backgroundColor: colors.primary,
    color: colors.white,
    '&:hover': {
      backgroundColor: colors.lightPrimary
    }
  }
}))
