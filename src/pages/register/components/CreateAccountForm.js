import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, TextField, Typography, InputAdornment, IconButton } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

import * as register from '../../../redux/actions/RegisterActions';
import { toggleValidForm } from '../../../redux/actions/FormValidationActions';
import { colors } from '../../../constants';

function CreateAccountForm() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const registerForm = useSelector(state => state.register);
  const valid = useSelector(state => state.formValidation.valid);
  const { t } = useTranslation();
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmation, setShowConfirmation] = useState(false);

  const handleChangeFirstName = (event) => {
    dispatch(register.setFirstName(event.target.value));
  //  dispatch(toggleValidForm(true));
    dispatch(register.toggleValidPassword(false));
  }

  const handleChangeLastName = (event) => {
   // dispatch(toggleValidForm(true));
    dispatch(register.toggleValidPassword(false));
    dispatch(register.setLastName(event.target.value));
  }

  // const handleChangeEmail = (event) => {
  //  // dispatch(toggleValidForm(true));
  //   dispatch(registerReducer.toggleValidPassword(false));
  //   dispatch(registerReducer.setEmail(event.target.value));
  // }

  const handleChangeStreet = (event) => {
    dispatch(register.toggleValidPassword(false));
    dispatch(register.setStreet(event.target.value));
  };

  const handleChangeZipCode = (event) => {
    dispatch(register.toggleValidPassword(false));
    dispatch(register.setZipCode(event.target.value));
  }

  const handleChangeCity = (event) => {
   // dispatch(toggleValidForm(true));
    dispatch(register.toggleValidPassword(false));
    dispatch(register.setCity(event.target.value));
  }

  const handleChangePhone = (event) => {
    dispatch(register.setPhone(event.target.value));
  }

  const handleChangeDateOfBirth = (date) => {
   // dispatch(toggleValidForm(true));
    dispatch(register.toggleValidPassword(false));
    dispatch(register.setDateOfBirth(date));
  }

  const handleChangeNif = (event) => {
    dispatch(register.setNif(event.target.value));
  }

  const handleChangePassword = (event) => {
    dispatch(register.setPassword(event.target.value));
    dispatch(register.toggleValidPassword(false));
   // dispatch(toggleValidForm(true))
  };


  const handleChangeConfirmPassword = (event) => {
    dispatch(register.setConfirmPassword(event.target.value));
    //dispatch(toggleValidForm(true));
    //dispatch(registerReducer.toggleValidPassword(false));
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  }

  const handleClickShowConfirmation = () => {
    setShowConfirmation(!showConfirmation);
  }

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  return (
    <div className={classes.gridDiv}>
      <Grid item xs={12}>
        <div>
        <Typography variant='body2' className={classes.labels}>
          {t('mail-label-no-*')}
        </Typography>
        <Typography variant='body2' style={{textAlign: 'left', marginTop: 15}}>
          {registerForm.email}
        </Typography>
        </div>
        {!(valid || registerForm.emailFilled) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('insert-email-error')}
          </Typography>
          </div>}
      </Grid>
      <Grid item xs={12}>
        <div>
          <Typography variant='body2' className={classes.labels}>
            {t('first-name-label')}
          </Typography>
          <TextField
            required
            wrap='true'
            placeholder={t('first-name-placeholder')}
            fullWidth
            //className={classes.textField}
            value={registerForm.firstName}
            onChange={handleChangeFirstName}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || registerForm.firstNameFilled) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
        </div>
        {!(valid || registerForm.firstNameFilled) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('insert-first-name-error')}
          </Typography>
        </div>}
      </Grid>
      <Grid item xs={12}>
        <div>
        <Typography variant='body2' className={classes.labels}>
            {t('last-name-label')}
          </Typography>
          <TextField
            required
            wrap='true'
            fullWidth
            placeholder={t('last-name-placeholder')}
            //className={classes.textField}
            value={registerForm.lastName}
            onChange={handleChangeLastName}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || registerForm.lastNameFilled) ? classes.notchedOutline : classes.errorNotchedOutline
              },
              shrink: true
            }}
          />
        </div>
        {!(valid || registerForm.lastNameFilled) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('insert-last-name-error')}
          </Typography>
        </div>}
      </Grid>
      {/* <Grid item xs={12}>
        <div>
          <TextField
            required
            wrap='true'
            fullWidth
            label="EMAIL"
            //className={classes.textField}
            value={registerForm.email}
            onChange={handleChangeEmail}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || registerForm.emailFilled) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
        </div>
        {!(valid || registerForm.emailFilled) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('insert-email-error')}
          </Typography>
        </div>}
      </Grid> */}
      <Grid item xs={12}>
        <div>
        <Typography variant='body2' className={classes.labels}>
            {t('date-of-birth-label')}
          </Typography>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
            className={classes.datePickerInput}
            inputVariant="outlined"
            disableFuture={true}
            variant='dialog'
            format="dd/MM/yyyy"
            margin="normal"
            id="date-picker-dialog"
            format="dd/MM/yyyy"
            value={registerForm.dateOfBirth}
            label=""
            onChange={handleChangeDateOfBirth}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || registerForm.dateOfBirthFilled) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
          </MuiPickersUtilsProvider>
          {/* <TextField
            required
            fullWidth
            wrap='true'
            placeholder={t('date-format-placeholder')}
            //className={classes.textField}
            value={registerForm.dateOfBirth}
            onChange={handleChangeDateOfBirth}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || registerForm.dateOfBirthFilled) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          /> */}
        </div>
        {!(valid || registerForm.dateOfBirthFilled) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('insert-date-of-birth-error')}
          </Typography>
        </div>}
      </Grid>
      <Grid item xs={12}>
        <div>
        <Typography variant='body2' className={classes.labels}>
            {t('address-label')}
          </Typography>
          <TextField
            required
            wrap='true'
            fullWidth
            placeholder={t('street-placeholder')}
            //className={classes.textField}
            value={registerForm.street}
            onChange={handleChangeStreet}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || registerForm.streetFilled) ? classes.notchedOutline : classes.errorNotchedOutline
              },
              shrink: true
            }}
          />
        </div>
        {!(valid || registerForm.streetFilled) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('insert-street-error')}
          </Typography>
        </div>}
      </Grid>
      <Grid container direction='row' justify='space-between'>
        <Grid item xs={12} md={5}>
          <div>
          <TextField
            fullWidth
            placeholder={t('zip-code-placeholder')}
            //className={classes.textField}
            value={registerForm.zipCode}
            onChange={handleChangeZipCode}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || registerForm.zipCodeFilled) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
          </div>
          {!(valid || registerForm.zipCodeFilled) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('insert-zip-code-error')}
          </Typography>
        </div>}
        </Grid>
        <Grid item xs={12} md={5}>
          <div>
          <TextField
            fullWidth
            placeholder={t('city-placeholder')}
            //className={classes.textField}
            value={registerForm.city}
            onChange={handleChangeCity}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || registerForm.cityFilled) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
          </div>
          {!(valid || registerForm.cityFilled) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('insert-city-error')}
          </Typography>
        </div>}
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <div>
        <Typography variant='body2' className={classes.labels}>
            {t('phone-label')}
          </Typography>
        <TextField
          fullWidth
          wrap='true'
          placeholder={t('phone-placeholder')}
          //className={classes.textField}
          value={registerForm.phone}
          onChange={handleChangePhone}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            classes: {
              root: classes.cssLabel
            },
            shrink: true
          }}
          InputProps={{
            classes: {
              root: classes.text,
              notchedOutline: classes.notchedOutline
            }
          }}
        />
        </div>
      </Grid>
      <Grid item xs={12}>
        <div>
          <Typography variant='body2' className={classes.labels}>
            {t('nif-label')}
          </Typography>
        <TextField
          fullWidth
          wrap='true'
          placeholder={t('nif-placeholder')}
          //className={classes.textField}
          value={registerForm.nif}
          onChange={handleChangeNif}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            classes: {
              root: classes.cssLabel
            },
            shrink: true
          }}
          InputProps={{
            classes: {
              root: classes.text,
              notchedOutline: classes.notchedOutline
            }
          }}
        />
        </div>
      </Grid>
      <Grid item xs={12}>
        <div>
        <Typography variant='body2' className={classes.labels}>
            {t('insert-password-label')}
          </Typography>
          <TextField
            required
            fullWidth
            type={showPassword ? 'text' : 'password'}
            placeholder={t('password-placeholder')}
            //className={classes.textField}
            value={registerForm.password}
            onChange={handleChangePassword}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || registerForm.passwordFilled) ? classes.notchedOutline : classes.errorNotchedOutline
              },
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    edge="end"
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </div>
        {registerForm.validPassword && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('password-rules')}
          </Typography>
        </div>}
        {!(valid || registerForm.password) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('insert-password-error')}
          </Typography>
        </div>}
      </Grid>
      <Grid item xs={12}>
        <div>
        <Typography variant='body2' className={classes.labels}>
            {t('confirm-password-label')}
          </Typography>
          <TextField
            required
            fullWidth
            type={showConfirmation ? 'text' : 'password'}
            placeholder={t('confirm-password-placeholder')}
            //className={classes.textField}
            value={registerForm.confirmPassword}
            onChange={handleChangeConfirmPassword}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || registerForm.confirmPassword === registerForm.password) ? classes.notchedOutline : classes.errorNotchedOutline
              },
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    edge="end"
                    aria-label="toggle password visibility"
                    onClick={handleClickShowConfirmation}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {showConfirmation ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </div>
        {!(valid || registerForm.confirmPassword === registerForm.password) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('passwords-dont-match-error')}
          </Typography>
        </div>}
      </Grid>
    </div>
  )
}

export default CreateAccountForm;

const useStyles = makeStyles(theme => ({
  notchedOutline: {
    borderWidth: '1px',
    borderColor: colors.primary + ' !important',
    borderRadius: 7,
  },
  errorNotchedOutline: {
    borderWidth: '1px',
    borderColor: colors.errorPink + ' !important',
    borderRadius: 7,
  },
  cssLabel: {
  },
  gridDiv: {
    textAlign: 'center',
    width: '80%'
  },
  errorGrid: {
    textAlign: 'left',
    color: `${colors.errorPink} !important`,
    marginBottom: 10
  },
  text: {
    color: `${colors.htmlGrey} !important`
  },
  labels: {
    color: colors.primary,
    marginBottom: '-10px',
    marginTop: 10,
    textAlign: 'left'
  },
  datePickerInput: {
    width: '100%'
  }
}))
