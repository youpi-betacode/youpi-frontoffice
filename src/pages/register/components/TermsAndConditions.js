import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Checkbox, Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import { toggleAcceptConditions } from '../../../redux/actions/RegisterActions';
import { colors } from '../../../constants';

export default function TermsAndConditions() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const accepted = useSelector(state => state.register.acceptConditions);
  const valid = useSelector(state => state.formValidation.valid);
  //const [accepted, setAccepted] = React.useState(false);
  const { t } = useTranslation();

  const handleAcceptChange = (value) => {
    dispatch(toggleAcceptConditions(value));

  }

  return (
    <Grid container justify={'center'} style={{marginBottom: 20}}>
      <Grid item xs={12}>
        <Checkbox
          checked={accepted}
          onChange={(event)=>handleAcceptChange(event.target.checked)}
          color="primary"
        />
        {t('data-exchange-accept')}
      </Grid>
      {!(valid || accepted) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('must-accept-terms-and-conditions')}
          </Typography>
        </div>}
    </Grid>
  )
};

const useStyles = makeStyles(theme => ({
  errorGrid: {
    textAlign: 'left',
    color: `${colors.errorPink} !important`,
    marginBottom: 10
  },
}));

