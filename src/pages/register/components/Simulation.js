import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Typography, Button } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import {useSnackbar} from 'notistack';
import { useTranslation } from 'react-i18next';

import { submitRegister, toggleValidPassword, submitNoSimRegister } from '../../../redux/actions/RegisterActions';
import { colors } from '../../../constants';
import { toggleValidForm } from '../../../redux/actions/FormValidationActions';
import FrameCanvas from '../../../common/frame/FrameCanvas';


// function NoSimulation() {
//   const dispatch = useDispatch();
//   const classes = useStyles();
//   const registerForm = useSelector(state => state.registerReducer);
//   const { t } = useTranslation();
//   const {enqueueSnackbar} = useSnackbar();

//   const handleSubmit = () => {

//     const phone = registerForm.phone ? registerForm.phone : 0;
//     const nif = registerForm.nif ? registerForm.nif : 0;

//     const data = {
//       'email': registerForm.email,
//       'password': registerForm.password,
//       'first_name': registerForm.firstName,
//       'last_name': registerForm.lastName,
//       'local': registerForm.city,
//       'contact': phone,
//       'extra': {
//         'nif': nif,
//         'date_of_birth': registerForm.dateOfBirth
//       }
//     }

//     const loginReducer = {
//       'identifier': registerForm.email,
//       'password': registerForm.password
//     }

//     // if(!registerForm.firstNameFilled)
//     //   enqueueSnackbar(t('insert-first-name-error'),{
//     //     anchorOrigin: {
//     //       vertical: 'top',
//     //       horizontal: 'right',
//     //     },
//     //     variant: 'error',
//     //     autoHideDuration: 3000
//     //   });

//     //   if(!registerForm.lastNameFilled)
//     //   enqueueSnackbar(t('insert-last-name-error'),{
//     //     anchorOrigin: {
//     //       vertical: 'top',
//     //       horizontal: 'right',
//     //     },
//     //     variant: 'error',
//     //     autoHideDuration: 3000
//     //   });

//     //   if(!registerForm.emailFilled)
//     //   enqueueSnackbar(t('insert-email-error'),{
//     //     anchorOrigin: {
//     //       vertical: 'top',
//     //       horizontal: 'right',
//     //     },
//     //     variant: 'error',
//     //     autoHideDuration: 3000
//     //   });

//     //   if(!registerForm.cityFilled)
//     //   enqueueSnackbar(t('insert-city-error'),{
//     //     anchorOrigin: {
//     //       vertical: 'top',
//     //       horizontal: 'right',
//     //     },
//     //     variant: 'error',
//     //     autoHideDuration: 3000
//     //   });

//     //   if(!registerForm.dateOfBirthFilled)
//     //   enqueueSnackbar(t('insert-date-of-birth-error'),{
//     //     anchorOrigin: {
//     //       vertical: 'top',
//     //       horizontal: 'right',
//     //     },
//     //     variant: 'error',
//     //     autoHideDuration: 3000
//     //   });

//     //   if(!registerForm.passwordFilled)
//     //   enqueueSnackbar(t('insert-password-error'),{
//     //     anchorOrigin: {
//     //       vertical: 'top',
//     //       horizontal: 'right',
//     //     },
//     //     variant: 'error',
//     //     autoHideDuration: 3000
//     //   });

//     //   if(registerForm.password !== registerForm.confirmPassword)
//     //   enqueueSnackbar(t('passwords-dont-match-error'),{
//     //     anchorOrigin: {
//     //       vertical: 'top',
//     //       horizontal: 'right',
//     //     },
//     //     variant: 'error',
//     //     autoHideDuration: 3000
//     //   });

//       if(registerForm.firstNameFilled && registerForm.lastNameFilled && registerForm.emailFilled && registerForm.cityFilled && registerForm.dateOfBirthFilled && registerForm.passwordFilled && registerForm.password === registerForm.confirmPassword){
//         dispatch(toggleValidForm(true));
//         dispatch(submitRegister(data,loginReducer));
//       }
//     else
//     dispatch(toggleValidForm(false));
//   };

//   return (
//     <div className={classes.gridDiv}>
//       <Grid item xs={12}>
//         <h1>Não tem nenhuma simulação</h1>
//       </Grid>
//       <Grid item xs={12}>
//         <Button size='large' className={classes.btn} onClick={handleSubmit}>
//           <b>Criar Conta</b>
//         </Button>
//       </Grid>
//     </div>
//   )
// }

function Simulation() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const registerForm = useSelector(state => state.register);
  const simulator = useSelector(state => state.simulator);
  const { t } = useTranslation();
  //const {enqueueSnackbar} = useSnackbar();

  // const hasSim = simulatorReducer.filteredPhoto ? true : false;

  const handleSubmit = () => {

    const phone = registerForm.phone ? registerForm.phone : 0;
    const nif = registerForm.nif ? registerForm.nif : 0;
    const passwordRegExp = /^.*(?=.{8,})(?=.*[A-Za-z])(?=.*\d)(?=.*[\W])*.*$/;

    const data = {
      'email': registerForm.email,
      'password': registerForm.password,
      'first_name': registerForm.firstName,
      'last_name': registerForm.lastName,
      'local': registerForm.city,
      'contact': phone,
      'extra': {
        'nif': nif,
        'date_of_birth': registerForm.dateOfBirth,
        'name': `${registerForm.firstName} ${registerForm.lastName}`
      }
    }

    const info = {
      'info': {
        'url': simulator.filteredPhoto,
        'pattern': simulator.pattern,
        'gama': simulator.gama,
        'size': simulator.size,
        'margin': simulator.whiteSpace,
      }
    };

    const login = {
      'identifier': registerForm.email,
      'password': registerForm.password
    }

    // if(!registerForm.firstNameFilled)
    //   enqueueSnackbar(t('insert-first-name-error'),{
    //     anchorOrigin: {
    //       vertical: 'top',
    //       horizontal: 'right',
    //     },
    //     variant: 'error',
    //     autoHideDuration: 3000
    //   });

    //   if(!registerForm.lastNameFilled)
    //   enqueueSnackbar(t('insert-last-name-error'),{
    //     anchorOrigin: {
    //       vertical: 'top',
    //       horizontal: 'right',
    //     },
    //     variant: 'error',
    //     autoHideDuration: 3000
    //   });

    //   if(!registerForm.emailFilled)
    //   enqueueSnackbar(t('insert-email-error'),{
    //     anchorOrigin: {
    //       vertical: 'top',
    //       horizontal: 'right',
    //     },
    //     variant: 'error',
    //     autoHideDuration: 3000
    //   });

    //   if(!registerForm.cityFilled)
    //   enqueueSnackbar(t('insert-city-error'),{
    //     anchorOrigin: {
    //       vertical: 'top',
    //       horizontal: 'right',
    //     },
    //     variant: 'error',
    //     autoHideDuration: 3000
    //   });

    //   if(!registerForm.dateOfBirthFilled)
    //   enqueueSnackbar(t('insert-date-of-birth-error'),{
    //     anchorOrigin: {
    //       vertical: 'top',
    //       horizontal: 'right',
    //     },
    //     variant: 'error',
    //     autoHideDuration: 3000
    //   });

    //   if(!registerForm.passwordFilled)
    //   enqueueSnackbar(t('insert-password-error'),{
    //     anchorOrigin: {
    //       vertical: 'top',
    //       horizontal: 'right',
    //     },
    //     variant: 'error',
    //     autoHideDuration: 3000
    //   });

    //   if(registerForm.password !== registerForm.confirmPassword)
    //   enqueueSnackbar(t('passwords-dont-match-error'),{
    //     anchorOrigin: {
    //       vertical: 'top',
    //       horizontal: 'right',
    //     },
    //     variant: 'error',
    //     autoHideDuration: 3000
    //   });


    if(!passwordRegExp.test(registerForm.password))
      dispatch(toggleValidPassword(true));

      if(registerForm.firstNameFilled && registerForm.lastNameFilled && registerForm.emailFilled && registerForm.cityFilled && registerForm.dateOfBirthFilled && registerForm.passwordFilled && registerForm.password === registerForm.confirmPassword && passwordRegExp.test(registerForm.password)){
      dispatch(toggleValidForm(true));
      dispatch(submitRegister(data,info,login));
    }
    else
      dispatch(toggleValidForm(false));
  };

  // if (!hasSim)
  //   return (
  //     <NoSimulation />
  //   )

  return (
    <div className={classes.gridDiv}>
      <Grid item xs={12}>
        <FrameCanvas image={simulator.filteredPhoto} pattern={simulator.pattern.frame} />
      </Grid>
      <Grid item xs={12} className={classes.details}>
        <Typography variant='body1'>
          <strong>{t('simulation-size')}</strong> {simulator.size[1]}
        </Typography>
        <Typography variant='body1'>
          <strong>{t('simulation-gama')}</strong> {t(simulator.gama)}
        </Typography>
        <Typography variant='body1'>
          <strong>{t('simulation-pattern')}</strong> {simulator.pattern.label}
        </Typography>
        <Typography variant='body1'>
          <strong>{t('simulation-margin')} </strong> {simulator.whiteSpace ? 'Sim' : 'Não'}
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Button size='large' className={classes.btn} onClick={handleSubmit}>
          <b>{t('register-button')}</b>
        </Button>
      </Grid>
    </div>
  )
}

export default Simulation;

const useStyles = makeStyles(theme => ({
  gridDiv: {
    textAlign: 'center'
  },
  image: {
    height: 320,
    width: 320
  },
  details: {
    textAlign: 'left',
    marginTop: 20,
    color: colors.black
  },
  btn: {
    marginTop: 40,
    backgroundImage: "url('/images/fundo_header.jpg')",
    backgroundRepeat: "repeat",
    backgroundPosition: "center",
    backgroundSize: "cover",
    color: colors.white,
    padding: '10px 40px',
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.8)'
    }
  }
}))
