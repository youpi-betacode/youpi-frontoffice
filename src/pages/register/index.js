import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Container, Grid, Typography, Button } from '@material-ui/core';
import clsx from 'clsx';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { Helmet } from 'react-helmet';
import { useSnackbar } from 'notistack';

// import Header from '../../common/header';
import CreateAccountForm from './components/CreateAccountForm';
import Simulation from './components/Simulation';
import { submitNoSimRegister, toggleValidPassword, submitRegister } from '../../redux/actions/RegisterActions';
import { toggleValidForm } from '../../redux/actions/FormValidationActions';
import { colors } from '../../constants';
import Footer from "../../common/footer/Footer";
import TermsAndConditions from './components/TermsAndConditions';


function Register() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const sim = useSelector(state => state.simulator.filteredPhoto);
  const registerForm = useSelector(state => state.register);
  const simulator = useSelector(state => state.simulator);
  const passwordRegExp = /^.*(?=.{6,})(?=.*[A-Za-z])(?=.*\d)(?=.*[\W])*.*$/;
  const { enqueueSnackbar } = useSnackbar();

  const currentURL = useSelector(state => state.router.location.search);

  const redirect = currentURL.includes('return=/checkout');

  const hasSim = sim ? true : false;

  const handleSubmit = () => {
    const phone = registerForm.phone ? registerForm.phone : 0;
    const nif = registerForm.nif ? registerForm.nif : 0;

    const data = {
      'email': registerForm.email,
      'password': registerForm.password,
      'first_name': registerForm.firstName,
      'last_name': registerForm.lastName,
      'local': registerForm.city,
      'contact': phone,
      'extra': {
        'nif': nif,
        'date_of_birth': registerForm.dateOfBirth,
        'name': `${registerForm.firstName} ${registerForm.lastName}`,
        'zipCode': registerForm.zipCode,
        'street': registerForm.street
      }
    };

    const simData = {
      'info': {
        'url': simulator.filteredPhoto,
        'pattern': simulator.pattern,
        'gama': simulator.gama,
        'size': simulator.size,
        'margin': simulator.whiteSpace,
      }
    };

    const login = {
      'identifier': registerForm.email,
      'password': registerForm.password
    };

    if(!passwordRegExp.test(registerForm.password))
      dispatch(toggleValidPassword(true));

    if(registerForm.firstNameFilled && registerForm.lastNameFilled && registerForm.emailFilled && registerForm.cityFilled &&
      registerForm.dateOfBirthFilled && registerForm.passwordFilled && registerForm.password === registerForm.confirmPassword &&
      passwordRegExp.test(registerForm.password) && registerForm.zipCodeFilled && registerForm.streetFilled && registerForm.acceptConditions){
      if(!hasSim)
        dispatch(submitNoSimRegister(data,login,redirect));
      else
        dispatch(submitRegister(data,simData,login,redirect))
    }
    else{
      dispatch(toggleValidForm(false));
      enqueueSnackbar(t('fill-required-fields'), {
        variant: 'error',
        anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
        },
        autoHideDuration: 5000
      });
    }
  }

  return (
    <div>
      <Helmet>
        <title>YOUPi! - Your Best Feeling - {t('register-tab-title')}</title>
      </Helmet>
      {/* <Header title='A NOSSA LOJA' subTitle='Não existe um caminho para a felicidade. A felicidade é o caminho' /> */}
      <Container className={classes.container}>
        <Grid container>
          <Grid item xs={12} md={12} className={classes.item}>
            <div className={classes.gridTitle}>
              <Typography variant='h4' className={classes.title}>
                <b>{t('simulation-register-header')}</b>
              </Typography>
              <Typography variant='p' component="p" className={classes.subtitle}>
                {t('simulation-register-subheader')}
              </Typography>
            </div>
            <Grid container className={classes.innerContainer} direction='column' justify='center' alignItems='center'>
              <CreateAccountForm />
            </Grid>
            <Grid item xs={12} style={{textAlign: 'center'}}>
              <TermsAndConditions/>
            </Grid>
            {/* {!hasSim && */}
              <Grid item xs={12} style={{textAlign: 'center'}}>
                <Button size='large' className={classes.btn} onClick={handleSubmit} color="primary">
                  <b>{t('register-button')}</b>
                </Button>
              </Grid>
            {/* } */}

          </Grid>

          {/* {hasSim &&
            <Grid item xs={12} md={6} className={clsx(classes.item, classes.right)}>
              <div className={classes.gridTitle}>
                <Typography variant='h4' className={classes.title}>
                  <b>Resumo Simulação</b>
                </Typography>
              </div>
              <Grid container className={classes.innerContainer} direction='column' justify='center' alignItems='center'>
                <Simulation />
              </Grid>
            </Grid>
          } */}
        </Grid>
      </Container>
      <Footer/>
    </div>
  )
}

export default Register;

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 50,
    marginBottom: 50,
  },
  innerContainer: {
    marginBottom: 15,
    maxWidth: 700,
    margin: 'auto'
  },
  item: {
    border: '1px solid ' + colors.borderGrey,
    borderRadius: 10
  },
  title: {
    color: colors.primary,
    fontSize: 18
  },
  subtitle: {
    marginTop: 10,
    color: "#86eed5",
    fontFamily: "ComfortaaBold",
    fontSize: 15,
    marginBottom: 20
  },
  gridTitle: {
    marginTop: 50,
    marginBottom: 30,
    marginRight: 10,
    marginLeft: 10,
    textAlign: "center"
  },
  right: {
    backgroundColor: colors.borderGrey
  },
  btn: {
    marginBottom: 20,
    color: colors.white,
    padding: '10px 40px',
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.8)'
    }
  }
}))

