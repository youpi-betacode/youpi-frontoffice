import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { Icon, Fab } from '@material-ui/core';

import { colors, fileManagerApi } from '../../constants';

import { setStep } from '../../redux/actions/SimulatorStepsActions';
import Breadcrumbs from './components/Breadcrumbs';
import StageSelector from './components/StageSelector'
import { resetFilter } from '../../redux/actions/SimulatorActions';
import Grid from '@material-ui/core/Grid';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import 'sweetalert2/dist/sweetalert2.css'
import Container from "@material-ui/core/Container";
import { useTranslation } from 'react-i18next';

import Hidden from "@material-ui/core/Hidden";
import { Helmet } from "react-helmet";
import { animateScroll as scroll } from 'react-scroll'
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import useTheme from "@material-ui/core/styles/useTheme";
import Footer from "../../common/footer/Footer";

function SimulatorNavigationMobile() {

  const classes = useStyles();
  const theme = useTheme();
  const step = useSelector(state => state.simulatorSteps.step);
  const picture = useSelector(state => state.simulator.picture);
  const pattern = useSelector(state => state.simulator.pattern);
  const size = useSelector(state => state.simulator.size);
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));


  const scrollToSimulatorBeginning = () => {
    if (isSmallDevice) {
      scroll.scrollTo(300);
    }
  };

  const nextStep = () => {
    if (step === 0 && !picture) {
      Swal.fire({
        imageUrl: 'images/error.png',
        imageWidth: 50,
        imageHeight: 50,
        confirmButtonColor: colors.primary,
        customClass: {
          confirmButton: 'youpii-confirm-button-class',
        },
        text: t('no-photo-upload-error')
      })
    } else if (step === 2 && !size) {
      Swal.fire({
        imageUrl: 'images/error.png',
        imageWidth: 50,
        imageHeight: 50,
        confirmButtonColor: colors.primary,
        customClass: {
          confirmButton: 'youpii-confirm-button-class',
        },
        text: t('no-size-selected')
      })
    } else if (step === 3 && !pattern) {
      Swal.fire({
        imageUrl: 'images/error.png',
        imageWidth: 50,
        imageHeight: 50,
        confirmButtonColor: colors.primary,
        customClass: {
          confirmButton: 'youpii-confirm-button-class',
        },
        text: t('no-pattern-selector')
      })
    } else {
      scrollToSimulatorBeginning();
      dispatch(setStep(step + 1))
    }
  };

  const previowsStep = () => {
    scrollToSimulatorBeginning();
    dispatch(setStep(step - 1));
  };

  const onClickResetFilter = () => {
    dispatch(resetFilter())
  };

  if (!isSmallDevice) {
    return null;
  } else {
    return (
      <Grid container>
        <Grid item xs={6} className={classes.marginTop}>
          {step > 0 &&
            <Fab className={classes.buttonLeft} onClick={previowsStep}>
              <Icon>chevron_left</Icon>
            </Fab>
          }
          <Hidden mdUp>
            {step > 1 && step < 4 &&
              <Fab className={classes.buttonLeft} style={{ marginLeft: 10 }} onClick={onClickResetFilter}>
                <Icon>format_color_reset</Icon>
              </Fab>
            }
          </Hidden>
          {/* <Hidden smDown>
            {step > 1 && step < 4 &&
              <Button variant="outlined" color="primary" className={classes.button} onClick={onClickResetFilter}>
                {t('reset-filter')}
              </Button>
            }
          </Hidden> */}
        </Grid>
        <Grid item xs={6} className={classes.marginTop}>
          {step < 4 &&
            <Fab className={classes.buttonRight} onClick={nextStep}>
              <Icon>chevron_right</Icon>
            </Fab>
          }
        </Grid>
      </Grid>
    );
  }
}

function SimulatorNavigationLeft() {
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
  const classes = useStyles();
  const step = useSelector(state => state.simulatorSteps.step);
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const previowsStep = () => {
    dispatch(setStep(step - 1));
  };

  const onClickResetFilter = () => {
    dispatch(resetFilter())
  };

  if (!isSmallDevice) {
    return (
      <React.Fragment>
        <Grid item md={1} sm={0} className={classes.leftNavigationAligner}>
          {
            step > 0 &&
            <Fab className={classes.buttonRight} onClick={previowsStep}>
              <Icon>chevron_left</Icon>
            </Fab>

          }
        </Grid>
      </React.Fragment>

    );
  } else {
    return null;
  }
}

function SimulatorNavigationRight() {
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
  const classes = useStyles();
  const step = useSelector(state => state.simulatorSteps.step);
  const dispatch = useDispatch();
  const picture = useSelector(state => state.simulator.picture);
  const pattern = useSelector(state => state.simulator.pattern);
  const { t } = useTranslation();
  const size = useSelector(state => state.simulator.size);

  const nextStep = () => {
    if (step === 0 && !picture) {
      Swal.fire({
        imageUrl: 'images/error.png',
        imageWidth: 50,
        imageHeight: 50,
        confirmButtonColor: colors.primary,
        customClass: {
          confirmButton: 'youpii-confirm-button-class',
        },
        text: t('no-photo-upload-error')
      })
    } else if (step === 2 && !size) {
      Swal.fire({
        imageUrl: 'images/error.png',
        imageWidth: 50,
        imageHeight: 50,
        confirmButtonColor: colors.primary,
        customClass: {
          confirmButton: 'youpii-confirm-button-class',
        },
        text: t('no-size-selected')
      })
    } else if (step === 3 && !pattern) {
      Swal.fire({
        imageUrl: 'images/error.png',
        imageWidth: 50,
        imageHeight: 50,
        confirmButtonColor: colors.primary,
        customClass: {
          confirmButton: 'youpii-confirm-button-class',
        },
        text: t('no-pattern-selector')
      })
    } else {
      dispatch(setStep(step + 1))
    }
  };

  if (!isSmallDevice) {
    return (

      <React.Fragment>
        <Grid item md={1} sm={0} className={classes.rightNavigationAligner}>
          {
            step < 4 &&
            <Fab className={classes.buttonRight} onClick={nextStep}>
              <Icon>chevron_right</Icon>
            </Fab>
          }

        </Grid>


      </React.Fragment>
    );
  } else {
    return null;
  }
}


function SimulatorBody() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Grid container className={classes.simulatorBody}>
        <Grid item xs={12}>
          {/* <SimulatorNavigationMobile/> */}
          <Grid container>
            <SimulatorNavigationLeft />
            <Grid item md={10} xs={12}>
              <StageSelector />
            </Grid>
            <SimulatorNavigationRight />
          </Grid>
        </Grid>
        <SimulatorNavigationMobile />
      </Grid>
    </React.Fragment>

  )
}

export default function Simulator(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  return (
    <div>
      <Helmet>
        <title>YOUPi! - Your Best Feeling - {t('simulator')}</title>
        <meta name="title" content="Eu já simulei o meu YOUPi! e tu?" />
        <meta name="description" content="YOUPi! - Your Best Feeling" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Eu já simulei o meu YOUPi! e tu?" />
        <meta property="og:description" content="YOUPi! - Your Best Feeling" />
        <meta property="og:image" content={fileManagerApi + "/files/" + props.match.params.uid + "/download/base64"} />
        <meta property="og:image:width" content="952" />
        <meta property="og:image:height" content="634" />
      </Helmet>
      {/*<Header title={t('simulator-header-title')} subTitle={t('simulator-header-subtitle')} />*/}
      <Container>
        <Grid container className={classes.simulator}>
          <Grid item xs={12}>
            <Breadcrumbs />
          </Grid>
          <Grid item xs={12} className={classes.box}>
            <SimulatorBody />
          </Grid>
        </Grid>
      </Container>
      <Footer />
    </div>
  );
}


const useStyles = makeStyles(theme => ({
  marginTop: {
    marginTop: theme.spacing(2),
    height: 40,
  },
  button: {
    marginLeft: theme.spacing(1),
  },
  simulator: {
    paddingTop: 40,
    paddingBottom: 40
  },
  simulatorBody: {
    minHeight: 500,
    paddingTop: 30,
    paddingBottom: 30,
    paddingLeft: 20,
    paddingRight: 20,
  },
  box: {
    backgroundColor: colors.backgroundGrey,
    borderRadius: '7.5px'
  },
  paginator: {
    width: '60px'
  },
  buttonLeft: {
    borderWidth: '1px',
    border: 'solid',
    width: '40px',
    height: '40px',
    borderColor: colors.primary,
    color: colors.primary,
    backgroundColor: colors.backgroundGrey,
    boxShadow: "none"
  },
  buttonRight: {
    borderWidth: '1px',
    border: 'solid',
    width: '40px',
    height: '40px',
    borderColor: colors.primary,
    color: colors.primary,
    backgroundColor: colors.backgroundGrey,
    float: "right",
    boxShadow: "none"
  },
  leftNavigationAligner: {
    display: 'flex',
    alignItems: 'center',
  },
  rightNavigationAligner: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
  }
}));
