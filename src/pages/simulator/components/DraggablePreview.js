import {useDispatch, useSelector} from "react-redux";
import FrameCanvas from "../../../common/frame/FrameCanvas";
import Draggable from "react-draggable";
import React, {useEffect} from "react";
import {makeStyles, useTheme} from "@material-ui/core";
import clsx from 'clsx';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import {convertURLToBase64} from "../../../common/utils";
import frameSizes from "../../../common/frame/frame-sizes"
import frameSizesMobile from "../../../common/frame/frame-sizes-mobile";
import {setPreviowParameters} from "../../../redux/actions/SimulatorActions";

// const bodyScrollLock = require('body-scroll-lock');
// const disableBodyScroll = bodyScrollLock.disableBodyScroll;
// const enableBodyScroll = bodyScrollLock.enableBodyScroll;

function getSizeIndex(sizes, size) {
  for (let i = 0; i < sizes.length; i++) {
    if (sizes[i][0] === size[0]) {
      return i
    }
  }
  return 0;
}

function DraggableBackgroundImagePreview({defaultPositionX=0, defaultPositionY=-100, disabled=false}) {

  const dispatch = useDispatch();
  const theme = useTheme();

  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
  const currentFrameSizes = isSmallDevice?frameSizesMobile:frameSizes;

  const backgroundPictureRotation = useSelector(state => state.simulator.backgroundPictureRotation);
  const distance = useSelector(state => state.simulator.distance);
  const size = useSelector(state => state.simulator.size);
  const pattern = useSelector(state => state.simulator.pattern);
  const backgroundPicture = useSelector(state => state.simulator.backgroundPicture);


  const {frameSize, frameMargin} = size && distance ? currentFrameSizes[size[1]]["" + distance] : {frameSize:null, frameMargin:null};
  const classes = useStyles({frameSize, disabled});

  let backgroundHeight = null;
  let backgroundWidth = null;
  // const url = URL.createObjectURL(backgroundPicture);
  // getImageSize(url).then(({width, height}) => {
  //   backgroundHeight = height;
  //   backgroundWidth = width;
  // });

  let backgroundImageBase64 = null;
  // convertBlobToBase64(backgroundPicture).then((base64) => {
  //   backgroundImageBase64 = base64;
  // });

  let patternBase64;
  convertURLToBase64(pattern.frame).then((base64) => {
    patternBase64 = base64;
  });

  const preventDrag = () => {
    document.getElementById("frameCanvas").addEventListener("touchmove", (e) => e.preventDefault(), true);
  };

  const sendPreviewParameter = (e, data) => {
    e.preventDefault();
    const height = 550;
    const width = (550*backgroundWidth)/backgroundHeight;

    // const deltaX = data.lastX+(width/2);
    // const deltaY = 800+data.lastY;

    const deltaX = data.lastX;
    const deltaY = data.lastY;


    const imageSize = frameSize-frameSize*0.235474;

    dispatch(setPreviowParameters(deltaX, deltaY, frameSize, imageSize, frameMargin, height, width, backgroundImageBase64, patternBase64))
  };

  useEffect(() => {
    preventDrag();
  },[]);

  return (
    <div style={{overflow: 'hidden'}} id="dragableContainer">
      <div className={clsx("", classes.draggableArea)}>
        <img
            style={{transform:`rotate(${backgroundPictureRotation}deg)`}}
            src={backgroundPicture}
            className={classes.backgroundPicture}
            alt="Background"
          />
        {/*<img src={backgroundPicture.preview} className={classes.backgroundPicture}/>*/}
      </div>
      <Draggable
        disabled={disabled}
        axis="both"
        handle=".handle"
        defaultPosition={{x: defaultPositionX, y: defaultPositionY}}
        position={null}
        grid={[25, 25]}
        scale={1}
        onStart={sendPreviewParameter}
        onStop={sendPreviewParameter}
        onDrag={preventDrag}
        onMouseDown={preventDrag}
        onTouchMove={(e) => e.preventDefault()}
      >
        <div className={clsx("handle", classes.canvasContainer)} id="frameCanvas">
          <FrameCanvas size={size[1]} distance={distance}/>
        </div>
      </Draggable>
    </div>
  )
}

export default function DraggablePreview({defaultPositionX=0, defaultPositionY=-100, disabled=false}) {
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
  const isExtraSmallDevice = useMediaQuery(theme.breakpoints.down('xs'));

  let scale = 1;
  scale = isSmallDevice?0.8:scale;
  scale = isExtraSmallDevice?0.6:scale;

  const backgroundPicture = useSelector(state => state.simulator.backgroundPicture);

  if (!backgroundPicture) {
    return (
      <FrameCanvas scale={scale} />
    );
  } else {
    return (
      <DraggableBackgroundImagePreview
        defaultPositionX={defaultPositionX}
        defaultPositionY={defaultPositionY} disabled={disabled}/>
    )
  }
}

const useStyles = makeStyles(theme => ({
  draggableArea: {
    width: "100%",
    marginBottom: ({frameSize}) => -frameSize,
    // maxHeight: 650,
    cursor: ({disabled}) => disabled?"cursor":"move",
    margin: 0,
     [theme.breakpoints.down('sm')]: {
      marginBottom: ({frameSize}) => -frameSize-50
     }
  },
  backgroundPicture: {
    height: 550,
    maxWidth: "100%",
    "-webkit-user-drag": "none",
    [theme.breakpoints.down('sm')]: {
      height: "auto",
      width: "100%"
    }
  },
  canvasContainer: {
    cursor: "move",
    width: "100%",
    height: "100%"
  }
}));
