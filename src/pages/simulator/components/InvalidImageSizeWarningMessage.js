import Grid from "@material-ui/core/Grid";
import React from "react";
import {makeStyles, Typography} from "@material-ui/core";
import {colors} from "../../../constants";
import {useSelector} from "react-redux";
import {ERROR_INVALID_SIZE_FILE} from "../../../redux/Types";
import { useTranslation } from 'react-i18next';

export default function InvalidImageSizeWarningMessage() {

  const { t } = useTranslation();
  const classes = useStyles();
  const error = useSelector(state => state.simulator.error);

  if (error != ERROR_INVALID_SIZE_FILE) {
    return null;
  } else {
    return (
      <Grid container className={classes.container}>
        <Grid item md={12} className={classes.alignCenter}>
          <img src={'images/error.png'} className={classes.image}/>
        </Grid>
        <Grid item md={12}>
          <Typography variant="h6" component="h6" className={classes.text}>
            {t('invalid-size-text1')}
          </Typography>
          <Typography variant="h6" component="h6" className={classes.text}>
            {t('invalid-size-text2')}
          </Typography>
        </Grid>
      </Grid>
    )
  }
}


const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 20,
    marginBottom: 20
  },
  image: {
    maxWidth: 50,
    marginBottom: 10
  },
  text: {
    textAlign: "center",
    color: colors.red
  },
  alignCenter: {
    textAlign: "center",
    margin: "auto"
  }
}));
