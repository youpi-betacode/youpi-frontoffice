import {useDispatch, useSelector} from "react-redux";
import {setWhiteSpace} from "../../../redux/actions/SimulatorActions";
import React from "react";
import {Checkbox, FormControlLabel, FormGroup} from "@material-ui/core";
import { useTranslation } from 'react-i18next';

export default function ToogleWhiteSpace() {

  const dispatch = useDispatch();
  const { t } = useTranslation();
  const whiteSpace = useSelector(state => state.simulator.whiteSpace);
  const toogleWhiteSpace = () => {
    dispatch(setWhiteSpace(!whiteSpace))
  };

  return (
    <React.Fragment>
      <FormGroup row>
        <FormControlLabel
          control={
            <Checkbox checked={whiteSpace} onChange={toogleWhiteSpace} value={t('white-margin')} color="primary"/>
          }
          label={t('white-margin')}
        />
      </FormGroup>
    </React.Fragment>
  )
}
