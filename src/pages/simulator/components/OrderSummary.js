import {useSelector} from "react-redux";
import {makeStyles, Paper, Typography} from "@material-ui/core";
import React from "react";
import {colors} from "../../../constants";

export default function OrderSummary() {
  const classes = useStyles();
  const selectedPattern = useSelector(state => state.simulator.pattern);
  const selectedSize = useSelector(state => state.simulator.size);

  return (
    <div>
      <Typography variant="h6" component="h6" gutterBottom className={classes.print}>
        Impressão em Polistereno 8mm de espessura
      </Typography>
      <Typography variant="h6" component="h6" gutterBottom className={classes.print}>
        Medida da foto {selectedSize[0]}
      </Typography>
      <Typography variant="h6" component="h6" gutterBottom className={classes.print}>
        Medida da moldura {selectedSize[1]}
      </Typography>
      <Typography variant="h6" component="h6" gutterBottom className={classes.print}>
        Fundo padrão {selectedPattern.name}
      </Typography>
      <Paper className={classes.pricePaper}>
        <Typography component="p" className={classes.print}>
          Simulação no valor de <b>35€</b>
        </Typography>
      </Paper>
    </div>
  )
}

const useStyles = makeStyles(theme => ({
  print: {
    color: colors.textColor
  },
   pricePaper: {
    padding: "20px",
    background: "none",
    border: "3px solid white",
    boxShadow: "none",
    textAlign: "center",
    marginTop: 20
  }
}));
