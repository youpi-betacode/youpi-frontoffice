import {Icon, makeStyles, Typography} from "@material-ui/core";
import React from "react";
import {colors} from "../../../constants";

function PanelIcon(props) {
  const classes = useStyles();

  if (props.expanded === props.panel) {
    return <Icon className={classes.iconCarret}>keyboard_arrow_up</Icon>;
  } else {
    return <Icon className={classes.iconCarret}>keyboard_arrow_down</Icon>;
  }
}

export default function PanelHeading(props) {
  const classes = useStyles();

  const heading = props.expanded !== props.panel && props.selectedValue ? props.selectedValue : props.heading;

  if (props.expanded === props.panel || !props.selectedValue) {
    return (
      <React.Fragment>
        <Typography className={classes.heading}>{heading} <PanelIcon expanded={props.expanded}
                                                                     panel={props.panel}/></Typography>
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Typography className={classes.heading}>{heading} <PanelIcon expanded={props.expanded}
                                                                     panel={props.panel}/></Typography>
      </React.Fragment>
    )
  }
}

const useStyles = makeStyles(theme => ({
  iconCarret: {
    position: "absolute",
    right: 10,
    top: 15,
    color: colors.primary
  },
  heading: {
    textTransform: "uppercase",
    fontSize: 12,
    textAlign: "center",
    width: "100%"
  },
}));
