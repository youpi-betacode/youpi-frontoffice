import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import {useDispatch, useSelector} from 'react-redux';
import { colors } from '../../../constants';
import Grid from "@material-ui/core/Grid";
import { useTranslation } from 'react-i18next';
import {setStep} from "../../../redux/actions/SimulatorStepsActions";
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { animateScroll as scroll} from 'react-scroll'
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import useTheme from "@material-ui/core/styles/useTheme";

export default function Breadcrumbs() {
  const classes = useStyles();
  const { t } = useTranslation();
  const step = useSelector(state => state.simulatorSteps.step);
  const picture = useSelector(state => state.simulator.picture);
  const pattern = useSelector(state => state.simulator.pattern);
  const size = useSelector(state => state.simulator.size);
  const dispatch = useDispatch();
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));

  const scrollToSimulatorBeginning = () => {
    if(isSmallDevice){
        scroll.scrollTo(300);
      }
  };

  const goToStep = (step) => {
    if (step > 0 && !picture) {
      Swal.fire({
        imageUrl: 'images/error.png',
        imageWidth: 50,
        imageHeight: 50,
        confirmButtonColor: colors.primary,
        customClass: {
          confirmButton: 'youpii-confirm-button-class',
        },
        text: t('no-photo-upload-error')
      })
    }else if(step > 2 && !size){
       Swal.fire({
        imageUrl: 'images/error.png',
        imageWidth: 50,
        imageHeight: 50,
        confirmButtonColor: colors.primary,
        customClass: {
          confirmButton: 'youpii-confirm-button-class',
        },
        text: t('no-size-selected')
      })
    } else if(step > 3 && !pattern){
       Swal.fire({
        imageUrl: 'images/error.png',
        imageWidth: 50,
        imageHeight: 50,
        confirmButtonColor: colors.primary,
        customClass: {
          confirmButton: 'youpii-confirm-button-class',
        },
        text: t('no-pattern-selector')
      })
    } else {
      scrollToSimulatorBeginning();
      dispatch(setStep(step))
    }
  };

  return (
      <Grid container spacing={3} className={classes.container}>
        <Grid item md={4} sm={12} xs={12}>
          <div className={clsx(classes.stage, classes.stageComplete)} onClick={() => goToStep(2)}>
            {t('simulator-stage1')}
         </div>
        </Grid>
        <Grid item md={4} sm={12} xs={12}>
          <div
            className={clsx(classes.stage, classes.stageCenter, step > 2 && classes.stageComplete)}
            onClick={() => goToStep(3)}>
            {t('simulator-stage2')}
          </div>
        </Grid>
        <Grid item md={4} sm={12} xs={12}>
          <div className={clsx(classes.stage, step > 3 && classes.stageComplete)} onClick={() => goToStep(4)}>
            {t('simulator-stage3')}
          </div>
        </Grid>
      </Grid>
  );
}


const useStyles = makeStyles(theme => ({
  container:{
    marginBottom: '10px'
  },
  stages: {
    display: 'flex',
    marginBottom: '20px',
  },
  stageCenter: {
  },
  stage: {
    alignItems: 'center',
    border: 'solid',
    borderColor: colors.textColor,
    borderWidth: '1px',
    borderRadius: '7.5px',
    color: colors.primary,
    display: 'flex',
    flex: 1,
    height: '50px',
    justifyContent: 'center',
    cursor:"pointer",
    transition: "all ease 0.1s",
    '&:hover':{
      background: "#745334", /* Old browsers */
      background: "-moz-linear-gradient(left,  #745334 0%, #fabc87 51%, #745334 93%, #745334 100%)", /* FF3.6-15 */
      background: "-webkit-linear-gradient(left,  #745334 0%,#fabc87 51%,#745334 93%,#745334 100%)", /* Chrome10-25,Safari5.1-6 */
      background: "linear-gradient(to right,  #745334 0%,#fabc87 51%,#745334 93%,#745334 100%)", /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
      filter: "progid:DXImageTransform.Microsoft.gradient( startColorstr='#745334', endColorstr='#745334',GradientType=1 )",/* IE6-9 */
      backgroundSize: "cover",
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center top",
      backgroundColor: colors.primary,
      borderColor: colors.primary,
      color: colors.white
    }
  },
  stageComplete: {
    background: "#745334", /* Old browsers */
    background: "-moz-linear-gradient(left,  #745334 0%, #fabc87 51%, #745334 93%, #745334 100%)", /* FF3.6-15 */
    background: "-webkit-linear-gradient(left,  #745334 0%,#fabc87 51%,#745334 93%,#745334 100%)", /* Chrome10-25,Safari5.1-6 */
    background: "linear-gradient(to right,  #745334 0%,#fabc87 51%,#745334 93%,#745334 100%)", /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: "progid:DXImageTransform.Microsoft.gradient( startColorstr='#745334', endColorstr='#745334',GradientType=1 )",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center top",
    backgroundColor: colors.primary,
    borderColor: colors.primary,
    color: colors.white,
  }
}));
