import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { generateShare, setShareLoading } from '../../../redux/actions/SimulatorActions';
import { convertURLToBase64 } from "../../../common/utils";
import Button from '@material-ui/core/Button';
import { sharePath } from '../../../constants';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import useTheme from "@material-ui/core/styles/useTheme";
import CircularProgress from "@material-ui/core/CircularProgress";

export default function ShareMenu() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const pattern = useSelector(state => state.simulator.pattern);
  const filteredPhoto = useSelector(state => state.simulator.filteredPhotoResized);
  const { t } = useTranslation();
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
  const loading = useSelector(state => state.simulator.shareLoading)

  const share = (type) => {
    dispatch(setShareLoading(true))
    let patternBase64;
    convertURLToBase64(pattern.frame).then((base64) => {
      patternBase64 = base64;
      dispatch(generateShare(patternBase64, filteredPhoto, type));
    });
  }

  function facebookShare(image){
    if(isSmallDevice) {
      window.open(
        `https://www.facebook.com/sharer/sharer.php?u=${sharePath}${image.uuid}`,
        '_self'
      )
    } else {
      window.open(
        `https://www.facebook.com/sharer/sharer.php?u=${sharePath}${image.uuid}`,
        'fbShareWindow',
        'height=450, width=550, top=' + (window.innerHeight / 2 - 275) +
        ', left=' + (window.innerWidth / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0'
      )
    }

  }

  return (
    <React.Fragment>
      <Button
        color="primary"
        onClick={()=>share(facebookShare)}
      className={classes.button}
        disabled={loading}
      >
        {t('facebook-share')}
      </Button>
    </React.Fragment>

  );
}

const useStyles = makeStyles(theme => ({
  button: {
      width: 360,
      height: 50,
      [theme.breakpoints.down('sm')]: {
         width: 240,
         height: 70,
      }  
  },
  buttonSmallDevice: {
    marginBottom: 20,
    marginTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    width: 260
  },
  radioGroup: {
    margin: theme.spacing(1, 0),
  },
  speedDial: {
    height: 40
  },
}));
