import React, {useEffect} from 'react';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../constants';
import {
  ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Typography,
  List, ListItem, ListItemText
} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import {
    setSize, resetFilter,
    setContrast, setBrightness, setSaturate, setPostorize, fetchFilteredImage, resizePicture
} from '../../../redux/actions/SimulatorActions';
import 'react-block-ui/style.css';
import FilteredImage from "../../../common/frame/FilteredImage";
import SliderInput from "./SliderInput";
import ToogleWhiteSpace from "./ToogleWhiteSpace";
import Button from "@material-ui/core/Button";
import {useTranslation} from 'react-i18next';
import PanelHeading from "./PanelHeading";
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import {enqueueSnackbar} from "../../../redux/actions/NotificationsActions";


function SelectSize(props) {
    const classes = useStyles();

    const options = useSelector(state => state.simulator.options);
    const size = useSelector(state => state.simulator.size);
    const selectedSize = size ? size[0] : null;
    const dispatch = useDispatch();
    const {t} = useTranslation();

    function handleSelectSize(size) {
        dispatch(setSize(size));
        props.setExpanded("");
    }

    function notAvailableSize() {
        const notification = {
            message: "size-not-available",
            options: {
                variant: 'warning',
                anchorOrigin: {
                    vertical: 'top',
                    horizontal: 'right',
                },
                autoHideDuration: 5000
            },
        };
        const snackbar = enqueueSnackbar(
            notification,
            new Date().toString()
        );
        dispatch(snackbar)
    }

    return (
        <ExpansionPanel expanded={props.expanded === 'panel1'} onChange={props.handleChange('panel1')}>
            <ExpansionPanelSummary>
                <PanelHeading expanded={props.expanded} panel="panel1" selectedValue={selectedSize}
                              heading={t('size')}/>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <List className={classes.list}>
                    {
                        options.sizesFeeling.map(size => {
                            if (size[2]) {
                                return (
                                    <ListItem
                                        button
                                        selected={selectedSize && selectedSize === size[0]}
                                        onClick={notAvailableSize}>
                                        <ListItemText primary={size[0]} style={{textAlign: "center", opacity: 0.5}}
                                                      classes={{primary: classes.patternText}}/>
                                    </ListItem>
                                )
                            } else {
                                return (
                                    <ListItem
                                        button
                                        selected={selectedSize && selectedSize === size[0]}
                                        onClick={event => handleSelectSize(size)}>
                                        <ListItemText primary={size[0]} style={{textAlign: "center"}}
                                                      classes={{primary: classes.patternText}}/>
                                    </ListItem>
                                )
                            }
                        })
                    }
                </List>
            </ExpansionPanelDetails>
        </ExpansionPanel>
    );
}

function EditPhoto(props) {
    const classes = useStyles();
    const contrast = useSelector(state => state.simulator.contrast);
    const brightness = useSelector(state => state.simulator.brightness);
    const saturate = useSelector(state => state.simulator.saturate);
    const posterize = useSelector(state => state.simulator.posterize);
    const croppedCanvas = useSelector(state => state.simulator.croppedCanvas);
    const croppedCanvasResized = useSelector(state => state.simulator.croppedCanvasResized)
    const dispatch = useDispatch();
    const {t} = useTranslation();

    const onFilterChange = (b, c, s, t) => {
        const filterData = {
            "img_file": croppedCanvasResized,
            "brightness": b,
            "contrast": c,
            "saturation": s,
            "temperature": t
        };

        dispatch(fetchFilteredImage(filterData))
    };

    const onChangeConstrast = (e, value) => {
        dispatch(setContrast(value));
        onFilterChange(brightness, value, saturate, posterize);
    };

    const onChangeBrightness = (e, value) => {
        dispatch(setBrightness(value));
        onFilterChange(value, contrast, saturate, posterize);
    };

    const onChangeSaturate = (e, value) => {
        dispatch(setSaturate(value));
        onFilterChange(brightness, contrast, value, posterize);
    };

    const onChangePosterize = (e, value) => {
        dispatch(setPostorize(value));
        onFilterChange(brightness, contrast, saturate, value);
    };

    return (
        <ExpansionPanel expanded={props.expanded === 'panel2'} onChange={props.handleChange('panel2')}>
            <ExpansionPanelSummary>
                <PanelHeading expanded={props.expanded} panel="panel2" heading={t('filters')}/>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <Grid container>
                    <SliderInput step={0.05} min={0} max={1} value={brightness} onChangeCommitted={onChangeBrightness}
                                 label={t('brightness')}/>
                    <SliderInput step={0.05} min={0} max={1} value={contrast} onChangeCommitted={onChangeConstrast}
                                 label={t('contrast')}/>
                    <SliderInput step={0.05} min={0} max={1} value={saturate} onChangeCommitted={onChangeSaturate}
                                 label={t('saturate')}/>
                    <SliderInput step={0.05} min={0} max={1} value={posterize} onChangeCommitted={onChangePosterize}
                                 label={t('posterize')}/>
                </Grid>
            </ExpansionPanelDetails>
        </ExpansionPanel>
    )
}

export default function EditBestStep1() {
    const classes = useStyles();
    const theme = useTheme();
    const isExtraSmallDevice = useMediaQuery(theme.breakpoints.down('xs'));
    const [expanded, setExpanded] = React.useState(false);
    const {t} = useTranslation();
    const croppedCanvas = useSelector(state => state.simulator.croppedCanvas);
    const dispatch = useDispatch();
    const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));

    useEffect(() => {
        setExpanded("");
        dispatch(resizePicture(croppedCanvas));
    }, []);

    const handleChange = panel => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    const onClickResetFilter = () => {
        dispatch(resetFilter())
    };

    return (
        <Grid container spacing={3}>
            <Grid item xs={12} md={3}>
                <SelectSize expanded={expanded} handleChange={handleChange} setExpanded={setExpanded}/>
                <EditPhoto expanded={expanded} handleChange={handleChange}/>
                <ToogleWhiteSpace/>

                {
                    !isSmallDevice &&
                    <Button variant="outlined" color="primary" fullWidth className={classes.resetButton}
                            onClick={onClickResetFilter}>
                        {t('reset-filter')}
                    </Button>
                }
            </Grid>
            <Grid item xs={12} md={6} className={classes.alignCanvasMiddle}>
                <Typography variant="h5" component="h2" gutterBottom className={classes.title}>
                    {t('feeling-text1')}
                </Typography>
                <div className={classes.alignItemsMiddle}>
                    <FilteredImage shadowBellow={true} scale={isExtraSmallDevice ? 0.8 : 1}/>
                </div>
            </Grid>
            <Grid item xs={12} md={3} className={classes.alignItemsMiddle}>
                {/*<OrderSummary/>*/}
            </Grid>
        </Grid>
    )
}


const useStyles = makeStyles(theme => ({
    patternText: {
        fontSize: 13
    },
    resetButton: {
        marginTop: 10,
    },
    alignItemsMiddle: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        height: "100%"
    },
    overlay: {
        backgroundColor: colors.black,
        opacity: 0.5
    },
    progress: {
        margin: theme.spacing(2),
    },
    filterTitle: {
        textAlign: "center",
        textTransform: "uppercase"
    },
    pricePaper: {
        padding: "20px",
        background: "none",
        border: "3px solid white",
        boxShadow: "none",
        textAlign: "center",
        marginTop: 20
    },
    alignCanvasMiddle: {
        textAlign: "center"
    },
    frame: {
        display: "inline-flex",
        backgroundSize: "400px 400px"
    },
    stage4: {
        display: 'flex',
        flex: 1,
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: '40px',
        paddingRight: '40px'
    },
    filters: {
        maxWidth: '250px',
    },
    info: {
        maxWidth: '250px',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        textAlign: 'center',
        color: colors.black,
        fontSize: 16,
    },
    print: {
        color: colors.textColor
    },
    image: {
        margin: '50px',
        maxWidth: 300,
        maxHeight: 300
    },
    title: {
        color: colors.primary,
        textAlign: "center",
        marginBottom: 10
    },
    list: {
        maxHeight: '192px',
        overflow: 'auto'
    }
}));
