import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';

import {colors} from '../../../constants';
import DropFile from '../../../common/DropFile';
import {useDispatch} from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {setError, setPicture, resizePicture} from '../../../redux/actions/SimulatorActions';
import {setStep} from '../../../redux/actions/SimulatorStepsActions';
import {ERROR_FILE, ERROR_INVALID_SIZE_FILE, ERROR_LARGE_FILE} from "../../../redux/Types";
import {useSnackbar} from 'notistack';
import {useTranslation} from 'react-i18next';
import {convertBlobToBase64, getImageSize} from "../../../common/utils";
import CloseModalButton from "../../../common/CloseModalButton";
import {animateScroll as scroll} from "react-scroll/modules";
import useTheme from "@material-ui/core/styles/useTheme";
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";

export default function UploadPhoto() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {enqueueSnackbar} = useSnackbar();
  const {t} = useTranslation();
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));

  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOnDrop = (files, rejectedFiles, e) => {

    if (!rejectedFiles || rejectedFiles.length === 0) {

      const url = URL.createObjectURL(files[0]);
      getImageSize(url).then(({width, height}) => {
        if (width < 2048 || height < 1365) {
          dispatch(setError(ERROR_INVALID_SIZE_FILE))
        } else {
          dispatch(setError(null));
        }
      });

      convertBlobToBase64(files[0]).then((binaryBase64) => {
        //dispatch(resizePicture(binaryBase64));

        dispatch(setPicture(binaryBase64));
        dispatch(setStep(1));
        handleClose();
      }).catch(error => {
        dispatch(setError(ERROR_FILE));
        enqueueSnackbar(t('error-photo'), {
          variant: 'error',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
          }
        });
      });
    }
  };

  const handleDropReject = (files, rejectedFiles) => {
    if (files[0].size >= 5000) {
      dispatch(setError(ERROR_LARGE_FILE));
      enqueueSnackbar(t('error-photo-limit-5m'), {
        variant: 'error',
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
        },
        autoHideDuration: 10000
      });
    } else {
      dispatch(setError(ERROR_FILE));
      enqueueSnackbar(t('error-photo'), {
        variant: 'error',
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
        },
        autoHideDuration: 10000
      });
    }

  };

  const scrollToSimulatorBeginning = () => {
    setTimeout(() => {
      if(isSmallDevice){
        scroll.scrollTo(300);
      }
    }, 1000)
  };

  useEffect(() => {
    scrollToSimulatorBeginning();
  }, [isSmallDevice]);

  return (
    <Grid container>
      <Grid item xs={12} className={classes.alignCenter}>
        <Typography variant="h5" component="h2" className={classes.title}>
          {t('upload-photo-text1')}
        </Typography>
      </Grid>
      <Grid item xs={12} className={classes.alignCenter}>
        <img src={'/images/YoupiiBigLogo.jpg'} alt='YOUPI' className={classes.image}></img>
      </Grid>
      <Grid item xs={12} className={classes.alignCenter}>
        <Button onClick={handleOpen} color="primary" className={isSmallDevice ? classes.buttonSmallDevice : classes.button}>
          {t('upload-photo')}
        </Button>
        <Typography variant="h6" component="h2" gutterBottom className={classes.subtitle}>
          {t('file-formats-1')}
        </Typography>
        <Typography variant="h6" component="h2" gutterBottom className={classes.subtitle}>
          {t('file-formats-2')}
        </Typography>
        <Typography variant="h6" component="h2" gutterBottom className={classes.subtitle}>
          {t('upload-photo-text2-1')}
        </Typography>
        <Typography variant="h6" component="h2" gutterBottom className={classes.subtitle}>
          {t('upload-photo-text2-2')}
        </Typography>
        <Dialog
          open={open}
          onClose={handleClose}
          maxWidth="xs"
        >
          <CloseModalButton handleClose={handleClose} />
          <DropFile handleOnDrop={handleOnDrop} accept="image/jpg, image/jpeg,, image/png, image/gif"
                    handleDropReject={handleDropReject}></DropFile>
        </Dialog>
      </Grid>
    </Grid>
  );
}


const useStyles = makeStyles(theme => ({
  stage1: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    color: colors.primary,
    textAlign: "center",
  },
  subtitle: {
    textAlign: "center",
    color: "#555555",
  },
  alignCenter: {
    textAlign: "center",
    margin: "auto"
  },
  image: {
    margin: '20px',
    [theme.breakpoints.down('xs')]: {
      width: 200
    }
  },
  button: {
    marginBottom: 10,
  },
  buttonSmallDevice:  {
    marginBottom: 10,
    paddingLeft: 20,
    paddingRight: 20
  }
}));
