import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {useSelector, useDispatch} from 'react-redux';
import {colors} from '../../../constants';
import {
  ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Typography,
  List, ListItem, ListItemText, TextField, Checkbox, FormGroup, FormControlLabel, Button
} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import {
  setPattern,
  setGama,
  setWhiteSpace,
  setPhrase,
  resetFilter
} from '../../../redux/actions/SimulatorActions';
import FrameCanvas from "../../../common/frame/FrameCanvas";
import 'react-block-ui/style.css';
import ToogleWhiteSpace from "./ToogleWhiteSpace";
import { useTranslation } from 'react-i18next';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import useTheme from "@material-ui/core/styles/useTheme";
import PanelHeading from "./PanelHeading";
import clsx from "clsx";


function SelectSize(props) {
  const classes = useStyles();
  const { t } = useTranslation();

  const options = useSelector(state => state.simulator.options);
  const size = useSelector(state => state.simulator.size);
  const selectedSize = size?size[1]:null;

  const getCssClasses = (isDisabled) => {
    return isDisabled?clsx(classes.disabledOption,classes.patternText2):classes.patternText2;
  };

  return (
    <ExpansionPanel expanded={props.expanded === 'panel1'} onChange={props.handleChange('panel1')}>
      <ExpansionPanelSummary>
        <PanelHeading expanded={props.expanded} panel="panel1" selectedValue={selectedSize} heading={t('size')}/>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <List className={classes.list}>
          {
            options.sizesFeeling.map(size =>
              <ListItem
                button
                selected={selectedSize && selectedSize === size[1]}
              >
                <ListItemText primary={size[1]} style={{textAlign:"center"}} classes={{primary:getCssClasses(!selectedSize || selectedSize !== size[1])}}/>
              </ListItem>
            )
          }
        </List>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
}

function SelectPattern(props) {
  const classes = useStyles();

  const options = useSelector(state => state.simulator.options);
  const selectGama = useSelector(state => state.simulator.gama);
  const pattern = useSelector(state => state.simulator.pattern);
  const selectedPattern = pattern?pattern.label:null;
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
  const dispatch = useDispatch();
  const { t } = useTranslation();

  function handleSelectPattern(pattern) {
    if(isSmallDevice){
      props.handleChange('panel3')();
    }

    dispatch(setPattern(pattern))
  }

  return (
    <ExpansionPanel expanded={props.expanded === 'panel3'} onChange={props.handleChange('panel3')}>
      <ExpansionPanelSummary>
        <PanelHeading expanded={props.expanded} panel="panel3" selectedValue={selectedPattern} heading={t('pattern')}/>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <List className={classes.list}>
          {!selectGama && <Typography>{t('selecionar-gama')}</Typography>}
          {selectGama &&
            options.patterns[selectGama].map(pattern => {
              const patternUrl = `url("${pattern.frame}")`;

                return (
                  <ListItem
                  button
                  selected={selectedPattern === pattern.label}
                  onClick={event => handleSelectPattern(pattern)}
                >
                  <ListItemText primary={pattern.label} classes={{primary:classes.patternText}}/>
                  <div className={classes.patterPreview} style={{backgroundImage: patternUrl }}>

                  </div>
                </ListItem>
                );
              }
            )
          }
        </List>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  )
}

function SelectGama(props) {

  const classes = useStyles();
  const { t } = useTranslation();
  const options = useSelector(state => state.simulator.options);
  const selectedGama = useSelector(state => state.simulator.gama);
  const dispatch = useDispatch();

  function handleSelectGama(gama) {
    dispatch(setGama(gama))
  }

  return (
    <ExpansionPanel expanded={props.expanded === 'panel3'} onChange={props.handleChange('panel3')}>
      <ExpansionPanelSummary>
        <PanelHeading expanded={props.expanded} panel="panel3" selectedValue={t(selectedGama)} heading={t('gama')}/>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <List className={classes.list}>
          {
            options.gamas.map(gama =>
              <ListItem
                button
                selected={selectedGama === gama}
                onClick={event => handleSelectGama(gama)}
              >
                <ListItemText primary={t(gama)} style={{textAlign:"center"}} classes={{primary:classes.patternText2}}/>
              </ListItem>
            )
          }
        </List>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  )
}

function Costumization(props) {
  const classes = useStyles();
  const { t } = useTranslation();
  const whiteSpace = useSelector(state => state.simulator.whiteSpace);
  const phrase = useSelector(state => state.simulator.phrase);
  const dispatch = useDispatch();

  const toogleWhiteSpace = () => {
    dispatch(setWhiteSpace(!whiteSpace))
  };

  const onChangePhrase = (event) => {
    dispatch(setPhrase(event.target.value))
  }

  return (
    <ExpansionPanel expanded={props.expanded === 'panel4'} onChange={props.handleChange('panel4')}>
      <ExpansionPanelSummary>
        <Typography className={classes.heading}>{t('customization')}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <TextField
              id="outlined-multiline-flexible"
              label="Insira uma frase no verso do seu YOUPI!"
              multiline
              rowsMax="4"
              onChange={onChangePhrase}
              margin="normal"
              fullWidth
              variant="outlined"
              value={phrase}
              inputProps={{
                style: {
                  minHeight: 70
                },
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <FormGroup row>
              <FormControlLabel
                control={
                  <Checkbox checked={whiteSpace} onChange={toogleWhiteSpace} value="Margem Branca" color="primary"/>
                }
                label={t('white-margin')}
              />
            </FormGroup>
          </Grid>
        </Grid>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  )
}

export default function EditBestStep2() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const { t } = useTranslation();
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
  const isExtraSmallDevice = useMediaQuery(theme.breakpoints.down('xs'));
  const dispatch = useDispatch();

  let scale = 1;
  scale = isSmallDevice?0.8:scale;
  scale = isExtraSmallDevice?0.7:scale;


  useEffect(() => {
    setExpanded("")
  }, []);

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const onClickResetFilter = () => {
    dispatch(resetFilter())
  };

  return (
    <Grid container xs={12} spacing={3}>
      <Grid item xs={12} md={3}>
        <SelectSize expanded={expanded} handleChange={handleChange}/>
        <SelectGama expanded={expanded} handleChange={handleChange}/>
        <SelectPattern expanded={expanded} handleChange={handleChange}/>
        <ToogleWhiteSpace/>
        {
          !isSmallDevice &&
          <Button variant="outlined" color="primary" fullWidth className={classes.resetButton} onClick={onClickResetFilter}>
            {t('reset-filter')}
          </Button>
        }
      </Grid>
      <Grid item xs={12} md={6} className={classes.alignCanvasMiddle}>
        <Typography variant="h5" component="h5" gutterBottom className={classes.title}>
          {t('best-text1')}
        </Typography>
        <div className={classes.alignItemsMiddle}>
          <FrameCanvas shadowBellow={true} scale={scale}/>
        </div>
      </Grid>
      <Grid item xs={12} md={3} className={classes.alignItemsMiddle}>
        {/*<OrderSummary/>*/}
      </Grid>
    </Grid>
  )
}


const useStyles = makeStyles(theme => ({
  patternText2:{
    fontSize:13
  },
  resetButton: {
    marginTop: 10,
  },
  patternText:{
    fontSize:10
  },
  patterPreview:{
    width: 40,
    height: 40,
    backgroundSize: "330px 330px"
  },
  alignItemsMiddle:{
    display:"flex",
    flexDirection:"column",
    justifyContent:"center",
    alignItems:"center",
    height:"100%"
  },
  overlay: {
    backgroundColor: colors.black,
    opacity: 0.5
  },
  progress: {
    margin: theme.spacing(2),
  },
  filterTitle: {
    textAlign: "center",
    textTransform: "uppercase"
  },
  pricePaper: {
    padding: "20px",
    background: "none",
    border: "3px solid white",
    boxShadow: "none",
    textAlign: "center",
    marginTop: 20
  },
  heading: {
    textTransform: "uppercase",
    fontSize: 12,
    textAlign: "center",
    width: "100%"
  },
  alignCanvasMiddle: {
    textAlign: "center"
  },
  frame: {
    display: "inline-flex",
    backgroundSize: "400px 400px"
  },
  stage4: {
    display: 'flex',
    flex: 1,
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: '40px',
    paddingRight: '40px'
  },
  filters: {
    maxWidth: '250px',
  },
  info: {
    maxWidth: '250px',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    textAlign: 'center',
    color: colors.black,
    fontSize: 16,
  },
  print: {
    color: colors.textColor
  },
  image: {
    margin: '50px',
    maxWidth: 300,
    maxHeight: 300
  },
  title: {
    color: colors.primary,
    textAlign: "center",
    marginBottom: 10
  },
  list: {
    maxHeight: '192px',
    overflow: 'auto'
  },
  disabledOption:{
    color: "#a1a1a1"
  }
}));
