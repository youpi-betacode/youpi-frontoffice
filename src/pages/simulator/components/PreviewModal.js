import React, { useEffect } from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {useDispatch, useSelector} from 'react-redux';
import {fetchOverlap, generateShare} from '../../../redux/actions/SimulatorActions';
import DraggablePreview from "./DraggablePreview";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  EmailShareButton,
} from 'react-share';
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {colors} from "../../../constants";
import {Icon} from "@material-ui/core";
import {convertURLToBase64} from "../../../common/utils";

export function ShareButtons() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const pattern = useSelector(state => state.simulator.pattern);
  const filteredPhoto = useSelector(state => state.simulator.filteredPhoto);
  const dispatch = useDispatch();

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function share() {
    let patternBase64;
    convertURLToBase64(pattern.frame).then((base64) => {
      patternBase64 = base64;
      dispatch(generateShare(patternBase64, filteredPhoto));
      handleClose();
    });

  }

  return (
    <React.Fragment>
      <Button variant="outlined" className={classes.root} aria-controls="menu-list-grow" aria-haspopup="true"
              onClick={handleClick}>
        Partilhar
          <Icon>share</Icon>
      </Button>
      <Menu
        id="simple-menu"
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={share}>
            Facebook
        </MenuItem>
      </Menu>
    </React.Fragment>
  )
}

export default function PreviewModal() {
  const classes = useStyles();
  const preview = useSelector(state => state.simulator.preview);


  return (
    <React.Fragment>
      <div className={classes.paper}>
        <div className={classes.marginFrame}>
          {/*<ShareButtons/>*/}
          <DraggablePreview defaultPositionX={preview.deltaX} defaultPositionY={preview.deltaY} disabled={true}/>
        </div>
      </div>


    </React.Fragment>
  );
}

const useStyles = makeStyles(theme => ({
  root: {
    borderRadius: 5,
    borderWidth: 2,
    paddingTop: 9,
    borderColor: colors.primary,
    color: 'black',
    height: 48,
    textAlign: 'center',
    background: "#fff",
    position:"absolute",
    bottom:50,
    right:50,
    "&:hover":{
      background: "rgba(255,255,255,0.8)"
    }
  },
  marginFrame: {
    marginBottom: -25,
    [theme.breakpoints.down('sm')]: {
      marginBottom: -5
    }
  },
  paper: {
    cursor: "pointer",
    width: "100",
    height: 840,
    padding: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.down('sm')]: {
      width: 280,
      height: "100%",
    }
  },
  picture: {
    width: '100%',
    maxHeight: '100%'
  }

}));


