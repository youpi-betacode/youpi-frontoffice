import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import { colors } from '../../../constants';
import ImageEditorRc from 'react-cropper-image-editor';
import {
   setCroppedCanvas,
   setError,
   setPicture
} from '../../../redux/actions/SimulatorActions';
import 'cropperjs/dist/cropper.css';
import './styles.css'
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { Button, Dialog } from "@material-ui/core";
import DropFile from "../../../common/DropFile";
import clsx from 'clsx';
import InvalidImageSizeWarningMessage from "./InvalidImageSizeWarningMessage";
import { ERROR_FILE, ERROR_INVALID_SIZE_FILE } from "../../../redux/Types";
import { useTranslation } from 'react-i18next';
import { useSnackbar } from 'notistack';
import { convertBlobToBase64 } from "../../../common/utils";
import { setStep } from "../../../redux/actions/SimulatorStepsActions";
import Hidden from '@material-ui/core/Hidden';
import CloseModalButton from "../../../common/CloseModalButton";

let cropper;

export default function CropPhoto() {
   const classes = useStyles();
   const dispatch = useDispatch();
   const picture = useSelector(state => state.simulator.picture);
   const { t } = useTranslation();
   const [open, setOpen] = React.useState(false);
   const { enqueueSnackbar } = useSnackbar();

   const handleOpen = () => {
      setOpen(true);
   };

   const handleClose = () => {
      setOpen(false);
   };

   const handleImageSize = (url) => {
      let img = new Image();
      img.onload = function () {
         if (this.width < 2048 || this.height < 1365) {
            dispatch(setError(ERROR_INVALID_SIZE_FILE))
         } else {
            dispatch(setError(null));
         }
      };
      img.src = url;
   };


   const handleOnDrop = (files, rejectedFiles, e) => {

      if (!rejectedFiles || rejectedFiles.length === 0) {

         const url = URL.createObjectURL(files[0]);
         handleImageSize(url);

         convertBlobToBase64(files[0]).then((binaryBase64) => {
            dispatch(setPicture(binaryBase64));
            dispatch(setStep(1));
            handleClose();
            setTimeout(() => { saveCrop() }, 1000);
         }).catch(error => {
            dispatch(setError(ERROR_FILE));
            enqueueSnackbar(t('error-photo'), { variant: 'error' });
         });
      }
   };

   const saveCrop = () => {
      if (cropper && cropper.getCroppedCanvas()) {
         const photo = cropper.getCroppedCanvas().toDataURL("image/jpg");
         dispatch(setCroppedCanvas(photo));
      }
   };

   const ready = () => {
      saveCrop();
   };

   return (
      <Grid container>
         <Grid item xs={12}>
            <InvalidImageSizeWarningMessage />
            <Typography variant="h5" component="h5" gutterBottom className={classes.title}>
               {t('crop-photo-text1')}
            </Typography>
            <Typography variant="h6" component="h6" gutterBottom className={classes.subtitle}>
               {t('crop-photo-text2')}
            </Typography>
         </Grid>
         <Grid item xs={12} className={clsx(classes.margin10)}>
            <Hidden mdUp>
               <ImageEditorRc
                  ref={ref => { cropper = ref; }}
                  crossOrigin='true' // boolean, set it to true if your image is cors protected or it is hosted on cloud like aws s3 image server
                  src={picture}
                  viewMode={1}
                  style={{ maxWidth: 250, maxHeight: 250 }}
                  className={'store-cropper'}
                  aspectRatio={1}
                  cropstart={saveCrop}
                  cropend={saveCrop}
                  ready={ready}
                  autoCrop={true}
                  imageName='image name with extension to download' // it has to catch the returned data and do it whatever you want
                  responseType='blob/base64' />
            </Hidden>
            <Hidden smDown>
               <ImageEditorRc
                  ref={ref => { cropper = ref }}
                  crossOrigin='true' // boolean, set it to true if your image is cors protected or it is hosted on cloud like aws s3 image server
                  src={picture}
                  viewMode={1}
                  style={{ maxWidth: 400, maxHeight: 400 }}
                  className={'store-cropper'}
                  aspectRatio={1}
                  cropstart={saveCrop}
                  cropend={saveCrop}
                  ready={ready}
                  autoCrop={true}
                  imageName='image name with extension to download' // it has to catch the returned data and do it whatever you want
                  responseType='blob/base64' />
            </Hidden>
         </Grid>
         <Grid item xs={12} className={clsx(classes.alignCenter, classes.margin10)}>
            <Button onClick={handleOpen} color="primary">
               {t('upload-other-photo')}
            </Button>

            <Dialog
               open={open}
               onClose={handleClose}
               maxWidth="xs"
            >
               <CloseModalButton handleClose={handleClose} />
               <DropFile handleOnDrop={handleOnDrop}></DropFile>
            </Dialog>
         </Grid>
      </Grid>
   );
}


const useStyles = makeStyles(theme => ({
   marginAuto: {
      margin: "auto"
   },
   margin10: {
      marginRight: "auto",
      marginLeft: "auto"
   },
   alignCenter: {
      textAlign: "center",
      margin: "auto",
      marginTop: 30,
      marginBottom: 30
   },
   stage2: {
      display: 'flex',
      flex: 1,
      height: '100%',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
   },
   image: {
      maxHeight: '300px',
      maxWidth: '500px',
      margin: '20px'
   },
   title: {
      color: colors.primary,
      textAlign: "center",
   },
   subtitle: {
      textAlign: "center",
      marginBottom: 10
   },
   button: {
      textTransform: 'none',
      border: 'solid',
      borderColor: colors.primary,
      borderWidth: '1px',
      backgroundColor: colors.white,

      paddingLeft: '30px',
      paddingRight: '30px'
   },
}));
