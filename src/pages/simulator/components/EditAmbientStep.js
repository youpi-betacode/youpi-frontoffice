import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import { push } from 'connected-react-router';

import { colors, feeling_default_reference } from '../../../constants';
import DropFile from '../../../common/DropFile';
import { useDispatch, useSelector } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { rotateBackground, setBackgroundPicture, setDistance, setError } from '../../../redux/actions/SimulatorActions';
import { ERROR_FILE, ERROR_LARGE_FILE } from "../../../redux/Types";
import { useSnackbar } from 'notistack';
import SizeSlider from "./SizeSlider";
import DraggablePreview from "./DraggablePreview";
import { useTranslation } from 'react-i18next';
import RegisterModal from '../../../common/register/registerModal';
import CloseModalButton from "../../../common/CloseModalButton";
import PreviewModal from './PreviewModal';
import clsx from "clsx";
import { submitLoggedSim } from '../../../redux/actions/SimulationActions';
import ShareMenu from './ShareMenu';
import CircularProgress from "@material-ui/core/CircularProgress";
import { addProductOnShoppingCart, incrementProductOnShoppingCart } from "../../../redux/actions/ShoppingCartActions";

export default function EditAmbientStep() {
   const classes = useStyles();
   const dispatch = useDispatch();
   const { enqueueSnackbar } = useSnackbar();
   const { t } = useTranslation();
   const simulator = useSelector(state => state.simulator);

   const backgroundPicture = useSelector(state => state.simulator.backgroundPicture);
   const pattern = useSelector(state => state.simulator.pattern);

   const registerModal = useSelector(state => state.register.modal);
   const preview = useSelector(state => state.simulator.preview);


   const [open, setOpen] = React.useState(false);
   const [openPreview, setPreviewOpen] = React.useState(false);

   const handleOpen = () => setOpen(true);
   const handleClose = () => setOpen(false);
   // const handleOpenRegister = () => dispatch(toggleModal(true));

   const handleSubmitSim = () => {
      if (localStorage.getItem('token')) {
         const info = {
            'info': {
               'url': simulator.filteredPhotoResized,
               'pattern': simulator.pattern,
               'gama': simulator.gama,
               'size': simulator.size,
               'margin': simulator.whiteSpace,
               'original_photo': simulator.filteredPhoto,
               'contrast': simulator.contrast,
               'brightness': simulator.brightness,
               'saturate': simulator.saturate,
               'posterize': simulator.posterize
            }
         };

         dispatch(submitLoggedSim(info));
         dispatch(push('/user'));
      }
      else
         dispatch(push('/user/login'))
   };

   const handleOpenPreview = () => setPreviewOpen(true);
   const handleClosePreview = () => setPreviewOpen(false);

   const handleOnDrop = (files, rejectedFiles, e) => {

      if (rejectedFiles && rejectedFiles.length > 0) {

      } else {
         const url = URL.createObjectURL(files[0]);
         let image = Object.assign(files[0], {
            preview: url
         });

         dispatch(setBackgroundPicture(image));
         handleClose();
      }
   };

   const handleDropReject = (files, rejectedFiles) => {
      if (files[0].size >= 5000) {
         dispatch(setError(ERROR_LARGE_FILE));
         enqueueSnackbar(t('error-photo-limit-5m'), {
            variant: 'error',
            anchorOrigin: {
               vertical: 'top',
               horizontal: 'right',
            },
            autoHideDuration: 10000
         });
      } else {
         dispatch(setError(ERROR_FILE));
         enqueueSnackbar(t('error-photo'), {
            variant: 'error',
            anchorOrigin: {
               vertical: 'top',
               horizontal: 'right',
            },
            autoHideDuration: 10000
         });
      }

   };

   const marks = [
      {
         value: 1,
         label: t('1-meter'),
      },
      {
         value: 2,
         label: t('2-meter'),
      },
      {
         value: 3,
         label: t('3-meter'),
      },
      {
         value: 4,
         label: t('4-meter'),
      },
      {
         value: 5,
         label: t('5-meter'),
      },
   ];

   const handleAddToCart = () => {

      const simulationData = {
         'simulation_data': {
            'original_photo': simulator.croppedCanvas,
            'small_filtered_photo': simulator.filteredPhotoResized,
            'pattern': simulator.pattern,
            'gama': simulator.gama,
            'size': simulator.size,
            'have_white_margin': simulator.whiteSpace,
            'contrast': simulator.contrast,
            'brightness': simulator.brightness,
            'saturation': simulator.saturate,
            'temperature': simulator.posterize
         }
      };

      const feelingSize = simulator.size[0] === "14.4x14.4 cm" ? "14.4_" : "23.6_"
      const feelingSplit = feeling_default_reference.replace("_", "_,").split(",");
      const firstFeelingSplit = feelingSplit.shift();
      
      const bestSize = simulator.size[1] === "20x20 cm" ? "20_" : "30_";
      const bestSplit = pattern.reference.replace("_", "_,").split(",");
      const firstBestSplit = bestSplit.shift();

      let bestReference = '';


      if(pattern.reference === 'YBBLLX_2_UV')
        bestReference = 'YB_BL_LX_30_2_UV';
      else
        bestReference = firstBestSplit + bestSize + bestSplit;
      
        const feelingReference = firstFeelingSplit + feelingSize + feelingSplit;

      dispatch(incrementProductOnShoppingCart(bestReference));
      dispatch(incrementProductOnShoppingCart(feelingReference, simulationData));
   };

   const valuetext = (value) => `${value} Metros`;
   const valueLabelFormat = (value) => marks.findIndex(mark => mark.value === value) + 1;
   const onChangeDistance = (e, value) => dispatch(setDistance(value));

   const rotateImage = () => dispatch(rotateBackground());

   const loading = useSelector(state => state.simulator.shareLoading)

   return (
      <Grid container>
         <Grid item xs={12}>
            <Typography variant="h5" component="h5" gutterBottom className={classes.title}>
               {t('edit-ambient-text1')}
            </Typography>
            <Typography variant="h5" component="h5" gutterBottom className={classes.title} style={{ marginBottom: 10 }}>
               {t('edit-ambient-text2')}
            </Typography>
            {backgroundPicture && <Button color="primary" className={classes.rotateButton} onClick={rotateImage}>
               {t('rotate-background')}
            </Button>}
         </Grid>
         <Grid item xs={12} className={classes.alignCenter}>
            <DraggablePreview />
         </Grid>
         {
            backgroundPicture &&
            <Grid item xs={12} className={classes.alignCenter}>
               <SizeSlider
                  aria-labelledby="discrete-slider-restrict"
                  min={1}
                  max={5}
                  step={null}
                  onChangeCommitted={onChangeDistance}
                  valueLabelFormat={valueLabelFormat}
                  getAriaValueText={valuetext}
                  defaultValue={3}
                  marks={marks}
               />
            </Grid>
         }
         <Grid item xs={12} className={classes.alignCenter}>
            <Grid container alignItems='center' justify='center' spacing={1}>
               <Grid item>
                  <Button onClick={handleOpen} className={classes.button} color="primary">
                     {t('upload-background')}
                  </Button>
               </Grid>
               <Grid item>
                  <Button onClick={handleSubmitSim} className={classes.button} color="primary">
                     {t('save-in-account')}
                  </Button>
               </Grid>
               <Grid item>
                  <ShareMenu />
               </Grid>            
               
               <Grid item>
                  <Button onClick={handleAddToCart} className={classes.button} color="primary">
                     {t('add-to-cart')}
                  </Button>
               </Grid>
            </Grid>

            {/*{*/}
            {/*  backgroundPicture !== null &&*/}
            {/*  <Button onClick={handleOpenPreview} className={classes.button} style={{marginLeft:10}}>*/}
            {/*  {t('show-preview')}*/}
            {/*</Button>*/}
            {/*}*/}


            {/*{backgroundPicture && <PreviewModal />}*/}
            {/*<Typography variant="h6" component="h6" gutterBottom className={classes.subtitle}>*/}
            {/*  {t('file-formats-1')}*/}
            {/*</Typography>*/}
            {/*<Typography variant="h6" component="h6" gutterBottom className={classes.subtitle}>*/}
            {/*  {t('file-formats-2')}*/}
            {/*</Typography>*/}
            {/* <Typography variant="h6" component="h6" gutterBottom className={classes.subtitle}>
          {t('error-photo-limit-5m')}
        </Typography> */}
            <Dialog
               open={open}
               onClose={handleClose}
               maxWidth="xs"
            >
               <CloseModalButton handleClose={handleClose} />
               <DropFile handleOnDrop={handleOnDrop} accept="image/jpg, image/jpeg,, image/png, image/gif"
                  handleDropReject={handleDropReject} />
            </Dialog>

            <Dialog open={openPreview} maxWidth="lg">
               <CloseModalButton handleClose={handleClosePreview} />
               <PreviewModal />
            </Dialog>

            <RegisterModal />
         </Grid>
         <Grid item xs={12} style={{ textAlign: 'center' }}>
            {
               loading &&
               <CircularProgress></CircularProgress>
            }

         </Grid>
      </Grid>
   );
}


const useStyles = makeStyles(theme => ({
   rotateButton: {
      textAlign: "center",
      margin: "auto",
      display: "block",
      textTransform: "inherit",
      marginBottom: theme.spacing(2)
   },
   marginLeftMobile: {
      marginLeft: 10,
      [theme.breakpoints.down('sm')]: {
         marginLeft: 0
      }
   },
   stage1: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
   },
   title: {
      color: colors.primary,
      textAlign: "center",
   },
   subtitle: {
      textAlign: "center",
      color: "#555555",
   },
   alignCenter: {
      textAlign: "center",
      margin: "auto"
   },
   image: {
      margin: '20px'
   },
   button: {
      width: 360,
      height: 50,
      [theme.breakpoints.down('sm')]: {
         width: 240,
         height: 70,
      }
   },
   buttonMargin: {
      marginBottom: 20,
      marginTop: 10,
      paddingLeft: '10px',
      paddingRight: '10px',
      width: 260
   }
}));
