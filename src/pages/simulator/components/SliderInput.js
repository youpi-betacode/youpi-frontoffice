import {makeStyles, withStyles} from "@material-ui/core";
import Slider from "@material-ui/core/Slider";
import Grid from "@material-ui/core/Grid";
import React from "react";
import {colors} from "../../../constants";

const PrettoSlider = withStyles({
  root: {
    color: '#52af77',
    height: 8,
    width:"100%"
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus,&:hover,&$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 4px)',
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
})(Slider);

export default function SliderInput({step, min, max, value, defaultValue, onChangeCommitted, label}) {

  const classes = useStyles();

  return (
    <React.Fragment>
      <Grid item md={12} className={classes.alignInMobile}>
        <p className={classes.filterTitle}>{label}</p>
      </Grid>
      <Grid item md={12} className={classes.alignInMobile}>
        <PrettoSlider
          step={step}
          min={min}
          max={max}
          value={value}
          defaultValue={defaultValue}
          valueLabelDisplay="auto"
          onChangeCommitted={onChangeCommitted}></PrettoSlider>
      </Grid>
    </React.Fragment>
  )
}

const useStyles = makeStyles(theme => ({
  alignInMobile:{
    [theme.breakpoints.down('sm')]: {
      width:"100%"
    }
  },
  filterTitle:{
    textAlign:"center",
    textTransform:"uppercase"
  }
}));
