import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';

import UploadPhoto from './UploadPhoto';
import CropPhoto from './CropPhoto';
import EditBestStep1 from './EditBestStep1';
import EditBestStep2 from "./EditBestStep2";
import EditAmbientStep from "./EditAmbientStep";

export default function StageSelector() {
  const classes = useStyles();

  const step = useSelector(state=> state.simulatorSteps.step);

  return (
    <div className={classes.selector}>
      {
        step === 0 &&
        <UploadPhoto/>
      }
      {
        step === 1 &&
        <CropPhoto />
      }
      {
        step === 2 &&
        <EditBestStep1 />
      }
      {
        step === 3 &&
        <EditBestStep2 />
      }
      {
        step === 4 &&
        <EditAmbientStep />
      }
    </div>
  )

}

const useStyles = makeStyles(theme => ({
  selector: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    flex: 1,
    transition: "all 0.3s",
  }
}));
