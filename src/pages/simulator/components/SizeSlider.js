import React from 'react';
import { withStyles} from '@material-ui/core/styles';
import Slider from '@material-ui/core/Slider';
import {colors} from "../../../constants";

const iOSBoxShadow =
  '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)';
const SizeSlider = withStyles({
  root: {
    color: colors.blue,
    height: 2,
    padding: '0px 0',
    marginTop:40,
    marginBottom:80,
    width:"80%"
  },
  thumb: {
    height: 28,
    width: 28,
    backgroundColor: '#fff',
    boxShadow: iOSBoxShadow,
    marginTop: -14,
    marginLeft: -14,
    '&:focus,&:hover,&$active': {
      boxShadow: '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.3),0 0 0 1px rgba(0,0,0,0.02)',
      // Reset on touch devices, it doesn't add specificity
      '@media (hover: none)': {
        boxShadow: iOSBoxShadow,
      },
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 11px)',
    top: -22,
    '& *': {
      background: 'transparent',
      color: colors.blue,
    },
  },
  track: {
    height: 3,
    backgroundColor: colors.blue,
  },
  rail: {
    height: 3,
    opacity: 1,
    backgroundColor: colors.blue,
  },
  mark: {
    backgroundColor: colors.blue,
    height: 30,
    width: 2,
    marginTop: -15,
  },
  markLabel:{
    color:colors.blue,
    opacity:1
  },
  markActive: {
    backgroundColor: 'currentColor',
  },
})(Slider);

export default SizeSlider;
