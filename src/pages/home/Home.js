import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  EmailShareButton,
} from 'react-share';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { colors } from '../../constants';

import ShareButton from '../../common/share/ShareButton'

export default function Home() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  return (
    <div className={classes.home}>
      <Footer/>
    </div>

  );
}


const useStyles = makeStyles(theme => ({
  home: {
    margin: 50
  },
  root: {
    borderRadius: 3,
    borderWidth: 2,
    paddingTop: 9,
    borderColor: colors.primary,
    color: 'black',
    height: 48,
    textAlign: 'center'
  }
}));