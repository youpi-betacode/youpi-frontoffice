import React, { useEffect } from 'react';
import { Grid } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { Helmet } from "react-helmet";
import { useTranslation } from 'react-i18next';
import makeStyles from "@material-ui/core/styles/makeStyles";
import PageContainer from '../../../common/pageContainer';
import NavbarProfile from '../../../common/navbar-profile';
import ListSimulations from './components/ListSimulations';
import { getUserInfo } from '../../../redux/actions/UserActions';
import Footer from "../../../common/footer/Footer";
import UserPicture from '../common/UserPicture';
import LastSimulation from '../common/LastSimulation';

export default function Simulations(props) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const classes = useStyles();

  useEffect(() => {
    dispatch(getUserInfo());
  },[])

  return(
    <div>
       <Helmet>
        <title>YOUPi! - Your Best Feeling - {t('simulations-tab-title')}</title>
      </Helmet>
      {/* <Header title='A NOSSA LOJA' subTitle='Não existe um caminho para a felicidade. A felicidade é o caminho' /> */}
      <PageContainer>
        <Grid container justify='space-evenly' spacing={3}>
          <Grid item xs={12} md={3}>
            <Grid container direction="column">
              <Grid item>
                <UserPicture/>
              </Grid>
              <Grid item className={classes.spacer}>
                <NavbarProfile/>
              </Grid>
              {/* <Grid item>
                <LastSimulation/>
              </Grid> */}
            </Grid>
          </Grid>
          <Grid item xs={12} md={9}>
            <ListSimulations/>
          </Grid>
        </Grid>
      </PageContainer>
      <Footer/>
    </div>
  )
}

const useStyles = makeStyles(theme => ({
  spacer: {
    marginTop: 24,
    marginBottom: 24,
  }
}));
