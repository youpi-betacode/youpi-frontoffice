import React, {useState} from 'react';
import {CardContent, Typography, Button, Icon, Grid} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import {useTranslation} from 'react-i18next';
import {useSelector, useDispatch} from 'react-redux';
import FrameCanvas from '../../../../common/frame/FrameCanvas';
import { generateShare } from '../../../../redux/actions/SimulatorActions';
import { convertURLToBase64 } from "../../../../common/utils";
import {colors, feeling_default_reference } from '../../../../constants';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import 'sweetalert2/dist/sweetalert2.css'
import Fab from "@material-ui/core/Fab";

import { sharePath } from '../../../../constants';
import {deleteSimulation} from '../../../../redux/actions/UserActions'
import { incrementProductOnShoppingCart } from "../../../../redux/actions/ShoppingCartActions";

function PanelIcon(props) {
  const classes = useStyles();


  if (props.expanded) {
    return <Icon className={classes.iconCarret}>keyboard_arrow_up</Icon>;
  } else {
    return <Icon className={classes.iconCarret}>keyboard_arrow_down</Icon>;
  }
}


function RemoveSimulationButton({simulation_id}) {
  const classes = useStyles();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const costumerID = useSelector(state=>state.user.id)

  const removeSimulation = () => {
    Swal.fire({
        imageUrl: '/images/letmeseee.png',
        imageWidth: 60,
        imageHeight: 60,
        title: t('remove-sim-prompt-title'),
        text: t('remove-sim-prompt-subtitle'),
        showCancelButton: true,
        customClass: {
          confirmButton: 'youpii-confirm-button-class',
          cancelButton: 'youpii-confirm-button-class',
        },
      }).then((result) => {
        if(result.value){
          dispatch(deleteSimulation(simulation_id, costumerID))
        }
      });
  };

  return (
    <Fab aria-label="close" className={classes.close} onClick={removeSimulation}>
      <Icon style={{fontSize:14}}>close</Icon>
    </Fab>
  )
}


function SimulationCardContent({sim, index}) {
  const classes = useStyles();
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const [expanded, setExpanded] = React.useState(false);

  const share = (e,type) => {
    e.preventDefault();
    let patternBase64;
    convertURLToBase64(sim.data.pattern.frame).then((base64) => {
      patternBase64 = base64;
      dispatch(generateShare(patternBase64, sim.data.url, type));
      // handleClose();
    });
  }

  const facebookShare = (image) =>{
    window.open(
      `https://www.facebook.com/sharer/sharer.php?u=${sharePath}${image.uuid}`
    );
  }

  const handleAddToCart = () => {
    debugger;
    const simulationData = {
      'simulation_data': {
        'original_photo': sim.data.original_photo,
        'small_filtered_photo': sim.data.url,
        'pattern': sim.data.pattern,
        'gama': sim.data.gama,
        'size': sim.data.size,
        'have_white_margin': sim.data.margin,
        'contrast': sim.data.contrast,
        'brightness': sim.data.brightness,
        'saturation': sim.data.saturate,
        'temperature': sim.data.posterize
      }
    }
    
    const feelingSize = sim.data.size[0] === "14.4x14.4 cm" ? "14.4_" : "23.6_"
    const feelingSplit = feeling_default_reference.replace("_", "_,").split(",");
    const firstFeelingSplit = feelingSplit.shift();
    
    const bestSize = sim.data.size[1] === "20x20 cm" ? "20_" : "30_";
    const bestSplit = sim.data.pattern.reference.replace("_", "_,").split(",");
    const firstBestSplit = bestSplit.shift();

    let bestReference = '';

     if(sim.data.pattern.reference === 'YBBLLX_2_UV')
        bestReference = 'YB_BL_LX_30_2_UV';
      else
        bestReference = firstBestSplit + bestSize + bestSplit;
      
      const feelingReference = firstFeelingSplit + feelingSize + feelingSplit;

    dispatch(incrementProductOnShoppingCart(bestReference));
    dispatch(incrementProductOnShoppingCart(feelingReference, simulationData));
  };

  return (
    <CardContent>
      <Grid container>
        <Grid item xs={10}>
        <Typography variant='body1' className={classes.date}>
          <b>#{index} {sim.date}</b>
        </Typography>
        </Grid>
      </Grid>
      <div className={classes.imageDiv}>
        <RemoveSimulationButton simulation_id={sim.id}/>
        <FrameCanvas image={sim.data.url} pattern={sim.data.pattern.frame} scale={0.6} noWhiteSpace={!sim.data.margin} profile={true} shadowBellow={true}/>
      </div>
      <div>
        <div className={classes.pannelHeadingContainer} onClick={() => setExpanded(!expanded)}>
          <h5 className={classes.pannelHeading}>{t('simulation-detail')}</h5>
          <PanelIcon expanded={expanded}/>
        </div>
        <div style={{height: expanded ? 100 : 0}} className={classes.expandedPannel}>
          <Typography variant='body1' className={classes.simCardLabel}>
            <strong>{t('simulation-size')}</strong> {sim.data.size[1]}
          </Typography>
          <Typography variant='body1' className={classes.simCardLabel}>
            <strong>{t('simulation-gama')}</strong> {t(sim.data.gama)}
          </Typography>
          <Typography variant='body1' className={classes.simCardLabel}>
            <strong>{t('simulation-pattern')}</strong> {sim.data.pattern.label}
          </Typography>
          <Typography variant='body1' className={classes.simCardLabel}>
            <strong>{t('simulation-margin')} </strong> {sim.data.margin ? 'Sim' : 'Não'}
          </Typography>
        </div>
      </div>
      <div style={{textAlign: 'center'}}>
       <Button size='large' className={classes.btn} color='primary' onClick={e => share(e,facebookShare)}>
         <b>{t('share')}</b>
       </Button>
       <Button size='large' className={classes.btn} color='primary' onClick={() => handleAddToCart()}>
         <b>{t('add-to-cart')}</b>
       </Button>
      </div>
    </CardContent>
  )
}

export default SimulationCardContent;

const useStyles = makeStyles(theme => ({
  pannelHeadingContainer: {
    display: "flex",
    [theme.breakpoints.down('sm')]: {
      display: "inherit"
    }
  },
  close: {
    top: 40,
    right: -184,
    width: 25,
    minHeight: "auto",
    height: 25,
    position: "relative",
    marginTop: -30,
    [theme.breakpoints.down('sm')]: {
      right: "-95px",
    }
  },
  expandedPannel: {
    overflow: "hidden",
    transition: "all ease 0.3s"
  },
  pannelHeading: {
    fontSize: 14,
    fontWeight: 800,
    color: colors.blue,
    margin: "1px 0px 10px 0px",
    cursor: "pointer",
    [theme.breakpoints.down('sm')]: {
      textAlign: "center"
    }
  },
  simCardLabel: {
    fontSize: 14
  },
  dateDiv: {
    marginBottom: 10
  },
  date: {
    color: colors.primary
  },
  imageDiv: {
    marginBottom: 10,
    marginTop: -10,
  },
  image: {
    width: 270,
    height: 270,
  },
  btn: {
    marginTop: 20,
    width: '100%',
    color: colors.white,
    padding: '10px 20px',
    '&:hover': {
      backgroundColor: colors.lightPrimary
    }
  },
  iconCarret: {
    color: colors.primary,
    marginLeft: "auto",
    cursor: "pointer"
  },
  deleteSim: {
    cursor: 'pointer',
    backgroundColor: `${colors.lightGrey} !important`,
    borderRadius: '50%'
  }
}))
