import React, { useEffect } from 'react';
import { Grid, Card, Typography, Hidden } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';

import SimulationCardContent from './SimulationCardContent';
import Loader from '../../../../common/loading/Loading';
import { getUserSimulations, getUserInfo } from '../../../../redux/actions/UserActions';
import { colors } from '../../../../constants';
import { useTranslation } from 'react-i18next'

function SimsNull() {
  const classes = useStyles();
  const { t } = useTranslation();
  return (
    <Loader loading="simulations">
      <Grid container>
        <Grid item xs={12}>
          <Typography variant='h4' className={classes.formTitle}>
            <b>{t('simulation-header')}</b>
          </Typography>
          <Typography className={classes.subtitle}>
            <b>{t('simulation-sub-header')}</b>
          </Typography>
          <div>
          </div>
        </Grid>
      </Grid>
    </Loader>
  )
}

function NoSims() {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <Loader loading="simulations">
      <Grid container>
      <Grid item xs={12}>
          <Typography variant='h4' className={classes.formTitle}>
            <b>{t('simulation-header')}</b>
          </Typography>
          <Typography className={classes.subtitle}>
            <b>{t('simulation-sub-header')}</b>
          </Typography>
        </Grid>
        <Grid item xs={12} style={{ textAlign: 'center' }}>
          <Typography component="h4">{t('no-simulations-available')}</Typography>
        </Grid>
      </Grid>
    </Loader>
  )
}


function ListSimulations() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const customer = useSelector(state => state.user);
  const { t } = useTranslation();

  useEffect(() => {
    if (!customer || !customer.id) {
      dispatch(getUserInfo());

    } else {
      dispatch(getUserSimulations(customer.id));
    }
  }, [customer.id]);

  // useEffect(() => {
  //   dispatch(getUserSimulations(customer.id));
  // }, [customer.id]);

  if (customer.simulations === null)
    return (
      <SimsNull />
    )
  else if (customer.simulations && customer.simulations.length === 0)
    return (
      <NoSims />
    )

  return (
    <Loader loading="simulations" >
      <Grid container>
        <Grid item xs={12}>
          <Typography variant='h4' className={classes.formTitle}>
            <b>{t('simulation-header')}</b>
          </Typography>
          <Typography className={classes.subtitle}>
            <b>{t('simulation-sub-header')}</b>
          </Typography>
        </Grid>
        {/* <Grid item xs={12} style={{textAlign: 'center'}}>
        { customer.simulations !== null && customer.simulations.length === 0 && <Typography component="h4">{t('no-simulations-available')}</Typography>}
        </Grid> */}
        {customer.simulations &&
          customer.simulations.map((item, index) => {
            return (
              <React.Fragment>
                <Hidden smDown>
                  <Grid item xs={12} md={3} key={index}>
                    <Card className={classes.card}>
                      <SimulationCardContent sim={item} index={index + 1} />
                    </Card>
                  </Grid>
                </Hidden>
                <Hidden mdUp>
                  <Grid item xs={12} md={4} key={index + 1}>
                    <Card className={classes.card} style={{ textAlign: 'center' }}>
                      <SimulationCardContent sim={item} index={index} />
                    </Card>
                  </Grid>
                </Hidden>
              </React.Fragment>
            )
          })
        }
      </Grid>
    </Loader>
  )
}

export default ListSimulations;

const useStyles = makeStyles(theme => ({
  formTitle: {
    marginBottom: 10,
    marginLeft: 15,
    color: colors.primary,
    fontSize: 18,
    textAlign: 'center'
  },
  subtitle: {
    color: "#86eed5",
    marginLeft: 15,
    fontFamily: "ComfortaaBold",
    fontSize: 15,
    marginBottom: 20,
    textAlign: 'center'
  },
  card: {
    backgroundColor: 'transparent',
    boxShadow: 'none',
    marginBottom: 10
  }
}))
