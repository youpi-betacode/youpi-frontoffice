import React, { useEffect, useState } from 'react';
import { Grid, Typography, TextField, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import { useSnackbar } from 'notistack';

import { colors } from '../../../../constants';
import * as userActions from '../../../../redux/actions/UserActions';

function UserDataForm() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();
  const user = useSelector(state => state.user);
  const [validForm, setValidForm] = useState(true);

  const handleChangeFirstName = (event) => {
    dispatch(userActions.changeFirstName(event.target.value));
    // setValidForm(true);
  };

  const handleChangeLastName = (event) => {
    dispatch(userActions.changeLastName(event.target.value));
    // setValidForm(true);
  };

  const handleChangeEmail = (event) => {
    dispatch(userActions.changeEmail(event.target.value));
    // setValidForm(true);
  };

  const handleChangeStreet = (event) => {
    dispatch(userActions.changeStreet(event.target.value));
    // setValidForm(true);
  }

  const handleChangeZipCode = (event) => {
    dispatch(userActions.changeZipCode(event.target.value));
    // setValidForm(true);
  }

  const handleChangeCity = (event) => {
    dispatch(userActions.changeCity(event.target.value));
    // setValidForm(true);
  };

  const handleChangeDateOfBirth = (date) => {
    dispatch(userActions.changeDateOfBirth(date));
    // setValidForm(true);
  };

  const handleChangeNif = (event) => {
    dispatch(userActions.changeNif(event.target.value));
  };

  const handleChangePhone = (event) => {
    dispatch(userActions.changePhone(event.target.value));
  };

  const handleOnClick = () => {

    const phone = user.phone ? user.phone : 0
    const nif = user.nif ? user.nif : 0;

    const data = {
      'email': user.email,
      'first_name': user.firstName,
      'last_name': user.lastName,
      // 'role': userReducer.role,
      'local': user.city,
      'contact': phone,
      //'password': userReducer.newPassword,
      'extra': {
        'NIF': nif,
        'date_of_birth': user.dateOfBirth,
        'zipCode': user.zipCode,
        'street': user.street,
        'name': `${user.firstName} ${user.lastName}`,
      }

    }

    // const data = new FormData;
    // data.append('email', userReducer.email);
    // data.append('first_name', userReducer.firstName);
    // data.append('last_name', userReducer.lastName);
    // data.append('role', userReducer.role);
    // data.append('password', 'password2');

    if (user.firstName && user.lastName && user.email && user.city && user.dateOfBirth
      && user.zipCode && user.street) {
      setValidForm(true);
      dispatch(userActions.submit(data));
    }
    else{
      setValidForm(false)
      enqueueSnackbar(t('fill-required-fields'), {
        variant: 'error',
        anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
        },
        autoHideDuration: 5000
      });
    }
  };

  useEffect(() => {
    dispatch(userActions.getUserInfo())
  }, [])

  return (
    <Grid container direction='row' justify='center'>
      <Grid item xs={12} className={classes.titles}>
        <Typography component="h4" variant='h4' className={classes.formTitle}>
          <b>{t('user-data-header')}</b>
        </Typography>
        <Typography variant='p' component="p" className={classes.subtitle}>
          <b>{t('user-data-sub-header')}</b>
        </Typography>
      </Grid>
      <Grid item xs={12} md={8}>
        <div>
          <Typography component="span" variant='body2' className={classes.labels}>
            {t('email-label')}
          </Typography>
          <TextField
            required
            fullWidth
            // label="EMAIL"
            //className={classes.textField}
            value={user.email}
            onChange={handleChangeEmail}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (validForm || user.email) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
        </div>
        {!(validForm || user.email) && <div className={classes.errorGrid}>
          <Typography component="span" variant='body2'>
            {t('insert-email-error')}
          </Typography>
        </div>}
      </Grid>
      <Grid item xs={12} md={8}>
        <div>
          <Typography component="span" variant='body2' className={classes.labels}>
            {t('first-name-label')}
          </Typography>
          <TextField
            required
            fullWidth
            // label={t('first-name-label')}
            //className={classes.textField}
            value={user.firstName}
            onChange={handleChangeFirstName}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (validForm || user.firstName) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
        </div>
        {!(validForm || user.firstName) && <div className={classes.errorGrid}>
          <Typography component="span" variant='body2'>
            {t('insert-first-name-error')}
          </Typography>
        </div>}
      </Grid>
      <Grid item xs={12} md={8}>
        <div>
          <Typography component="span" variant='body2' className={classes.labels}>
            {t('last-name-label')}
          </Typography>
          <TextField
            required
            fullWidth
            // label="ÚLTIMO NOME"
            //className={classes.textField}
            value={user.lastName}
            onChange={handleChangeLastName}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (validForm || user.lastName) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
        </div> {!(validForm || user.lastName) && <div className={classes.errorGrid}>
          <Typography component="span" variant='body2'>
            {t('insert-last-name-error')}
          </Typography>
        </div>}
      </Grid>
      <Grid item xs={12} md={8}>
        <div>
          <Typography component="span" variant='body2' className={classes.labels}>
            {t('date-of-birth-label')}
          </Typography>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              className={classes.datePickerInput}
              inputVariant="outlined"
              disableFuture={true}
              variant='dialog'
              format="dd/MM/yyyy"
              margin="normal"
              id="date-picker-dialog"
              format="dd/MM/yyyy"
              value={user.dateOfBirth}
              onChange={handleChangeDateOfBirth}
              KeyboardButtonProps={{
                'aria-label': 'change date',
              }}
              InputProps={{
                classes: {
                  root: classes.text,
                  notchedOutline: (validForm || user.dateOfBirthFilled) ? classes.notchedOutline : classes.errorNotchedOutline
                }
              }}
            />
          </MuiPickersUtilsProvider>
          {/* <TextField
            required
            fullWidth
            // label="DATA DE NASCIMENTO"
            //className={classes.textField}
            value={userReducer.dateOfBirth}
            onChange={handleChangeDateOfBirth}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (validForm || userReducer.dateOfBirth) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          /> */}
        </div>
        {!(validForm || user.dateOfBirth) && <div className={classes.errorGrid}>
          <Typography component="span" variant='body2'>
            {t('insert-date-of-birth-error')}
          </Typography>
        </div>}
      </Grid>
      <Grid item xs={12} md={8}>
        <div>
          <Typography component="span" variant='body2' className={classes.labels}>
            {t('address-label')}
          </Typography>
          <TextField
            fullWidth
            //className={classes.textField}
            value={user.street}
            onChange={handleChangeStreet}
            margin="normal"
            variant="outlined"
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (validForm || user.street) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
        </div>
        {!(validForm || user.street) && <div className={classes.errorGrid}>
          <Typography component="span" variant='body2'>
            {t('insert-street-error')}
          </Typography>
        </div>}
      </Grid>
      <Grid container direction='row' justify='space-evenly'>
        <Grid item xs={12} md={3}>
          <div>
            <Typography variant='body2' className={classes.labels}>
              {t('zip-code-label')}
            </Typography>
            <TextField
              fullWidth
              //label={t('zip-code-label')}
              //className={classes.textField}
              value={user.zipCode}
              onChange={handleChangeZipCode}
              margin="normal"
              variant="outlined"
              InputProps={{
                classes: {
                  root: classes.text,
                  notchedOutline: (validForm || user.zipCode) ? classes.notchedOutline : classes.errorNotchedOutline
                }
              }}
            />
          </div>
          {!(validForm || user.zipCode) && <div className={classes.errorGrid}>
            <Typography component="span" variant='body2'>
              {t('insert-zipCode-error')}
            </Typography>
          </div>}
        </Grid>
        <Grid item xs={12} md={3}>
          <div>
            <Typography variant='body2' className={classes.labels}>
              {t('city-label')}
            </Typography>
            <TextField
              fullWidth
              //label={t('city-label')}
              //className={classes.textField}
              value={user.city}
              onChange={handleChangeCity}
              margin="normal"
              variant="outlined"
              InputProps={{
                classes: {
                  root: classes.text,
                  notchedOutline: (validForm || user.city) ? classes.notchedOutline : classes.errorNotchedOutline
                }
              }}
            />
          </div>
          {!(validForm || user.city) && <div className={classes.errorGrid}>
            <Typography component="span" variant='body2'>
              {t('insert-city-error')}
            </Typography>
          </div>}
        </Grid>
      </Grid>
      <Grid item xs={12} md={8}>
        <div>
          <Typography component="span" variant='body2' className={classes.labels}>
            {t('phone-label')}
          </Typography>
          <TextField
            fullWidth
            placeholder={t('phone-placeholder')}
            //className={classes.textField}
            value={user.phone}
            onChange={handleChangePhone}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: classes.notchedOutline
              }
            }}
          />
        </div>
      </Grid>
      <Grid item xs={12} md={8}>
        <div>
          <Typography component="span" variant='body2' className={classes.labels}>
            {t('nif-label')}
          </Typography>
          <TextField
            fullWidth
            placeholder={t('nif-placeholder')}
            //className={classes.textField}
            value={user.nif}
            onChange={handleChangeNif}
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: false
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: classes.notchedOutline
              }
            }}
          />
        </div>
      </Grid>
      <Grid item xs={12} className={classes.btnGrid}>
        <Button size='large' className={classes.btn} onClick={handleOnClick} color="primary">
          <b>Atualizar</b>
        </Button>
      </Grid>
    </Grid>
  )
}

export default UserDataForm;

const useStyles = makeStyles(theme => ({
  formTitle: {
    color: colors.primary,
    fontSize: 18,
    marginBottom: 10
  },
  subtitle: {
    color: "#86eed5",
    fontFamily: "ComfortaaBold",
    fontSize: 15,
    marginBottom: 20
  },
  notchedOutline: {
    borderWidth: '1px',
    borderColor: colors.primary + ' !important',
    borderRadius: 7,
  },
  cssLabel: {
    color: colors.primary,
    backgroundColor: colors.white,
    paddingLeft: 2
  },
  btnGrid: {
    textAlign: 'center'
  },
  btn: {
    marginTop: 30,
    marginBottom: 30,
    padding: '10px 40px',
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.8)'
    }
  },
  errorNotchedOutline: {
    borderWidth: '1px',
    borderColor: colors.errorPink + ' !important',
    borderRadius: 7,
  },
  errorGrid: {
    textAlign: 'left',
    color: `${colors.errorPink} !important`,
    marginBottom: 10
  },
  titles: {
    textAlign: 'center'
  },
  text: {
    color: `${colors.htmlGrey} !important`
  },
  labels: {
    color: colors.primary,
    marginBottom: '-10px',
    marginTop: 10
  },
  datePickerInput: {
    width: '100%'
  }
}))
