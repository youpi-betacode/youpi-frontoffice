import React, { useState } from 'react';
import { Grid, TextField, Button, Typography, InputAdornment, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Visibility, VisibilityOff } from '@material-ui/icons';

import { colors } from '../../../../constants';
import { changePassword } from '../../../../redux/actions/UserActions';

function ChangePasswordForm() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [newPassword, setNewPassword] = useState('');
  const [confirmNewPassword, setConfirmNewPassword] = useState('');
  const [missmatchPws, setMissmatchPws] = useState(false);
  const [invalidPassword, setInvalidPassword] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmation, setShowConfirmation] = useState(false);

  const onPasswordChange = (event) => {
    setNewPassword(event.target.value);
    setMissmatchPws(false);
    setInvalidPassword(false);
  }

  const onConfirmChange = (event) => {
    setConfirmNewPassword(event.target.value);
    setMissmatchPws(false);
    setInvalidPassword(false);
  }

  const handleOnClick = () => {
    const passwordRegExp = /^.*(?=.{6,})(?=.*[A-Za-z])(?=.*\d)(?=.*[\W])*.*$/;

    const data = {
      'password': newPassword
    };

    if (!passwordRegExp.test(newPassword))
      setInvalidPassword(true)
    else if (newPassword !== confirmNewPassword)
      setMissmatchPws(true);
    else
      dispatch(changePassword(data));
  };

  const handleOnKeyUp = (event) => {
    if (event.key === 'Enter')
      handleOnClick();
  }

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  }

  const handleClickShowConfirmation = () => {
    setShowConfirmation(!showConfirmation);
  }

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  return (
    <Grid container direnction="row" justify="center" className={classes.mainDiv}>
      <Grid item xs={12} md={8}>
        <div>
          <Typography variant='body2' className={classes.labels}>
            {t('new-password-label')}
          </Typography>
          <TextField
            type={showPassword ? 'text' : 'password'}
            fullWidth
            value={newPassword}
            onChange={event => onPasswordChange(event)}
            onKeyUp={handleOnKeyUp}
            margin='normal'
            variant='outlined'
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: classes.notchedOutline
              },
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    edge="end"
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </div>
      </Grid>
      <Grid item xs={12} md={8}>
        <div>
          <Typography variant='body2' className={classes.labels}>
            {t('confirm-new-password-label')}
          </Typography>
          <TextField
            type={showConfirmation ? 'text' : 'password'}
            fullWidth
            value={confirmNewPassword}
            onChange={event => onConfirmChange(event)}
            onKeyUp={handleOnKeyUp}
            margin='normal'
            variant='outlined'
            InputLabelProps={{
              classes: {
                root: classes.cssLabel
              },
              shrink: true
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: classes.notchedOutline
              },
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    edge="end"
                    aria-label="toggle password visibility"
                    onClick={handleClickShowConfirmation}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {showConfirmation ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </div>
        {invalidPassword && <div>
          <Typography variant='body2' className={classes.error}>
            {t('password-rules')}
          </Typography>
        </div>}
        {missmatchPws && <div>
          <Typography variant='body2' className={classes.error}>
            {t('passwords-dont-match-error')}
          </Typography>
        </div>}
      </Grid>
      <Grid item xs={12} className={classes.btnGrid}>
        <Button size='large' className={classes.btn} onClick={handleOnClick} color="primary">
          <b>Submeter</b>
        </Button>
      </Grid>
    </Grid>
  )
}

export default ChangePasswordForm;

const useStyles = makeStyles(theme => ({
  notchedOutline: {
    borderWidth: '1px',
    borderColor: colors.primary + ' !important',
    borderRadius: 7,
  },
  cssLabel: {
  },
  mainDiv: {
    marginTop: 30
  },
  btn: {
    marginTop: 30,
    color: colors.white,
    padding: '10px 40px',
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.8)'
    }
  },
  btnGrid: {
    textAlign: 'center'
  },
  error: {
    color: `${colors.errorPink} !important`
  },
  text: {
    color: colors.htmlGrey
  },
  labels: {
    color: colors.primary,
    marginBottom: '-10px',
    marginTop: 10
  }
}))
