import React, { useEffect } from 'react'
import { Grid, Divider } from '@material-ui/core';
import { Helmet } from "react-helmet";
import { useTranslation } from 'react-i18next';
import {animateScroll as scroll} from "react-scroll/modules";

import PageContainer from '../../../common/pageContainer';
import NavbarProfile from '../../../common/navbar-profile';
import Footer from "../../../common/footer/Footer";
import UserDataForm from './components/UserDataForm';
import ChangePasswordForm from './components/ChangePasswordForm';
import Loader from '../../../common/loading/Loading';
import makeStyles from "@material-ui/core/styles/makeStyles";
import UserPicture from '../common/UserPicture';
import LastSimulation from '../common/LastSimulation';

export default function UserProfile() {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <div>
      <Helmet>
        <title>YOUPi! - Your Best Feeling - {t('profile-tab-title')}</title>
      </Helmet>
      <Loader loading="userInfo">
        {/* <Header title='A NOSSA LOJA' subTitle='Não existe um caminho para a felicidade. A felicidade é o caminho' /> */}
        <PageContainer>
          <Grid container>
            <Grid item xs={12} md={3}>
              <Grid container direction="column">
                <Grid item>
                  <UserPicture/>
                </Grid>
                <Grid item className={classes.spacer}>
                  <NavbarProfile/>
                </Grid>
                {/* <Grid item >
                  <LastSimulation/>
                </Grid> */}
              </Grid>
            </Grid>
            <Grid item xs={12} md={9} >
              <Grid container className={classes.userDataForm} direction='column' justify='center' alignItems='center'>
                <UserDataForm />
                <Divider />
                <ChangePasswordForm />
              </Grid>
            </Grid>
          </Grid>
        </PageContainer>
      </Loader>
      <Footer />
    </div>
  )
}

const useStyles = makeStyles(theme => ({
  userDataForm: {
    "padding-right": "100px !important;",
    [theme.breakpoints.down('sm')]: {
      paddingRight: "0px !important",
      maxWidth: "80%"
    },
    maxWidth: 700,
    margin: 'auto',
    marginTop: 20
  },
  spacer: {
    marginTop: 24,
    marginBottom: 24,

  }
}));
