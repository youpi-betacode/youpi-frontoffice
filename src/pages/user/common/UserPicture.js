import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Hidden } from '@material-ui/core'

export default function UserPicture() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Hidden mdUp>
      <Grid container direction='column' justify='center' alignItems='center' spacing={4}>
      <Grid item xs={12} md={2}>
        
      </Grid>
      <Grid item xs={12} md={5} className={classes.layout}>
        <div className={classes.imageBorder}>
          <div className={classes.imageMargin}>
            <img src="/images/mrfancy.png" className={classes.imageHolder}/>
          </div>
        </div>
      </Grid>
      <Grid item xs={12} md={5} className={classes.emailLayout}>
        <img src="/images/envelope.png" className={classes.emailIcon}/>
      </Grid>
    </Grid>
      </Hidden>
      <Hidden smDown>
      <Grid container justify={'space-evenly'}>
        <Grid item xs={3} md={2}>
          
        </Grid>
        <Grid item xs={9} md={5} className={classes.layout}>
          <div className={classes.imageBorder}>
            <div className={classes.imageMargin}>
              <img src="/images/mrfancy.png" className={classes.imageHolder}/>
            </div>
          </div>
        </Grid>
        <Grid item xs={3} md={5} className={classes.emailLayout}>
          <img src="/images/envelope.png" className={classes.emailIcon}/>
        </Grid>
      </Grid>
      </Hidden>
    </React.Fragment>
  )
};

const useStyles = makeStyles(theme => ({
  layout: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 22,
  },
  imageBorder: {
    height: 80,
    width: 80,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'red',
    background: "#745334", /* Old browsers */
    background: "-moz-linear-gradient(left,  #745334 0%, #fabc87 51%, #745334 93%, #745334 100%)", /* FF3.6-15 */
    background: "-webkit-linear-gradient(left,  #745334 0%,#fabc87 51%,#745334 93%,#745334 100%)", /* Chrome10-25,Safari5.1-6 */
    background: "linear-gradient(to right,  #745334 0%,#fabc87 51%,#745334 93%,#745334 100%)", /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: "progid:DXImageTransform.Microsoft.gradient( startColorstr='#745334', endColorstr='#745334',GradientType=1 )" /* IE6-9 */
  },
  imageMargin: {
    height: '85%',
    width: '85%',
    backgroundColor: 'white',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageHolder: {
    height: '85%',
    width: '85%',
    backgroundColor: 'white'
  },
  emailLayout: {
    //textAlign: '',
  },
  emailIcon: {
    maxWidth: 30,       
  }

}));

