import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { colors } from '../../../constants';
// import { useDispatch, useSelector } from 'react-redux';
// import { getUserLastSimulation } from '../../../redux/actions/userActions';

export default function LastSimulation() {
  const classes = useStyles();
  const { t } = useTranslation();
  // const dispatch = useDispatch();
  // const costumerId = useSelector(state => state.userReducer.ud);

  // useEffect(()=>{
  //   if(costumerId>0){
  //     dispatch(getUserLastSimulation(costumerId));
  //   }
  // }, [costumerId])


  return (
    <Grid container direction={'column'} justify='center' alignItems='center'>
      <Grid item xs={12} className={classes.spacer}>
        <Typography variant='h4' className={classes.formTitle}>
          {t('last-seen-youpii')}
        </Typography>
      </Grid>
      <Grid item xs={12}>        
        <img src="images/YoupiiBigLogo2.jpg" className={classes.image}/>      
      </Grid>
    </Grid>
  )
};

const useStyles = makeStyles(theme => ({
  spacer: {
    marginBottom: 20,
    marginTop: 135
  },
  formTitle: {
    color: colors.primary,
    fontSize: 18,
    fontWeight: 'bold'
  },
  image: {
    height: 175,
    width: 175
  }
}));

