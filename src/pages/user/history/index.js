import React, { useEffect } from 'react';
import { Helmet } from "react-helmet";
import { useTranslation } from 'react-i18next';
import { Grid } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/styles';

import PageContainer from '../../../common/pageContainer';
import Loader from '../../../common/loading/Loading';
import Table from './components/table';
import UserPicture from '../common/UserPicture';
import { getUserPurchaseHistory } from '../../../redux/actions/UserActions';
import NavbarProfile from '../../../common/navbar-profile';

function History() {
  const classes = useStyles();
  const dispatch = useDispatch()
  const { t } = useTranslation();

  const historyPage = useSelector(state => state.user.historyPage);

  useEffect(() => {
    dispatch(getUserPurchaseHistory(historyPage))
  }, [historyPage])

  return (
    <div>
      <Helmet>
        <title>YOUPi! - Your Best Feeling - {t('profile-tab-title')}</title>
      </Helmet>
      <Loader loading="userHistory">
        <PageContainer>
          <Grid container>
            <Grid xs={12} md={3}>
              <Grid container direction="column">
                <Grid item>
                  <UserPicture />
                </Grid>
                <Grid item className={classes.spacer}>
                  <NavbarProfile />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} md={9} >
              <Grid container className={classes.tableGrid} direction='column' justify='center' alignItems='center'>
                <Table/>
              </Grid>
            </Grid>
          </Grid>
        </PageContainer>
      </Loader>
    </div>
  )
}

export default History;

const useStyles = makeStyles ((theme) => ({
  spacer: {
    marginTop: 24,
    marginBottom: 24,
    maxWidth: "80%"

  },
  tableGrid: {
   [theme.breakpoints.up('md')]: {
     paddingRight: 100
   }
  }
}))