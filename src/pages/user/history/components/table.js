import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/styles'

import TableItem from './tableItem'
import { colors } from '../../../../constants';
import { setHistoryPage } from '../../../../redux/actions/UserActions';

function Table() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const user = useSelector(state => state.user);

  const handleChangePage = (page) => {
    dispatch(setHistoryPage(page))
  }

  return (
    <React.Fragment>
      <Grid container>
        <Grid item xs={12}  style={{textAlign: 'right'}}>
          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end', marginRight: 10 }}>
            {user.historyPages.map(page =>
              <div key={page} style={{ marginLeft: 2, marginRight: 2}}>
                <Typography variant='body2' 
                  className={user.historyPage == page ? classes.selectedPage : classes.viewsText}
                  onClick={() => handleChangePage(page)}
                >
                    <b>{page}</b>
                </Typography>
              </div>
            )}
          </div>
        </Grid>
      </Grid>
      {user.purchaseHistory.map((item, index) =>
        <Grid container>
          <Grid item xs={12} style={{ textAlign: 'center' }}>
            <TableItem key={index} item={item} />
          </Grid>
        </Grid>
      )}
    </React.Fragment>
  )
}

export default Table;

const useStyles = makeStyles(() => ({
  viewsText: {
    textTransform: 'uppercase',
    color: colors.darkerPrimary,
    cursor: 'pointer'
  },
  selectedPage: {
    textDecoration: `underline ${colors.darkerPrimary}`,
    textTransform: 'uppercase',
    color: colors.darkerPrimary,
    cursor: 'pointer'
  }
}))