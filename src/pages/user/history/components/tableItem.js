import React from 'react';
import { Grid, Typography, Paper, Hidden } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/styles';
import clsx from 'clsx';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router';

import { colors } from '../../../../constants';

function TableItem({ item }) {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { t } = useTranslation();

  const handleOnClick = (uuid) => {
    dispatch(push(`/checkout/status/${uuid}`));
  }

  return (
    <Paper className={classes.paper}>
      <Grid container className={classes.container}>
        <Grid item xs={12} sm={6} className={classes.leftGrids}>
          <Typography variant='body2'>
            {`${t('order-date')}: ${item.timestamp}`}
            &nbsp;|&nbsp;
            {`${item.cart.total_products-1} ${t('items')}`}
        </Typography>
        </Grid>
        <Hidden xsDown>
          <Grid item xs={12} sm={6} className={classes.rightGrids}>
            <Typography variant="body2" className={clsx(classes.text,classes.details)} onClick={() => handleOnClick(item.uuid)}>
              {t('details')}
            </Typography>
          </Grid>
        </Hidden>
        {/* <Grid item xs={6} className={classes.rightGrids}>
          <Typography variant='body2'>
            {`${item.cart.total_products-1} ${t('items')}`}
          </Typography>
        </Grid> */}
        <Grid item xs={12} sm={6} className={clsx(classes.leftGrids,classes.secondRow)}>
          <Typography variant='body2' className={classes.text}>
            {`${t('total')}: ${item.total}€`}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6} className={clsx(classes.rightGrids,classes.secondRow)}>
          <Typography variant='body2' className={classes.text}>
            {`${t('payment-type')}: ${item.payment_type}`}
          </Typography>
        </Grid>
        <Hidden smUp>
          <Grid item xs={12} sm={6} className={classes.rightGrids} style={{marginTop: 15}}>
            <Typography variant="body2" className={clsx(classes.text,classes.details)} onClick={() => handleOnClick(item.uuid)}>
              {t('details')}
            </Typography>
          </Grid>
        </Hidden>
      </Grid>
    </Paper>
  )
}

export default TableItem;

const useStyles = makeStyles((theme) => ({
  leftGrids: {
    textAlign: 'left',
  },
  rightGrids: {
    [theme.breakpoints.up('sm')]: {
      textAlign: 'right'
    },
    [theme.breakpoints.down('xs')]: {
      textAlign: 'left'
    },
  },
  text: {
    textTransform: 'capitalize'
  },
  paper: {
    margin: 10,
  },
  container: {
    padding: 10,
  },
  secondRow: {
    [theme.breakpoints.up('md')]: {
      marginTop: 5
    }
  },
  details: {
    cursor: 'pointer',
    color: colors.primary
  }
}))