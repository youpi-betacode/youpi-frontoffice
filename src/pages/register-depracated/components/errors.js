import React from 'react';
import { Grid } from '@material-ui/core';
import { useSelector } from 'react-redux';

const differentPasswords = 'As palavra-passe introduzidas são diferentes';
const invalidEmail = 'Insira um email válido';
const invalidPhone = 'Insira um contacto válido';
const invalidNif = 'Insira um NIF válido';

function Errors() {
  const error = useSelector(state => state.register);

  return (
    <div>
      {error.confirmPasswordError &&
        <Grid item xs={12}>
          {differentPasswords}
        </Grid>
      }

      {error.emailError &&
        <Grid item xs={12}>
          {invalidEmail}
        </Grid>
      }

      {error.phoneError &&
        <Grid item xs={12}>
          {invalidPhone}
        </Grid>
      }

      {error.nifError &&
        <Grid item xs={12}>
          {invalidNif}
        </Grid>
      }
    </div>
  )
}

export default Errors;