import React, { useState } from 'react';
import { Grid, TextField, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';

import { colors } from '../../../constants';
import * as register from '../../../redux/actions/RegisterActions';
import Errors from '../components/errors';
import { matchPasswords, checkEmail, checkPhone, checkNif } from '../../../common/formValidation/validationUtils';
import { useTranslation } from 'react-i18next';

function TextInputs({ hide = false }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const registerForm = useSelector(state => state.register);
  const filteredPhoto = useSelector(state => state.simulator.croppedCanvas);
  const pattern = useSelector(state => state.simulator.pattern);
  const gama = useSelector(state => state.simulator.gama);
  const size = useSelector(state => state.simulator.size);
  const margin = useSelector(state => state.simulator.whiteSpace);
  const { t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();

  const [validForm, setValidForm] = useState(false);
  const [showErrors, setShowErrors] = useState(false);
  const requiredError = 'Campos obrigatórios';
  // const [firstName, setFirstName] = useState(false);
  // const [lastName, setLastName] = useState(false);
  // const [email, setEmail] = useState(false);
  // const [phone, setPhone] = useState(false);
  // const [city, setCity] = useState(false);
  // const [password, setPassword] = useState(false);
  // const [confirmPassword, setConfirmPassword] = useState(false);

  const handleChangeFirstName = (event) => {
    dispatch(register.setFirstName(event.target.value));
  }

  const handleChangeLastName = (event) => {
    dispatch(register.setLastName(event.target.value));
  }

  const handleChangeEmail = (event) => {
    dispatch(register.setEmail(event.target.value));
  }

  const handleChangeCity = (event) => {
    dispatch(register.setCity(event.target.value));
  }

  const handleChangePhone = (event) => {
    dispatch(register.setPhone(event.target.value));
  }

  const handleChangeNif = (event) => {
    dispatch(register.setNif(event.target.value));
  }

  const handleChangeDateOfBirth = (event) => {
    dispatch(register.setDateOfBirth(event.target.value));
  }

  const handleChangePassword = (event) => {
    dispatch(register.setPassword(event.target.value));
  };

  const handleChangeConfirmPassword = (event) => {
    dispatch(register.setConfirmPassword(event.target.value));
  };

  // const checkFilled = () => {

  //   registerForm.firstName === '' ? registerReducer.toggleFirstNameError(true) : registerReducer.toggleFirstNameError(false)
  //   registerForm.lastName ? registerReducer.toggleLastNameError(false) : registerReducer.toggleLastNameError(true)
  //   registerForm.email ? registerReducer.toggleEmailError(false) : registerReducer.toggleEmailError(true)
  //   registerForm.city ? registerReducer.toggleCityError(false) : registerReducer.toggleCityError(true)
  //   //registerForm.dateOfBirth ? registerReducer.toggleDateOfBirtError(true) : registerReducer.toggleDateOfBirtError(false);
  //   //registerForm.nif ? registerReducer.toggleNifError(true) : registerReducer.toggleNifError(false);
  //   registerForm.phone ? registerReducer.togglePhoneError(false) : registerReducer.togglePhoneError(true)
  //   registerForm.password ? registerReducer.togglePasswordError(false) : registerReducer.togglePasswordError(true)
  //   registerForm.confirmPassword ? registerReducer.toggleConfirmPasswordError(false) : registerReducer.toggleConfirmPasswordError(true)


  //   if (registerForm.firstNameError && registerForm.lastName && registerForm.email
  //     && registerForm.city && registerForm.phone && registerForm.password && registerForm.confirmPassword)
  //     return true;
  //   return false
  // }

  // const validateForm = () => {
  //   let check = checkFilled();
  //   if (!check)
  //     return false;
  //   if (!matchPasswords(registerForm.password, registerForm.confirmPassword));
  //   return false;
  //   if (!checkEmail(registerForm.email));
  //   return false
  //   if (!checkPhone(registerForm.phone));
  //   return false
  //   if (!checkNif(registerForm.nif))
  //     return false
  //   return true
  // }

  const handleOnClick = () => {
    // const data = new FormData;
    // data.append('first_name', registerForm.firstName);
    // data.append('last_name', registerForm.lastName);
    // data.append('email', registerForm.email);
    // data.append('local', registerForm.city);
    // data.append('contact', registerForm.phone);

    const phone = registerForm.phone ? registerForm.phone : 0;
    const nif = registerForm.nif ? registerForm.nif : 0;
    const date = new Date();

    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    const month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : date.getMonth();

    const data = {
      'email': registerForm.email,
      'password': registerForm.password,
      'first_name': registerForm.firstName,
      'last_name': registerForm.lastName,
      'local': registerForm.city,
      'contact': phone,
      'extra': {
        'nif': nif,
        'date_of_birth': registerForm.dateOfBirth,
        'name': `${registerForm.firstName} ${registerForm.lastName}`
      }
    }

    const info = {
      'info': {
        'url': filteredPhoto,
        'pattern': pattern,
        'gama': gama,
        'size': size,
        'margin': margin ? 'Sim' : 'Não',
        'date': `${day}-${month}-${date.getFullYear()}`
      }
    };

    const login = {
      'identifier': registerForm.email,
      'password': registerForm.password
    }

    dispatch(register.submitRegister(data, info, login));

    // let valid = validateForm();

    // if(valid){
    //   dispatch(registerReducer.submitRegister(data));
    //   dispatch(registerReducer.toggleModal(false));
    // }
    // else{
    //   setShowErrors(true);
    // }
  }

  const success = useSelector(state => state.simulation.successSave)

  const handleOnKeyUp = (event) => {
    if (event.key === 'Enter')
      handleOnClick();
  }

  return (
    <Grid container className={classes.gridContainer}>
      <Grid item xs={12}>
        <Typography variant='h4' className={classes.formTitle}>
          {t('register-header')}
        </Typography>
      </Grid>
      <Grid item xs={6}>
        <TextField
          error={registerForm.firstNameError}
          required
          fullWidth
          label="Primeiro Nome"
          //className={classes.textField}
          value={registerForm.firstName}
          onKeyUp={handleOnKeyUp}
          onChange={handleChangeFirstName}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            classes: {
              root: classes.cssLabel
            }
          }}
          InputProps={{
            classes: {
              root: classes.cssOutlinedInput,
              notchedOutline: classes.notchedOutline
            }
          }}
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          required
          fullWidth
          label="Último Nome"
          //className={classes.textField}
          value={registerForm.lastName}
          onKeyUp={handleOnKeyUp}
          onChange={handleChangeLastName}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            classes: {
              root: classes.cssLabel
            }
          }}
          InputProps={{
            classes: {
              root: classes.cssOutlinedInput,
              notchedOutline: classes.notchedOutline
            }
          }}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          required
          fullWidth
          label="Email"
          //className={classes.textField}
          value={registerForm.email}
          onKeyUp={handleOnKeyUp}
          onChange={handleChangeEmail}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            classes: {
              root: classes.cssLabel
            }
          }}
          InputProps={{
            classes: {
              root: classes.cssOutlinedInput,
              notchedOutline: classes.notchedOutline
            }
          }}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          required
          fullWidth
          label="Localidade"
          //className={classes.textField}
          value={registerForm.city}
          onKeyUp={handleOnKeyUp}
          onChange={handleChangeCity}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            classes: {
              root: classes.cssLabel
            }
          }}
          InputProps={{
            classes: {
              root: classes.cssOutlinedInput,
              notchedOutline: classes.notchedOutline
            }
          }}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          required
          fullWidth
          wrap='true'
          label="DATA DE NASCIMENTO (DD-MM-AAAA)"
          //className={classes.textField}
          value={registerForm.dateOfBirth}
          onKeyUp={handleOnKeyUp}
          onChange={handleChangeDateOfBirth}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            classes: {
              root: classes.cssLabel
            }
          }}
          InputProps={{
            classes: {
              root: classes.cssOutlinedInput,
              notchedOutline: classes.notchedOutline
            }
          }}
        />
      </Grid>
      {!hide &&
        <React.Fragment>
          <Grid item xs={12}>
            <TextField
              fullWidth
              wrap='true'
              label="NIF (Opcional)"
              //className={classes.textField}
              value={registerForm.nif}
              onKeyUp={handleOnKeyUp}
              onChange={handleChangeNif}
              margin="normal"
              variant="outlined"
              InputLabelProps={{
                classes: {
                  root: classes.cssLabel
                }
              }}
              InputProps={{
                classes: {
                  root: classes.cssOutlinedInput,
                  notchedOutline: classes.notchedOutline
                }
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              required
              fullWidth
              label="Telefone (Opcional)"
              //className={classes.textField}
              value={registerForm.phone}
              onKeyUp={handleOnKeyUp}
              onChange={handleChangePhone}
              margin="normal"
              variant="outlined"
              InputLabelProps={{
                classes: {
                  root: classes.cssLabel
                }
              }}
              InputProps={{
                classes: {
                  root: classes.cssOutlinedInput,
                  notchedOutline: classes.notchedOutline
                }
              }}
            />
          </Grid>
        </React.Fragment>
      }
      <Grid item xs={12}>
        <TextField
          required
          fullWidth
          type='password'
          label="Password"
          //className={classes.textField}
          value={registerForm.setPassword}
          onKeyUp={handleOnKeyUp}
          onChange={handleChangePassword}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            classes: {
              root: classes.cssLabel
            }
          }}
          InputProps={{
            classes: {
              root: classes.cssOutlinedInput,
              notchedOutline: classes.notchedOutline
            }
          }}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          required
          fullWidth
          type='password'
          label="Confirmar Password"
          //className={classes.textField}
          value={registerForm.confirmPassword}
          onKeyUp={handleOnKeyUp}
          onChange={handleChangeConfirmPassword}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            classes: {
              root: classes.cssLabel
            }
          }}
          InputProps={{
            classes: {
              root: classes.cssOutlinedInput,
              notchedOutline: classes.notchedOutline
            }
          }}
        />
      </Grid>
      <Errors />
      <Grid item xs={12} className={classes.btnGrid}>
        <Button size='large' className={classes.btn} onClick={handleOnClick}>
          <b>{t('register-button')}</b>
        </Button>
      </Grid>
    </Grid>
  )
}

export default TextInputs;

const useStyles = makeStyles(theme => ({
  gridContainer: {
    maxWidth: 500,
    margin: 'auto'
  },
  formTitle: {
    textAlign: 'center',
    marginBottom: 40,
    color: colors.black
  },
  btnGrid: {
    textAlign: 'center'
  },
  btn: {
    marginTop: 40,
    backgroundColor: colors.primary,
    color: colors.white,
    padding: '10px 25px',
    '&:hover': {
      backgroundColor: colors.lightPrimary
    }
  },
  notchedOutline: {
    borderWidth: '1px',
    borderColor: colors.primary,
    borderRadius: 7,
  },
  cssOutlinedInput: {
    "&:not(hover):not($disabled):not($cssFocused):not($error) $notchedOutline": {
      borderColor: colors.primary
    },
    "&:hover:not($disabled):not($cssFocused):not($error) $notchedOutline": {
      borderColor: colors.primary
    }
  },
  cssLabel: {
    color: colors.primary + '!important',
    textAlign: 'left'
  }
}))
