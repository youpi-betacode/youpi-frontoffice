import React from 'react';

import PageContainer from '../../../common/pageContainer';
import TextInputs from './textInputs';

function RegisterForm() {

  return (
    <PageContainer>
      <TextInputs/>
    </PageContainer>
  )
}

export default RegisterForm;