import React, { useEffect } from 'react';
import { Grid, Typography, MenuItem, Select } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';

import simulationOptions from '../../../../redux/reducers/simulator-options';
import { colors } from '../../../../constants';
import { setSubfilter, setStorePage, setDesc, setOrderBy } from '../../../../redux/actions/StoreActions';

function Pagination() {
   const classes = useStyles();
   const { t } = useTranslation();
   const dispatch = useDispatch();
   const store = useSelector(state => state.store);

   const handlChangePage = (page) => {
      dispatch(setStorePage(page))
   }


   return (

      <Grid item xs={12} sm={6} className={classes.views}>
         <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
            <div style={{ marginRight: 3 }}>
               <Typography variant='body2' className={classes.viewsText}>
                  <b>{store.count} artigos</b>
               </Typography>
            </div>
            <div style={{ marginLeft: 3, marginRight: 3 }}>
               <Typography variant='body2' className={classes.viewsText}>
                  <b>|</b>
               </Typography>
            </div>
            {store.pages.map(page =>
               <div style={{ marginLeft: 2, marginRight: 2 }} key={page} onClick={() => handlChangePage(page)}>
                  <Typography variant='body2'
                     className={store.page == page ? classes.selectedPage : classes.viewsText}
                  >
                     <b>{page}</b>
                  </Typography>
               </div>
            )}
         </div>
      </Grid>

   )
}

export default Pagination;

const useStyles = makeStyles(theme => ({
   views: {
      textAlign: 'right',
      [theme.breakpoints.down('xs')]: {
         marginTop: 20,
         textAlign: 'left'
      }
   },
   viewsText: {
      textTransform: 'uppercase',
      color: colors.darkerPrimary,
      cursor: 'pointer'
   },
   selectedPage: {
      textDecoration: `underline ${colors.darkerPrimary}`,
      textTransform: 'uppercase',
      color: colors.darkerPrimary,
      cursor: 'pointer'
   },
   select: {
      marginLeft: 10,
      marginRight: 5
   }
}))