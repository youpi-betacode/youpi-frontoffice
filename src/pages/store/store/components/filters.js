import React, { useEffect } from 'react';
import { Grid, Typography, MenuItem, Select } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import Pagination from './Pagination'

import simulationOptions from '../../../../redux/reducers/simulator-options';
import { colors } from '../../../../constants';
import { setSubfilter, setStorePage, setDesc, setOrderBy } from '../../../../redux/actions/StoreActions';

function Filters() {
   const classes = useStyles();
   const { t } = useTranslation();
   const dispatch = useDispatch();
   const subFilter = useSelector(state => state.store.subFilter);
   const store = useSelector(state => state.store);

   //const filters = [t('gama-filter'), t('price-filter'), t('best-sellers-filter')]
   const filters = [t('gama-filter'), t('price-filter')];
   const filtersFragrances = [t('price-filter')]
   const priceOptions = [t('price-low-to-high'), t('price-high-to-low')];

   const [selectedFilter, setSelectedFilter] = React.useState(filters[0]);
   const [selectedPrice, setSelectedPrice] = React.useState(priceOptions[0])

   const handleOnFilterChange = event => {
      const value = event.target.value;

      if (value === t('gama-filter')) {
         dispatch(setOrderBy('category'))
         dispatch(setStorePage(1))
      }

      else if (value === t('price-filter')) {
         dispatch(setOrderBy('price_base'))
         dispatch(setStorePage(1))
      }


      setSelectedFilter(event.target.value);
   }

   const handleSubFilterClick = event => {
      dispatch(setSubfilter(event.target.value));
      dispatch(setOrderBy('category'));
      dispatch(setStorePage(1))
   }


   const handlePriceClick = (event) => {
      const desc = event.target.value

      dispatch(setOrderBy('price_base'));
      setSelectedPrice(desc)

      if (desc === t('price-high-to-low'))
         dispatch(setDesc(true));
      else
         dispatch(setDesc(false));
   }


   useEffect(() => {
      if(store.category === 'YOUPi!'){
         setSelectedFilter(filters[0]);
      }else if(store.category === 'VELAS' || store.category === 'AMBIENTADORES'){
         setSelectedFilter(filtersFragrances[0]);
      }

   }, [store.category])



   return (
      <MuiThemeProvider theme={theme}>
         <Grid container direction='row' justify='space-around' alignItems='center'>
            <Grid item xs={12} sm={6} className={classes.orderBy}>
               <div style={{ display: 'flex', alignItems: 'center' }}>
                  <div>
                     <Typography variant='body2' className={classes.orderByText}>
                        <u><b>{t('order-by')}</b></u>
                     </Typography>
                  </div>
                  <div>

                     <Select
                        className={classes.select}
                        value={selectedFilter}
                        onChange={handleOnFilterChange}
                     >
                        
                        {store.category === 'YOUPi!' && 
                        filters.map((filter, index) =>
                           <MenuItem value={filters[index]} key={index}>
                              <Typography variant='body2'>
                                 <b>{filter}</b>
                              </Typography>
                           </MenuItem>
                        )}
                        {(store.category === 'VELAS' || store.category === 'AMBIENTADORES') && 
                        filtersFragrances.map((filter, index) =>
                           <MenuItem value={filtersFragrances[index]} key={index}>
                              <Typography variant='body2'>
                                 <b>{filter}</b>
                              </Typography>
                           </MenuItem>
                        )}

                     </Select>

                  </div>

                  {selectedFilter === filters[0] &&
                     <div>
                        <Select
                           className={classes.filtersOptions}
                           value={subFilter}
                           onChange={handleSubFilterClick}
                        >
                           {simulationOptions.gamas.map((gama, index) =>
                              <MenuItem value={gama} key={index}>
                                 <Typography variant='body2'>
                                    <b>{t(gama)}</b>
                                 </Typography>
                              </MenuItem>
                           )}
                        </Select>
                     </div>
                  }
                  {selectedFilter === filters[1] &&
                     <div>
                        <Select
                           className={classes.filtersOptions}
                           value={selectedPrice}
                           onChange={handlePriceClick}
                        >
                           {priceOptions.map((item, index) =>
                              <MenuItem value={item} key={index}>
                                 <Typography variant='body2'>
                                    <b>{item}</b>
                                 </Typography>
                              </MenuItem>
                           )}
                        </Select>
                     </div>
                  }
               </div>
            </Grid>
            <Pagination></Pagination>
         </Grid>
      </MuiThemeProvider >
   )
}

export default Filters;

const useStyles = makeStyles(theme => ({
   mainDiv: {
      display: 'flex',
      justifyContent: 'space-around',
      alignItems: 'center',
      flexDirection: 'row'
   },
   orderBy: {
      textAlign: 'left'
   },
   views: {
      textAlign: 'right'
   },
   orderByText: {
      textTransform: 'uppercase',
      color: colors.darkerPrimary,
      cursor: 'default'
   },
   viewsText: {
      textTransform: 'uppercase',
      color: colors.darkerPrimary,
      cursor: 'pointer'
   },
   selectedPage: {
      textDecoration: `underline ${colors.darkerPrimary}`,
      textTransform: 'uppercase',
      color: colors.darkerPrimary,
      cursor: 'pointer'
   },
   filtersOptions: {
      marginLeft: 5,
      borderLeft: '3px solid',
      borderLeftColor: colors.blue,
      paddingLeft: 10,
      color: colors.darkerPrimary
   },
   select: {
      marginLeft: 10,
      marginRight: 5
   },
   subFilter: {
      cursor: 'pointer',
      marginBottom: 5,
      marginTop: 5,
      textTransform: 'capitalize'
   },
   selectText: {
      color: colors.darkerPrimary,
      borderBottom: 'none'
   }
}))

const theme = createMuiTheme({
   overrides: {
      MuiInput: {
         root: {
            color: colors.darkerPrimary,
         },
         underline: {
            '&:before': {
               borderBottom: 'none'
            },
            '&:hover:not($disabled):before': {
               borderBottom: 'none'
            },
            '&:after': {
               borderBottom: 'none'
            }
         }
      }
   }
})
