import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/styles';
import { useSelector } from 'react-redux';

function CarouselContent({ type, title, image, category }) {
   const classes = useStyles();
   const theme = useTheme();
   const store = useSelector(state => state.store);

   return (
      <Grid container className={classes.card}>
         <Grid item xs={12} className={classes.content} style={{ backgroundImage: `url(${image})` }}>
            <div className={classes.div} style={type === store.displaying ? { backgroundColor: 'rgba(0, 0, 0, 0)' } : {}}>
               <Typography component={"h3"} className={classes.overlay} style={type === store.displaying ? { color: 'rgba(0, 0, 0, 0)' } : {}}>{title}</Typography>
            </div>
         </Grid>
      </Grid>
   )
}

export default CarouselContent;

const useStyles = makeStyles(theme => ({
   card: {
      padding: '1px'
   },
   content: {
      height: '250px',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      cursor: 'pointer',
      [theme.breakpoints.down('sm')]: {
         height: '75px'
      }
   },
   overlay: {
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex',
      fontSize: '26px',
      color: 'white',
      [theme.breakpoints.down('sm')]: {
         paddingTop: '25px'
      },
      transition: 'all 0.5s ease',
      '&:hover': {
         color: 'rgba(0, 0, 0, 0)'
      }
   },
   div: {
      height: '100%',
      backgroundColor: 'rgba(0, 0, 0, 0.4)',
      textAlign: 'center',
      transition: 'all 0.5s ease',
      '&:hover': {
         backgroundColor: 'rgba(0, 0, 0, 0)'
      }
   }
}))