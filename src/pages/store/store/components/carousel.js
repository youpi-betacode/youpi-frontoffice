import React from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import CarouselContent from './carouselContent';
import { useDispatch } from 'react-redux'
import { setStoreMainFilter } from '../../../../redux/actions/StoreActions';
import {useTheme} from '@material-ui/core/styles';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";

function Carousel() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Grid container className={classes.container} spacing={isSmallDevice ? 1: 0}>
      <Grid item xs={12} md={4} onClick={() => dispatch(setStoreMainFilter("YOUPi!", "YOUPi!", "YOUPi!"))}>
        <CarouselContent type="YOUPi!" title="YOUPi!" image="/images/store/caroussel/youpi.jpg"/>
      </Grid>
      <Grid item xs={12} md={4} onClick={() => dispatch(setStoreMainFilter("VELAS", "VELAS", ""))}>
        <CarouselContent type="VELAS" title="VELAS" image="/images/store/caroussel/velas.jpg"/>
      </Grid>
      <Grid item xs={12} md={4} onClick={() => dispatch(setStoreMainFilter("AMBIENTADORES", "AMBIENTADORES", ""))}>
        <CarouselContent type="AMBIENTADORES" title="AMBIENTADORES" image="/images/store/caroussel/ambientadores.jpg"/>
      </Grid>
    </Grid>
  )
}

export default Carousel;

const useStyles = makeStyles(theme => ({
}));