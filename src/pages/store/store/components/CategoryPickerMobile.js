import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import CarouselContent from './carouselContent';
import { useDispatch } from 'react-redux'
import { setStoreMainFilter } from '../../../../redux/actions/StoreActions';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import ItemCarousel from 'react-items-carousel';
import Slider from "react-slick";
import CategoryCard from './CategoryCard';
import clsx from 'clsx';
import { colors } from '../../../../constants';

function CategoryPickerMobile() {
   const classes = useStyles();
   const dispatch = useDispatch();
   const theme = useTheme();

   function SampleNextArrow(props) {
      const { className, style, onClick, iconClass } = props;
      return (
         <div
            className={clsx(className, iconClass)}
            style={{ ...style, display: "block" }}
            onClick={onClick}
         />
      );
   }

   function SamplePrevArrow(props) {
      const { className, style, onClick, iconClass } = props;
      console.log(className)
      return (
         //   <div
         //     className={className}
         //     style={{ ...style, display: "block", background: 'black', color: 'red'}}

         //   />
         <div
            className={clsx(className, iconClass)}
            style={{ ...style }}
            onClick={onClick} />
      );
   }

   var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <SampleNextArrow iconClass={classes.iconClass} />,
      prevArrow: <SamplePrevArrow iconClass={classes.iconClass} />
   };

   return (

      <Slider
         {...settings}
      >
         <CategoryCard
            title="YOUPi!"
            image="/images/store/caroussel/youpi.jpg"
            click={() => dispatch(setStoreMainFilter("YOUPi!", "YOUPi!", "YOUPi!"))}
         />
         <CategoryCard
            title="VELAS"
            image="/images/store/caroussel/velas.jpg"
            click={() => dispatch(setStoreMainFilter("VELAS", "VELAS", ""))}
         />
         <CategoryCard
            title="AMBIENTADORES"
            image="/images/store/caroussel/ambientadores.jpg"
            click={() => dispatch(setStoreMainFilter("AMBIENTADORES", "AMBIENTADORES", ""))}
         />
      </Slider>

   );
}

export default CategoryPickerMobile;

const useStyles = makeStyles(theme => ({
   iconClass: {
      '&::before': {
         color: colors.primary
      }
   }
}));