import React from 'react';
import { Card, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import ProductContent from './productContent';


function ProductList() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const products = useSelector(state => state.store.products);

  if (products.length > 0)
    return (
      <Grid container>
        {products.map((product, index) =>
          <Grid item xs={12} md={3}>
            <Card key={index} className={classes.card}>
              <ProductContent product={product}/>
            </Card>
          </Grid>
        )}
      </Grid>
    )

  return (
    <Grid container>
      <Grid item xs={12} className={classes.noProductsDiv}>
        <Typography variant='h4'>
          {t('no-products-for-that-filter')}
        </Typography>
      </Grid>
    </Grid>
  )
}

export default ProductList;

const useStyles = makeStyles(theme => ({
  card: {
    boxShadow: 'none'
  },
  noProductsDiv: {
    textAlign: 'center',
    margin: '40px auto'
  }
}))