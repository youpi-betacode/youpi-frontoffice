import React from 'react'
import { makeStyles } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import { CardContent, Grid, Typography, Icon } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import { push } from 'connected-react-router'
import { useDispatch, useSelector } from 'react-redux';

import { colors } from '../../../../constants';
import { isAbsolute } from 'path';
import { formatNumber } from '../../../../common/utils';

function ProductContent({ product }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const category = useSelector(state => state.store.category);

  const handleProductClick = reference => {
    dispatch(push(`/store/product/${reference}`))
  }

  let price = product.variants.length !== 0 ? product.variants[0].price_base : product.price_base;
  let promo_price = product.variants.length !== 0 ? product.variants[0].price_promo : product.price_promo;
  let percent = product.extra.discount_percentage * 100

  let v = parseFloat("554.20".replace(".", ","))

  return (
    <CardContent className={classes.content} onClick={() => handleProductClick(product.reference)}>
      <Grid container>
        <Grid item xs={12} className={classes.productImage} >
          {/*
                { 
                  product.stock != 0 &&
                  <div className={classes.stock}>
                    <CheckCircleIcon style={{color: 'green', maxWidth: '20px', marginRight: '5px'}}/>
                    <Typography style={{color: 'green', fontSize: '12px', marginTop: '5px'}}>Disponível</Typography>
                  </div>
                }
                { 
                  product.stock == 0 &&
                  <div className={classes.stock}>
                    <CancelIcon style={{color: 'red', maxWidth: '20px', marginRight: '5px'}}/>
                    <Typography style={{color: 'red', fontSize: '12px', marginTop: '5px'}}>Indisponível</Typography>
                  </div>
                }
              */}
          {category === "YOUPi!" && <img src={`/images/store/youpiis/${product.reference}/0.jpg`} alt='Product' height='200' className={classes.image} />}
          {(category === "VELAS" || category === "AMBIENTADORES") && <img src={`/images/store/fragrances/${product.reference}/0.jpg`} alt='Product' height='200' className={classes.image} />}
        </Grid>
      </Grid>
      <Grid item xs={12} className={classes.productDetails}>            
        <Typography variant='body2'>
          {`${product.name}`}
        </Typography>
        <Typography variant='body2' className={classes.price} style={promo_price !== price ? { textDecorationLine: 'line-through', textDecorationStyle: 'solid' } : {}}>
          {`${t('price-since')} ${formatNumber(price)}€`}
        </Typography>
        {
          promo_price !== price &&
          <Typography variant='body2' className={classes.promotion}>
            {`${t('promotion')} (${percent}%): Desde ${formatNumber(promo_price)}€`}
          </Typography>
        }

      </Grid>
    </CardContent>
  )
}

export default ProductContent;

const useStyles = makeStyles(theme => ({
  content: {
    textAlign: 'center',
    paddingLeft: 5,
    paddingRight: 5,
    cursor: 'pointer',
    '&:hover': {
      textDecorationLine: 'underline',
      textDecorationColor: colors.primary
    }
  },
  stock: {
    textAlign: 'center',
    display: 'flex',
    margin: '20px',
    position: 'absolute',
  },
  productImage: {
    // backgroundColor: colors.backgroundGrey,
    border: "1px solid " + colors.backgroundGrey,
  },
  price: {
    textTransform: 'capitalize'
  },
  promotion: {
    color: colors.blue,
    fontWeight: '700'
  },
  productDetails: {
    marginTop: 10,
    color: colors.darkerPrimary
  },
  image: {
    margin: '90px auto'
  }
}))
