import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/styles';
import { useSelector } from 'react-redux';

function CategoryCard({ title, image, click }) {
   const classes = useStyles();
   const theme = useTheme();

   return (
      <Grid container justify='center' alignItems='center' onClick={click}>
         <div className={classes.card} style={{ backgroundImage: `url(${image})` }}>
            <Typography component={"h3"}>{title}</Typography>
         </div>
         {/* <img src={image} className={classes.image}></img>
        */}
      </Grid>
   )
}

export default CategoryCard;

const useStyles = makeStyles(theme => ({
   card: {
      backgroundSize: 200,
      height: 200,
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      width: '100%',
      textAlign: 'center'
   },
   image:{
      height: 200
   }
}))