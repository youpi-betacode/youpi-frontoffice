import React, { useEffect } from 'react';
import { Grid, Container, Typography, Hidden } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/styles';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';

import Filters from './components/filters';
import Carousel from './components/carousel';
import ProductList from './components/productList';
import Footer from '../../../common/footer/Footer';
import Loader from '../../../common/loading/Loading';
import { colors } from '../../../constants';
import { getProducts } from '../../../redux/actions/StoreActions';
import Pagination from './components/Pagination';
import CategoryPickerMobile from './components/CategoryPickerMobile';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";

function Store() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const store = useSelector(state => state.store);

  let dataBySubCategory = {
    'category': store.category,
    'sub_category': store.subFilter,
    'page': store.page
  }

  let dataByPrice = {
    'category': store.category,
    'order_by': store.orderBy,
    'desc': store.desc,
    'page': store.page
  }

  useEffect(() => {
    store.orderBy === 'category' ? dispatch(getProducts(dataBySubCategory)) : dispatch(getProducts(dataByPrice))
  }, [store.subFilter, store.page, store.desc, store.orderBy, store.category])

  return (
    <Loader loading='storeProducts'>
      <React.Fragment>
        <Container className={classes.container}>
          <Grid container direction='row' justify='space' alignItems='center' spacing={3}>
            <Grid item xs={12} className={classes.grids}>
              <Typography variant='h4' className={classes.pageTitle}>
                <strong>{t('store')}</strong>
              </Typography>
            </Grid>

            <Grid item xs={12} className={classes.grids}>
               <Hidden smDown>
                  <Carousel/>
               </Hidden>
               <Hidden mdUp>
                  <div style={{marginLeft: 20, marginRight: 20}}>
                  <CategoryPickerMobile/>
                  </div>
               
               </Hidden>
                
             
            </Grid>
            <Grid item xs={12}>
              <Filters />
            </Grid>
            <Grid item xs={12}>
              <ProductList />
              <Hidden mdUp>
               <Pagination />
              </Hidden>
            
            </Grid>
          </Grid>
        </Container>
        <Footer />
      </React.Fragment>
    </Loader>
  )
}

export default Store;

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 50,
    marginBottom: 50
  },
  pageTitle: {
    color: colors.darkerPrimary
  },
  grids: {
    textAlign: 'center',
    textTransform: 'uppercase'
  }
}))
