import React from 'react';
import { useDispatch } from 'react-redux'
import { Grid, Typography, RadioGroup, FormControlLabel, Radio } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import clsx from 'clsx'
import { useTranslation } from 'react-i18next';

import { colors } from '../../../../../constants';
import { setPaymentType } from '../../../../../redux/actions/CheckoutPaymentsActions';


function StyledRadio(props) {
  const classes = useStyles();

  return (
    <Radio
      disableRipple
      color="default"
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      {...props}
    />
  );
}

function ProductCheckoutPayment() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const handleChange = event => {
    dispatch(setPaymentType(event.target.value));
  };

  return (
    <Grid container justify='center' alignItems='center' spacing={1}>
      <Grid item xs={12}>
        <Typography variant='h6' className={classes.formHeader}>
          <b>{t('pay-with')}:</b>
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <RadioGroup defaultValue="female" aria-label="gender" name="customized-radios" onChange={handleChange}>
          {
          /*
          <div className={classes.methodGrid}>
            <img src='/images/icons/paypal.png' alt='PayPal' width='25' height='25'/>
            <FormControlLabel value="paypal" control={<StyledRadio />} label="PayPal" labelPlacement="start" className={classes.text}/>
          </div>
          */
          }
           <div className={classes.methodGrid}>
            <img src='/images/icons/multibanco.png' alt='Multibanco' width='25' height='25'/>
            <FormControlLabel value="multibanco" control={<StyledRadio />} label='Multibanco' labelPlacement="start" className={classes.text}/>
          </div>
          <div className={classes.methodGrid}>
            <img src='/images/icons/payshop.png' alt='PayShop' width='25' height='25'/>
            <FormControlLabel value="payshop" control={<StyledRadio />} label='Payshop' labelPlacement="start" className={classes.text}/>
          </div>
          {/* <div className={classes.methodGrid}>
            <img src='/images/icons/bankTransfer.png' alt={t('bank-transfer')} width='25' height='25'/>
            <FormControlLabel value="bankTransfer" control={<StyledRadio />} label={t('bank-transfer')} labelPlacement="start" className={classes.text}/>
          </div>
        */}
          <div className={classes.methodGrid}>
            <img src='/images/icons/visa.png' alt='Visa' width='25' height='25'/>
            <FormControlLabel value="visa" control={<StyledRadio />} label='Visa' labelPlacement="start" className={classes.text}/>
          </div>
          <div className={classes.methodGrid}>
            <img src='/images/icons/masterCard.png' alt='MasterCard' width='25' height='25'/>
            <FormControlLabel value="mastercard" control={<StyledRadio />} label='MasterCard' labelPlacement="start" className={classes.text}/>
          </div>
          <div className={classes.methodGrid}>
            <img src='/images/icons/maestro.png' alt='Maestro' width='25' height='25'/>
            <FormControlLabel value="maestro" control={<StyledRadio />} label='Maestro' labelPlacement="start" className={classes.text}/>
          </div>
          {/*
          <div className={classes.methodGrid}>
            <img src='/images/icons/paypal.png' alt='Gift Card' width='25' height='25'/>
            <FormControlLabel value="giftCard" control={<StyledRadio />} label='YOUPI! gift card' labelPlacement="start" className={classes.text}/>
          </div> */}
          </RadioGroup>
      </Grid>
    </Grid>
  )
}

export default ProductCheckoutPayment;

const useStyles = makeStyles(theme => ({
  formHeader: {
    color: colors.primary,
    textTransform: 'uppercase'
  },
  methodGrid: {
    borderWidth: 2,
    borderColor: colors.darkerPrimary,
    borderStyle: 'solid',
    marginTop: 5,
    marginBottom: 5,
    padding: '5px 10px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  icon: {
    borderRadius: '50%',
    width: 16,
    height: 16,
    boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#f5f8fa', //unchecked
    //backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    // '$root.Mui-focusVisible &': {
    //   outline: '2px auto rgba(19,124,189,.6)',
    //   outlineOffset: 2,
    // },
    // 'input:hover ~ &': {
    //   backgroundColor: '#ebf1f5',
    // },
  },
  checkedIcon: {
    backgroundColor: '#ffffff',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'block',
      width: 16,
      height: 16,
      backgroundImage: 'radial-gradient(#d29f72,#d29f72 35%,transparent 0%)',
      content: '""',
    },
    // 'input:hover ~ &': {
    //   backgroundColor: '#106ba3',
    // },
  },
  text: {
    textTransform: 'capitalize'
  }
}))
