import React, {useEffect, useState} from 'react';
import {Grid, Container, Button} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import {makeStyles} from '@material-ui/styles';
import {useTranslation} from 'react-i18next';
import {push} from 'connected-react-router';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import useTheme from "@material-ui/core/styles/useTheme";

import ProductCheckoutPayment from './components/ProductCheckoutPayment';
import CheckoutResume from '../common/CheckoutResume';
import CheckoutStepper from '../common/Stepper';
import { setCheckoutStep } from '../../../../redux/actions/CheckoutStepsActions';
import { createSale } from '../../../../redux/actions/CheckoutPaymentsActions';
import Loader from '../../../../common/loading/Loading';

function CheckoutPayment() {
    const dispatch = useDispatch();
    const classes = useStyles();
    const {t} = useTranslation();
    const theme = useTheme();
    const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
    const shoppingCart = useSelector(state => state.shoppingCart.cart);
    const paymentType = useSelector(state => state.checkoutPayments.paymentType);
    const addresses = useSelector(state => state.checkoutAddresses);

  const generateAddress = () => {

    const address_data = {
      "shipping_address": addresses.shippingAddress,
      "shipping_zip_code": addresses.shippingZipCode,
      "shipping_city": addresses.shippingCity,
      "billing_address": addresses.billingAddress,
      "billing_zip_code": addresses.billingZipCode,
      "billing_city": addresses.billingCity,
      'billing_nif': addresses.billingNIF,
      'billing_name': addresses.billingName,
      'shipping_name': addresses.shippingName,
      'shipping_phone_number': addresses.shippingPhoneNumber
    };

    return address_data
  };

  const handleNextClick = () => {
      const addresses = generateAddress();
      dispatch(createSale(shoppingCart.uuid, paymentType, addresses));
  };

  const handlePreviousClick = () => {
    dispatch(push('/checkout/addresses'));
  };

  useEffect(() => {
    dispatch(setCheckoutStep(2));
  });

  return (
    <Loader loading="checkoutPayment">
      <React.Fragment>
        <Container className={classes.container}>
          <Grid container direction='row' alignItems='flex-start'>
            <Grid item xs={11} md={8} style={{ margin: 'auto' }}>
              <Grid container direction='column' spacing={3}>
                <Grid item xs={12}>
                  <CheckoutStepper />
                </Grid>
                <Grid item xs={12} style={{ textAlign: 'center' }}>
                  <ProductCheckoutPayment />
                </Grid>
              </Grid>
            </Grid>
            <Grid item md={4}>
              <Grid container direction='column' spacing={1}
                className={isSmallDevice ? classes.rightPanelSmall : classes.rightPanel}>
                <Grid item xs={11} md={12}>
                  <CheckoutResume />
                </Grid>
                <Grid item md={12}>
                  <Grid container direction='column' justify='center' alignItems='center' spacing={3}>
                    <Button color='primary' className={classes.button}
                      onClick={() => dispatch(push('/store'))}>
                      {t('back-to-store')}
                    </Button>
                    <Button color='primary' className={classes.button} onClick={handleNextClick} disabled={paymentType ? false : true}>
                      {t('advance')}
                    </Button>
                    <Button color='primary' style={{marginBottom: '15px'}}className={classes.button} onClick={handlePreviousClick}>
                      {t('back')}
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </React.Fragment>
    </Loader>
  )
}

export default CheckoutPayment;

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 50,
    marginBottom: 50,
  },
  rightPanel: {
    marginLeft: theme.spacing(8),
  },
  rightPanelSmall: {
    marginTop: 20,
    margin: 'auto'
  },
  button: {
    marginTop: 14,
    width: 250
  }
}))
