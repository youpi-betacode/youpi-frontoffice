import React from 'react';
import { Grid, Typography, Link, InputBase } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { colors, PAID_DELIVERY, FREE_DELIVERY } from '../../../../constants';
import { useTranslation } from 'react-i18next';
import { formatNumber } from '../../../../common/utils';

function CheckoutResumeCard({ product, image, size}) {
   const classes = useStyles();
   const { t } = useTranslation();

   const {
      reference,
      price_promo,
      price_base,
      amount,
      name
   } = product

   return (
      <Grid container alignItems='center' justify='center' spacing={3} className={classes.card}>
         <Grid item xs={4} className={classes.imageWrapper}>
            {!(reference === PAID_DELIVERY || reference === FREE_DELIVERY) && <Grid container alignItems='center' justify='center'>
               <img src={image} className={classes.image} />
            </Grid>}
         </Grid>
         <Grid item xs={5}>
            <Grid container direction="column" spacing={1} className={classes.titleSizeContainer}>
               <Grid item className={classes.title}>
                  {name}
               </Grid>
               <Grid item>
                  {size}
               </Grid>
               {!(reference === PAID_DELIVERY || reference === FREE_DELIVERY) && <Grid item className={classes.quantityText}>
                  {t('quantity-checkout')}:  {amount}
               </Grid>}
            </Grid>
         </Grid>
         <Grid item xs={3} className={classes.price}>
            {(reference === PAID_DELIVERY || reference === FREE_DELIVERY) ? formatNumber(price_base) : formatNumber(price_promo)}€
        </Grid>
      </Grid>
   )
}

export default CheckoutResumeCard;

const useStyles = makeStyles(theme => ({
   card: {
      height: 120,
      backgroundColor: colors.backgroundGrey,//'#f4f4f5',
      marginBottom: 30,
      boxShadow: "2px 2px 4px #9E9E9E"
   },
   imageWrapper: {
      //backgroundColor: 'red'
   },
   image: {
      width: '80%'
   },
   titleSizeContainer: {
      //paddingLeft: 20
      //backgroundColor: 'orange'
   },
   title: {
      fontSize: 14,
      // textTransform: 'uppercase'
   },
   remove: {
      marginTop: 10,
   },
   price: {
      fontSize: 14,
      textAlign: 'center'

   },
   quantityText: {
      color: colors.primary,
      fontWeight: 600
   }




}))