import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import useTheme from "@material-ui/core/styles/useTheme";

import { colors } from '../../../../constants';
import CheckoutResumeCardTotal from './CheckoutResumeCardTotal';
import CheckoutResumeCard from './CheckoutResumeCard';
import { defaultProductPhoto } from "../../../../constants";

function CheckoutResume() {
  const classes = useStyles();
  const cart = useSelector(state => state.shoppingCart.cart);
  const { t } = useTranslation();
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Grid container alignItems='center' justify='center'>
      <Grid item xs={12} className={isSmallDevice ? classes.headerSmall : classes.header}>
        <Typography variant='h5'>
          <b>{t('summary')}</b>
        </Typography>
      </Grid>
      {
        cart && cart.products &&
        cart.products.map((product, index) => {
          let image = null;

          if (product.category === "YOUPi!") {
            if(product.reference === 'YB_BL_LX_30_2_UV')
              image = `/images/store/youpiis/YBBLLX_2_UV/0.jpg`
            else{
              const image_reference = product.reference.replace(/_20|_30/, "")
              image = `/images/store/youpiis/${image_reference}/0.jpg`;
            }
          } 
          else if (product.category === "VELAS" || product.category === "AMBIENTADORES") {
            image = `/images/store/fragrances/${product.reference}/0.jpg`;
          }

          if (product.extra && product.extra["simulation_data"]) {
            image = product.extra["simulation_data"]["small_filtered_photo"];
          }

          return (
            <CheckoutResumeCard
              key={index}
              product={product}
              image={image}
            />
          );
        })
      }
      <CheckoutResumeCardTotal />
    </Grid>
  )
}

export default CheckoutResume;

const useStyles = makeStyles(theme => ({
  header: {
    marginBottom: 25,
    color: colors.primary,
    textTransform: 'uppercase'
  },
  headerSmall: {
    marginBottom: 25,
    color: colors.primary,
    textAlign: 'center',
    textTransform: 'uppercase'
  },
}))
