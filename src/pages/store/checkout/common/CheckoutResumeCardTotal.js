import React from 'react';
import { Grid, Typography, Link, InputBase } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { colors } from '../../../../constants';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { formatNumber } from '../../../../common/utils';

function CheckoutResumeCardTotal() {
  const classes = useStyles();
  const { t } = useTranslation();
  const cart = useSelector(state => state.shoppingCart.cart);
  const subtotal = cart ? cart.total_price : 0;
  // const youpiCoins = 10;

  return (
    <Grid container alignItems='center' justify='center' spacing={3} className={classes.card}>
      <Grid item xs={4}>
      </Grid>
      <Grid item xs={5}>
        <Grid container direction="column" spacing={1}>
          <Grid item className={classes.font}>
            Total
            </Grid>
        </Grid>
      </Grid>
      <Grid item xs={3}>
        <Grid container direction="column" spacing={1}>
          <Grid item xs={12} className={classes.font} style={{textAlign: 'center'}}>
            {formatNumber(subtotal)}€
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default CheckoutResumeCardTotal;

const useStyles = makeStyles(theme => ({
  card: {
    height: 120,
    backgroundColor: colors.backgroundGrey,//'#f4f4f5',
    marginBottom: 30,
    boxShadow: "2px 2px 4px #9E9E9E"
  },
  values: {
    textAlign: 'right',
    marginRight: theme.spacing(2)
  },
  total: {
    marginTop: theme.spacing(1),
    color: colors.primary,
    fontWeight: 600
  },
  font: {
    fontSize: 14
  }
}))
