import React from 'react';
import { Grid, Step, Stepper, StepLabel, StepConnector } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { colors } from '../../../../constants';
import { useSelector } from 'react-redux';

function getSteps() {
  return ['Carrinho', 'Endereço de Entrega', 'Pagamento'];
}
const ColorlibConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    '& $line': {
      backgroundImage:
        'linear-gradient(to right,  #745334 0%,#fabc87 51%,#745334 93%,#745334 100%)',
    },
  },
  completed: {
    '& $line': {
      backgroundImage:
        'linear-gradient(to right,  #745334 0%,#fabc87 51%,#745334 93%,#745334 100%)',
    },
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: '#eaeaf0',
    borderRadius: 1,
  },
})(StepConnector);

const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: 'white',
    borderColor: colors.primary,
    border: '2px solid',
    zIndex: 1,
    color: colors.blue,
    width: 50,
    height: 50,
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    fontSize: 32,
    alignItems: 'flex-end',
  },
  active: {
    backgroundImage:
      'linear-gradient(to right, #745334 0%,#fabc87 51%,#745334 93%,#745334 100%)',
    boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
    border: '0px',
    color: 'white'
  },
  completed: {
    backgroundImage:
      'linear-gradient(to right,  #745334 0%,#fabc87 51%,#745334 93%,#745334 100%)',
    border: '0px',
    color: 'white'
  },
});

function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles();
  const { active, completed } = props;

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {props.icon}
    </div>
  );
}

ColorlibStepIcon.propTypes = {
  active: PropTypes.bool,
  completed: PropTypes.bool,
  icon: PropTypes.number,
};



function CheckoutStepper() {
  const classes = useStyles();
  const steps = getSteps();
  const step = useSelector(state => state.checkoutSteps.step)

  return (
    <Grid container alignItems='center' justify='center' className={classes.stepper}>
      <img src='/images/youpiCheckout.png' className={classes.checkoutImage} alt=''/>
      <Stepper alternativeLabel activeStep={step} connector={<ColorlibConnector />}>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
    </Grid>
  )
}

export default CheckoutStepper;


const useStyles = makeStyles(theme => ({
  root: {
    width: '90%',
  },
  stepper: {
    marginBottom: 50
  },
  checkoutImage:{
    marginBottom: theme.spacing(2),
    maxWidth: '350px',
    [theme.breakpoints.down('sm')]: {
      width: 280
    }
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },

}));