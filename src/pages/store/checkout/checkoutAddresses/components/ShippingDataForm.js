import React, { useEffect } from 'react';
import { Grid, Typography, TextField, FormControlLabel, Checkbox } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import useTheme from "@material-ui/core/styles/useTheme";

import { colors } from '../../../../../constants';
import { setShippingName, setShippingPhoneNumber, getUserCheckoutInfo } from '../../../../../redux/actions/CheckoutAddressesActions';

function ShippingDataForm() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { t } = useTranslation();
  const theme = useTheme();
  const addresses = useSelector(state => state.checkoutAddresses);
  const valid = useSelector(state => state.checkoutAddresses.validForm)

  const [shippingBox, setShippingBox] = React.useState(true)

  useEffect(() => {
    if(!shippingBox) {
      dispatch(setShippingName(''));
      dispatch(setShippingPhoneNumber(''));
    }
    else
      dispatch(getUserCheckoutInfo('shipping'))
  },[shippingBox])

  return (
    <Grid container alignItems='center' justify='space-between' className={classes.formContainer}>
      <Grid item xs={12} md={6}>
        <Typography variant='h6' className={classes.formHeader}>
          <b>{t('delivery-data')}</b>
        </Typography>
      </Grid>

      <Grid item xs={12} md={6} className={classes.checkboxGrid}>
        <FormControlLabel
          control={
            <Checkbox
              checked={shippingBox}
              onChange={event => setShippingBox(event.target.checked)} color="primary" />
          }
          labelPlacement="start"
          label={t('use-profile-info')}
        />
      </Grid>

      <Grid item xs={12}>
        <div>
          <TextField
            required
            fullWidth
            margin="normal"
            variant="outlined"
            placeholder={t('delivery-name-placeholder')}
            value={addresses.shippingName}
            onChange={event => dispatch(setShippingName(event.target.value))}
            InputLabelProps={{
              shrink: false
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || addresses.shippingName) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
        </div>
        {!(valid || addresses.shippingName) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('required-field')}
          </Typography>
        </div>}
      </Grid>
      <Grid item xs={12} >
        <div>
        <TextField
          required
          fullWidth
          margin="normal"
          variant="outlined"
          placeholder={t('phone-number-placeholder')}
          value={addresses.shippingPhoneNumber}
          onChange={event => dispatch(setShippingPhoneNumber(event.target.value))}
          InputLabelProps={{
            shrink: false
          }}
          InputProps={{
            classes: {
              root: classes.text,
              notchedOutline: (valid || addresses.shippingPhoneNumber) ? classes.notchedOutline : classes.errorNotchedOutline
            }
          }}
        />
        </div>
        {!(valid || addresses.shippingPhoneNumber) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('required-field')}
          </Typography>
        </div>}
      </Grid>
    </Grid>
  )
}

export default ShippingDataForm;

const useStyles = makeStyles(theme => ({
  formContainer: {
    margin: '20px auto'
  },
  formHeader: {
    color: colors.primary,
    textTransform: 'uppercase',
    [theme.breakpoints.down('sm')]: {
      textAlign: 'center'
    }
  },
  text: {
    color: `${colors.htmlGrey} !important`
  },
  notchedOutline: {
    borderWidth: '1px',
    borderColor: colors.darkerPrimary + ' !important',
    borderRadius: 7,
  },
  checkboxGrid: {
    textAlign: 'right',
    [theme.breakpoints.down('sm')]: {
      marginTop: 10,
      marginBottom: 10
    },
    [theme.breakpoints.down('xs')]: {
      textAlign: 'left'
    }
  },
  errorGrid: {
    textAlign: 'left',
    color: `${colors.errorPink} !important`,
    marginBottom: 10
  },
  errorNotchedOutline: {
    borderWidth: '1px',
    borderColor: colors.errorPink + ' !important',
    borderRadius: 7,
  }
}))
