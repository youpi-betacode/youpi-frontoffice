import React from 'react';
import { Grid } from '@material-ui/core';
import BillingAddressForm from './BillingAddressForm';
import ShippingAddressForm from './ShippingAddressForm';
import BillingDataForm from './BillingDataForm';
import ShippingDataForm from './ShippingDataForm';


function ProductCheckoutForm() {

   return (
      <Grid container>
         <Grid item xs={12}>
            <BillingDataForm/>
         </Grid>
         <Grid item xs={12}>
            <BillingAddressForm/>
         </Grid>
         <Grid item xs={12}>
            <ShippingDataForm/>
         </Grid>
         <Grid item xs={12}>
            <ShippingAddressForm/>
         </Grid>
      </Grid>
   )
}

export default ProductCheckoutForm;

