import React, { useEffect } from 'react';
import { Grid, Typography, TextField, Checkbox, FormControlLabel } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import useTheme from "@material-ui/core/styles/useTheme";

import { colors, zipCodeRegex } from '../../../../../constants';
import { setShippingAddress, setShippingZipCode,
  setShippingCity, getUserAddresses } from '../../../../../redux/actions/CheckoutAddressesActions';

function ShippingAddressForm() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { t } = useTranslation();
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
  const addresses = useSelector(state => state.checkoutAddresses);
  const valid = useSelector(state => state.checkoutAddresses.validForm);

  const [shippingBox, setShippingBox] = React.useState(true);

  useEffect(() => {
    if (!shippingBox) {
      dispatch(setShippingAddress(''));
      dispatch(setShippingZipCode(''));
      dispatch(setShippingCity(''));
    }
    else
      dispatch(getUserAddresses('shipping'))
  }, [shippingBox])

  return (
    <Grid container alignItems='center' justify='space-between' className={classes.formContainer}>
      <Grid item xs={12} md={6}>
        <Typography variant='h6' className={classes.formHeader}>
          <b>{t('deliver-address')}</b>
        </Typography>
      </Grid>
      <Grid item xs={12} md={6} className={classes.checkboxGrid}>
        <FormControlLabel
          control={
            <Checkbox
              checked={shippingBox}
              onChange={event => setShippingBox(event.target.checked)} color="primary" />
          }
          labelPlacement="start"
          label={t('use-default-address')}
        />
      </Grid>

      <Grid item xs={12}>
        <div>
          <TextField
            required
            fullWidth
            margin="normal"
            variant="outlined"
            placeholder={t('street-placeholder')}
            value={addresses.shippingAddress}
            onChange={event => dispatch(setShippingAddress(event.target.value))}
            InputLabelProps={{
              shrink: false
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || addresses.shippingAddress) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
        </div>
        {!(valid || addresses.shippingAddress) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('required-field')}
          </Typography>
        </div>}
      </Grid>
      <Grid item xs={12} md={5}>
        <div>
          <TextField
            required
            fullWidth
            margin="normal"
            variant="outlined"
            placeholder={t('zip-code-placeholder')}
            value={addresses.shippingZipCode}
            onChange={event => dispatch(setShippingZipCode(event.target.value))}
            InputLabelProps={{
              shrink: false
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || zipCodeRegex.test(addresses.shippingZipCode)) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
        </div>
        {!(valid || zipCodeRegex.test(addresses.shippingZipCode)) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('invalid-zip-code')}
          </Typography>
        </div>}
      </Grid>
      <Grid item xs={12} md={5}>
        <div>
          <TextField
            required
            fullWidth
            margin="normal"
            variant="outlined"
            placeholder={t('city-placeholder')}
            value={addresses.shippingCity}
            onChange={event => dispatch(setShippingCity(event.target.value))}
            InputLabelProps={{
              shrink: false
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || addresses.shippingCity) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
        </div>
        {!(valid || addresses.shippingCity) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('required-field')}
          </Typography>
        </div>}
      </Grid>
    </Grid>
  )
}

export default ShippingAddressForm;

const useStyles = makeStyles(theme => ({
  formContainer: {
    margin: '20px auto'
  },
  formHeader: {
    color: colors.primary,
    textTransform: 'uppercase',
    [theme.breakpoints.down('sm')]: {
      textAlign: 'center'
    }
  },
  text: {
    color: `${colors.htmlGrey} !important`
  },
  notchedOutline: {
    borderWidth: '1px',
    borderColor: colors.darkerPrimary + ' !important',
    borderRadius: 7,
  },
  checkboxGrid: {
    textAlign: 'right'
  },
  checkboxGrid: {
    textAlign: 'right',
    [theme.breakpoints.down('sm')]: {
      marginTop: 10,
      marginBottom: 10
    },
    [theme.breakpoints.down('xs')]: {
      textAlign: 'left'
    }
  },
  errorGrid: {
    textAlign: 'left',
    color: `${colors.errorPink} !important`,
    marginBottom: 10
  },
  errorNotchedOutline: {
    borderWidth: '1px',
    borderColor: colors.errorPink + ' !important',
    borderRadius: 7,
  }
}))
