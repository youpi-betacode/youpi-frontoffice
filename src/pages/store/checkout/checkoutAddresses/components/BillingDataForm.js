import React, { useEffect } from 'react';
import { Grid, Typography, TextField, FormControlLabel, Checkbox } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import useTheme from "@material-ui/core/styles/useTheme";

import { colors, nifRegex } from '../../../../../constants';
import { setBillingName, setBillingNIF, getUserCheckoutInfo } from '../../../../../redux/actions/CheckoutAddressesActions';


function BillingDataForm() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { t } = useTranslation();
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
  const addresses = useSelector(state => state.checkoutAddresses);
  const valid = useSelector(state => state.checkoutAddresses.validForm);

  const [billingBox, setBillingBox] = React.useState(true)

  useEffect(() => {
    if(!billingBox) {
      dispatch(setBillingName(''));
      dispatch(setBillingNIF(''));
    }
    else
      dispatch(getUserCheckoutInfo('billing'))
  },[billingBox])

  return (
    <Grid container alignItems='center' justify='space-between' className={classes.formContainer}>
      <Grid item xs={12} md={6}>
        <Typography variant='h6' className={classes.formHeader}>
          <b>{t('billing-data')}</b>
        </Typography>
      </Grid>

      <Grid item xs={12} md={6} className={classes.checkboxGrid}>
        <FormControlLabel
          control={
            <Checkbox
              checked={billingBox}
              onChange={event => setBillingBox(event.target.checked)} color="primary" />
          }
          labelPlacement="start"
          label={t('use-profile-info')}
        />
      </Grid>

      <Grid item xs={12}>
        <div>
          <TextField
            required
            fullWidth
            margin="normal"
            variant="outlined"
            name='BillingAddress'
            placeholder={t('billing-name-placeholder')}
            value={addresses.billingName}
            onChange={event => dispatch(setBillingName(event.target.value))}
            InputLabelProps={{
              shrink: false
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || addresses.billingName) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
        </div>
        {!(valid || addresses.billingName) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('required-field')}
          </Typography>
        </div>}
      </Grid>


      <Grid item xs={12}>
        <div>
          <TextField
            required
            fullWidth
            margin="normal"
            variant="outlined"
            placeholder={t('fiscal-id-placeholder')}
            value={addresses.billingNIF}
            onChange={event => dispatch(setBillingNIF(event.target.value))}
            InputLabelProps={{
              shrink: false
            }}
            InputProps={{
              classes: {
                root: classes.text,
                notchedOutline: (valid || nifRegex.test(addresses.billingNIF) || !addresses.billingNIF) ? classes.notchedOutline : classes.errorNotchedOutline
              }
            }}
          />
        </div>
        <div style={{textAlign: 'left', marginBottom: 10}}>
            <Typography variant='body2'>
              {t('costumer-does-not-want-nif')}
            </Typography>
        </div>
        {!(valid || nifRegex.test(addresses.billingNIF)) && <div className={classes.errorGrid}>
          <Typography variant='body2'>
            {t('invalid-nif')}
          </Typography>
        </div>}
      </Grid>
    </Grid>
  )
}

export default BillingDataForm;

const useStyles = makeStyles(theme => ({
  formContainer: {
    margin: '20px auto'
  },
  formHeader: {
    color: colors.primary,
    textTransform: 'uppercase',
    [theme.breakpoints.down('sm')]: {
      textAlign: 'center'
    }
  },
  text: {
    color: `${colors.htmlGrey} !important`
  },
  notchedOutline: {
    borderWidth: '1px',
    borderColor: colors.darkerPrimary + ' !important',
    borderRadius: 7,
  },
  checkboxGrid: {
    textAlign: 'right',
    [theme.breakpoints.down('sm')]: {
      marginTop: 10,
      marginBottom: 10
    },
    [theme.breakpoints.down('xs')]: {
      textAlign: 'left'
    }
  },
  errorGrid: {
    textAlign: 'left',
    color: `${colors.errorPink} !important`,
    marginBottom: 10
  },
  errorNotchedOutline: {
    borderWidth: '1px',
    borderColor: colors.errorPink + ' !important',
    borderRadius: 7,
  }
}))
