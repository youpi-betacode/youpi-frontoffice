import React, { useEffect } from 'react';
import { Grid, Container, Button } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import { push } from 'connected-react-router';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import useTheme from "@material-ui/core/styles/useTheme";
import { useSnackbar } from 'notistack';

import CheckoutStepper from '../common/Stepper';
import CheckoutResume from '../common/CheckoutResume';
import { getUserAddresses, setValidForm } from '../../../../redux/actions/CheckoutAddressesActions';
import { setCheckoutStep } from '../../../../redux/actions/CheckoutStepsActions';
import ProductCheckoutForm from './components/ProductCheckoutForm';
import {zipCodeRegex, nifRegex } from '../../../../constants';

function Addresses() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const theme = useTheme();
  const { t } = useTranslation();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
  const addresses = useSelector(state => state.checkoutAddresses);
  const { enqueueSnackbar } = useSnackbar();

  const handleNextClick = () => {

    if(addresses.shippingName && addresses.shippingAddress && addresses.shippingCity
      && addresses.shippingZipCode && addresses.shippingPhoneNumber && addresses.billingAddress
      && addresses.billingCity && (nifRegex.test(addresses.billingNIF) || !addresses.billingNIF) && addresses.billingName
      && zipCodeRegex.test(addresses.billingZipCode)){
        dispatch(setValidForm(true))
        dispatch(push('/checkout/payment'));
      }
    else{
      dispatch(setValidForm(false))
      enqueueSnackbar(t('verify-all-fields'), {
        variant: 'error',
        anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
        },
        autoHideDuration: 5000
      });
    }
  }

  const handlePreviousClick = () => {
    dispatch(push('/checkout/summary'));
  }

  useEffect(() => {
    //dispatch(getUserAddresses());
    dispatch(setCheckoutStep(1));
  },[])

  return (
    <React.Fragment>
      <Container className={classes.container}>
        <Grid container direction='row' alignItems='flex-start'>
          <Grid item xs={11} md={8} style={{margin: 'auto'}}>
            <Grid container direction='column' spacing={3}>
              <Grid item xs={12}>
                <CheckoutStepper />
              </Grid>
              <Grid item xs={12}>
                <ProductCheckoutForm />
              </Grid>
            </Grid>
          </Grid>
          <Grid item md={4}>
            <Grid container direction='column' spacing={3} className={isSmallDevice ? classes.rightPanelSmall : classes.rightPanel}>
              <Grid item xs={11} md={12}>
                <CheckoutResume />
              </Grid>
              <Grid item xs={12}>
                <Grid container direction='column' justify='center' alignItems='center' spacing={3}>
                  <Button color='primary' className={classes.button} onClick={() => dispatch(push('/store'))}>
                    {t('back-to-store')}
                  </Button>
                  <Button color='primary' className={classes.button} onClick={handleNextClick}>
                    {t('advance')}
                  </Button>
                  <Button color='primary' className={classes.button} onClick={handlePreviousClick}>
                    {t('back')}
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </React.Fragment >
  )
}

export default Addresses;

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 50,
    marginBottom: 50,
  },
  rightPanel: {
    marginLeft: theme.spacing(8),
  },
  rightPanelSmall: {
  margin: 'auto',
  },
  button: {
    marginTop: 14,
    width: 250
  }
}))
