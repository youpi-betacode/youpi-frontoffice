import React from 'react';
import { useSelector } from 'react-redux';
import { Card, Typography, Grid, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import ProductCardSmall from './productCardSmall';
import { colors } from '../../../../../constants';
import { formatNumber } from '../../../../../common/utils';


function ProductListSmall() {
  const classes = useStyles();
  const sale = useSelector(state => state.checkoutPayments.sale);

  if (sale) {
    const products = sale.cart.products;
    const subtotal = sale.total;

    return (
      <div className={classes.cards}>
        <Typography variant='h5' className={classes.title}>
          Resumo da Compra
        </Typography>
        {products &&
          products.map((product, index) => {
            let image = null;

            if (product.category === "YOUPi!") {
              const image_reference = product.reference.replace(/_20|_30/, "")
              image = `/images/store/youpiis/${image_reference}/0.jpg`;
            } else if (product.category === "VELAS" || product.category === "AMBIENTADORES") {
              image = `/images/store/fragrances/${product.reference}/0.jpg`;
            }

            if (product.extra && product.extra["simulation_data"]) {
              image = product.extra["simulation_data"]["small_filtered_photo"];
            }

            return (
              <Card style={{ marginBottom: 25 }}>
                <ProductCardSmall
                  key={index}
                  product={product}
                  reference={product.reference}
                  image={image}
                  title={product.name}
                  quantity={product.amount}
                  price={product.price_base}
                />
              </Card>
            )
          })
        }
        <Grid container direction='row' justify='center' alignItems='center' spacing={1}>
          <Grid item md={6} style={{ textAlign: 'left' }}>
            <Typography variant='h6' className={classes.total}>
              Total:
            </Typography>
          </Grid>
          <Grid item md={6} className={classes.total}>
            <Typography variant='h6' style={{ textAlign: 'right' }}>
              {formatNumber(subtotal)}€
            </Typography>
          </Grid>
        </Grid>
      </div>
    )
  } else {
    return <Typography>Loading</Typography>
  }
}

export default ProductListSmall;

const useStyles = makeStyles(theme => ({
  cards: {
    maxWidth: '80%',
    margin: 'auto'
  },
  title: {
    marginBottom: '20px',
    color: colors.primary
  },
  total: {
    color: colors.primary
  }
}))
