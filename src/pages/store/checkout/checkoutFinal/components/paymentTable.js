import React from 'react';
import { useSelector } from 'react-redux';
import { Typography, Table, TableBody, TableRow, TableCell } from '@material-ui/core';
import { withStyles, makeStyles } from '@material-ui/styles';

import { colors } from '../../../../../constants';
import { formatNumber } from '../../../../../common/utils';

function PaymentType(type) {
  switch (type) {
    case 'multibanco':
      return 'Multibanco';
    case 'payshop':
      return 'Payshop';
    case 'visa':
      return 'Visa';
    case 'mastercard':
      return 'MasterCard';
    case 'maestro':
      return 'Maestro';
    default:
      return null;
  }
}

function PaymentStatus(status) {
  switch (status) {
    case 'ok':
      return 'Pago';
    default:
      return 'Pendente'
  }
}

function PaymentTable() {
  const classes = useStyles();
  const sale = useSelector(state => state.checkoutPayments.sale);
  // const sale_status = useSelector(state => state.checkoutPayments.status) 

  if (sale) {
    const type = sale.payment_type
    const payment = sale.extra.payment_info

    return (
      <React.Fragment>
        <Typography variant='h5' className={classes.title}>
          Detalhes de Pagamento
        </Typography>
        <Table className={classes.table}>
          <TableBody>
            <TableRow>
              <TableCell>
                <b>Pagamento</b>
              </TableCell>
              <TableCell>
                {
                  PaymentType(type)
                }
              </TableCell>
            </TableRow>
            {
              payment.reference &&
              <TableRow>
                <TableCell>
                  <b>Referência</b>
                </TableCell>
                <TableCell>
                  {payment.reference}
                </TableCell>
              </TableRow>
            }
            {
              (payment.entity || payment.paymentDeadline) &&
              <TableRow>
                <TableCell>
                  {
                    type === 'multibanco' &&
                    <b>Entidade</b>
                  }
                  {
                    type === 'payshop' &&
                    <b>Prazo limite</b>
                  }
                </TableCell>
                <TableCell>
                  {
                    type === 'multibanco' &&
                    payment.entity
                  }
                  {
                    type === 'payshop' &&
                    payment.paymentDeadline
                  }
                </TableCell>
              </TableRow>
            }
            {
              (payment.origAmount || payment.amountOut) &&
              <TableRow>
                <TableCell>
                  <b>Valor</b>
                </TableCell>
                <TableCell>
                  {
                    payment.amountOut &&
                    formatNumber(payment.amountOut) + '€'
                  }
                  {
                    payment.origAmount &&
                    formatNumber(payment.origAmount) + '€'
                  }
                </TableCell>
              </TableRow>
            }
            <TableRow>
              <TableCell>
                <b>Status</b>
              </TableCell>
              <TableCell>
                {
                  PaymentStatus(payment.status)
                }
              </TableCell>
            </TableRow>
            {
              payment.transid &&
              (type === 'visa' || type === 'mastercard' || type === 'maestro') &&
              <TableRow>
                <TableCell>
                  <b>ID Transacção</b>
                </TableCell>
                <TableCell>
                  {payment.transid}
                </TableCell>
              </TableRow>
            }
          </TableBody>
        </Table>
      </React.Fragment>
    )
  } else {
    return <Typography>Loading</Typography>
  }
}

export default PaymentTable;

const StyledTableBody = withStyles(theme => ({
  root: {
    border: `2px solid ${colors.errorPink} !important`
  }
}))(TableBody)

const useStyles = makeStyles(theme => ({
  title: {
    marginBottom: '10px',
    color: colors.primary
  },
  table: {
    maxWidth: '90%',
    margin: 'auto'
  }
}))
