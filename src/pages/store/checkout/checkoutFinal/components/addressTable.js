import React from 'react';
import { useSelector } from 'react-redux';
import { Typography, Grid, Table, TableBody, TableRow, TableCell } from '@material-ui/core';
import { withStyles, makeStyles } from '@material-ui/styles';

import { colors } from '../../../../../constants';

function AddressTable() {
  const classes = useStyles();
  const sale = useSelector(state => state.checkoutPayments.sale)

  if (sale) {
    const addresses = sale.extra.addresses

    return (
      <React.Fragment>
        <Grid container direction='row' justify='center' alignItems='center' spacing={3}>
          <Grid item md={6}>
            <Typography variant='h5' className={classes.title}>
              Dados de Envio
            </Typography>
            <Table className={classes.table}>
              <TableBody>
                <TableRow>
                  <TableCell>
                    <b>Morada</b>
                  </TableCell>
                  <TableCell>
                    {addresses.shipping_address}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>Código Postal</b>
                  </TableCell>
                  <TableCell>
                    {addresses.shipping_zip_code}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>Cidade</b>
                  </TableCell>
                  <TableCell>
                    {addresses.shipping_city}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>Nome</b>
                  </TableCell>
                  <TableCell>
                    {addresses.shipping_name}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>Contacto</b>
                  </TableCell>
                  <TableCell>
                    {addresses.shipping_phone_number}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Grid>
          <Grid item md={6}>
            <Typography variant='h5' className={classes.title}>
              Dados de Faturação
            </Typography>
            <Table className={classes.table}>
              <TableBody>
                <TableRow>
                  <TableCell>
                    <b>Morada</b>
                  </TableCell>
                  <TableCell>
                    {addresses.billing_address}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>Código Postal</b>
                  </TableCell>
                  <TableCell>
                    {addresses.billing_zip_code}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>Cidade</b>
                  </TableCell>
                  <TableCell>
                    {addresses.billing_city}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>Nome</b>
                  </TableCell>
                  <TableCell>
                    {addresses.billing_name}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <b>NIF</b>
                  </TableCell>
                  <TableCell>
                    {addresses.billing_nif}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  } else {
    return <Typography>Loading</Typography>
  }
}

export default AddressTable;

const useStyles = makeStyles(theme => ({
  title: {
    marginBottom: '10px',
    color: colors.primary
  },
  table: {
    maxWidth: '90%',
    margin: 'auto'
  }
}))