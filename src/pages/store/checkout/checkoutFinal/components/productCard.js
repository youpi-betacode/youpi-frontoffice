import React from 'react';
import { CardContent, Grid, Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/styles';

import { colors, PAID_DELIVERY, FREE_DELIVERY } from '../../../../../constants';
import { formatNumber } from '../../../../../common/utils';

function ProductCard({ product }) {
  const classes = useStyles();
  const { t } = useTranslation();

  const {
    reference,
    name,
    amount,
    price_promo,
    price_base
  } = product;

  let image = null;

  if (product.category === "YOUPi!") {
    const image_reference = product.reference.replace(/_20|_30/, "")
    image = `/images/store/youpiis/${image_reference}/0.jpg`;
  } else if (product.category === "VELAS" || product.category === "AMBIENTADORES") {
    image = `/images/store/fragrances/${product.reference}/0.jpg`;
  }

  if (product.extra && product.extra["simulation_data"]) {
    image = product.extra["simulation_data"]["small_filtered_photo"];
  }

  return (
    <CardContent className={classes.card}>
      <Grid container direction='row' justify='space-evenly' alignItems='center'>
        <Grid item md={2}>
          {
            !(reference === PAID_DELIVERY || reference === FREE_DELIVERY) && 
            <img src={image} alt='Product' width='100px' style={{ margin: '0px auto' }} />
          }
        </Grid>
        <Grid item md={6} style={{ textAlign: 'left' }}>
          <Typography variant='body2' className={classes.cardText}>
            {name}
          </Typography>
        </Grid>
        <Grid item md={2}>
          <Grid container direction='row' justify='center' alignItems='center' spacing={2}>
            <Grid item md={12}>
              {
                !(reference === PAID_DELIVERY || reference === FREE_DELIVERY) &&
                <Typography variant='body2' className={classes.cardText}>
                  Quantidade
                        </Typography>
              }
            </Grid>
            <Grid item md={12}>
              {
                !(reference === PAID_DELIVERY || reference === FREE_DELIVERY) && 
                <Typography variant='body2' className={classes.cardText}>
                  {amount}
                </Typography>
              }
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={2}>
          <Grid container direction='row' justify='center' alignItems='center' spacing={2}>
            <Grid item md={12}>
              <Typography variant='body2' className={classes.cardText}>
                {t('price-checkout')}
              </Typography>
            </Grid>
            <Grid item md={12}>
              <Typography variant='body2' className={classes.cardText}>
                {(reference === PAID_DELIVERY || reference === FREE_DELIVERY) ? formatNumber(price_base) : formatNumber(price_promo)}€
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </CardContent>
  )
}

export default ProductCard;

const useStyles = makeStyles(theme => ({
  card: {
    backgroundColor: colors.backgroundGrey,
    boxShadow: "2px 2px 4px #9E9E9E",
    paddingBottom: '16px !important'
  },
  cardText: {
    color: 'rgb(130,130,130)'
  }
}))
