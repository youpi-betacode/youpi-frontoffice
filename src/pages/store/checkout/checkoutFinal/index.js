import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/styles';
import { Container, Grid, Divider, Typography, Button, Hidden } from '@material-ui/core';
import PaymentTable from './components/paymentTable';
import ProductList from './components/productsList';
import ProductListSmall from './components/productListSmall';
import AddressTable from './components/addressTable';
import { getSale } from '../../../../redux/actions/CheckoutPaymentsActions';
import { colors } from '../../../../constants';
import { toggleLoading } from '../../../../redux/actions/LoadingActions';

function SaleStatus(status) {
  switch (status) {
    case 'SALE_STATUS_PAID':
      return <Typography variant='h5' style={{ color: 'gold', display: 'inline' }}>Pago</Typography>;
    case 'SALE_STATUS_PROCESSED':
      return <Typography variant='h5' style={{ color: 'gold', display: 'inline' }}>Processado</Typography>;
    case 'SALE_STATUS_FAILED':
      return <Typography variant='h5' style={{ color: 'red', display: 'inline' }}>Cancelado</Typography>;
    case 'SALE_STATUS_PENDING':
      return <Typography variant='h5' style={{ color: 'red', display: 'inline' }}>Pendente</Typography>;
    case 'SALE_STATUS_DELIVERED':
      return <Typography variant='h5' style={{ color: 'green', display: 'inline' }}>Entregue</Typography>;
    case 'SALE_STATUS_SHIPPED':
      return <Typography variant='h5' style={{ color: 'green', display: 'inline' }}>Enviado</Typography>;
    default:
      return <Typography variant='h5' style={{ color: 'red', display: 'inline' }}>Sem dados</Typography>;
  }
}

export function downloadFile(url){
  var a = document.createElement("a");
  document.body.appendChild(a);
  a.setAttribute("style","display: none");
  a.href = url;
  a.click();
  window.URL.revokeObjectURL(url);
}

function CheckoutFinal(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const reference = props.match.params.reference;

  useEffect(() => {
    dispatch(getSale(reference));
    dispatch(toggleLoading(false, "checkoutPayment"));
  }, [])

  const sale = useSelector(state => state.checkoutPayments.sale);

  if (sale) {
    const invoice = sale.extra.payment_info.invoice_url

    return (
      <Container className={classes.container}>
        <Grid container direction='row' justify='center' alignItems='center' spacing={3}>
          <Grid item md={12} style={{ textAlign: 'center' }}>
            <Typography variant='h4' className={classes.title}>
              Encomenda
            </Typography>
            <Typography variant='h5' className={classes.title}>
              Data: {sale.timestamp}
            </Typography>
            <Typography variant='h5' style={{ textAlign: 'center' }}>
              Estado da encomenda: {SaleStatus(sale.status)}
            </Typography>
          </Grid>
          {
            invoice &&
            <Grid item md={12} style={{ textAlign: 'center' }}>
              <Button color='primary' className={classes.button} onClick={() => downloadFile(invoice)}>Imprimir Fatura</Button>
            </Grid>
          }
          <Hidden smDown>
            <Grid item md={12}>
              <Divider />
            </Grid>
          </Hidden>
          <Grid item md={8} style={{ textAlign: 'center' }}>
            <AddressTable />
          </Grid>
          <Grid item md={4} style={{ textAlign: 'center' }}>
            <PaymentTable />
          </Grid>
          <Hidden smDown>
            <Grid item md={12}>
              <Divider />
            </Grid>
          </Hidden>
          <Grid item md={12} style={{ textAlign: 'center' }}>
            <Hidden smDown>
              <ProductList />
            </Hidden>
            <Hidden mdUp>
              <ProductListSmall />
            </Hidden>
          </Grid>
        </Grid>
      </Container>
    )
  } else {
    return <Typography>Loading</Typography>
  }

}

export default CheckoutFinal;

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 50,
    marginBottom: 50,
  },
  title: {
    marginBottom: '20px',
    color: colors.primary
  },
  button: {
    width: '200px'
  }
}))
