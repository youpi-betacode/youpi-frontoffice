import React from 'react';
import { useDispatch } from 'react-redux';
import { Grid, Typography, Link, InputBase, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { colors, PAID_DELIVERY, FREE_DELIVERY } from '../../../../../constants';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import {
   decrementProductOnShoppingCart,
   incrementProductOnShoppingCart, removeProductOnShoppingCart
} from "../../../../../redux/actions/ShoppingCartActions";
import { formatNumber } from '../../../../../common/utils';


function ProductCheckoutCard({ product }) {
   const classes = useStyles();
   const dispatch = useDispatch();
   const { t } = useTranslation();

   const {
      reference, 
      name,
      category,
      size, 
      amount,
      price_promo,
      price_base
   } = product

   let image = null;
          
   if (category === "YOUPi!") {
    if(reference === 'YB_BL_LX_30_2_UV')
      image = `/images/store/youpiis/YBBLLX_2_UV/0.jpg`
    else{
     const image_reference = product.reference.replace(/_20|_30/, "")
     image = `/images/store/youpiis/${image_reference}/0.jpg`;
    }
   } 
   else if (category === "VELAS" || category === "AMBIENTADORES") {
     image = `/images/store/fragrances/${reference}/0.jpg`;
   }

   if (product.extra && product.extra["simulation_data"]) {
     image = product.extra["simulation_data"]["small_filtered_photo"];
   }

   return (
      <Grid container alignItems='center' justify='center' spacing={3} className={classes.card}>
         <MuiThemeProvider theme={theme}>
            <Grid item xs={2} className={classes.imageWrapper}>
               {!(reference === PAID_DELIVERY || reference === FREE_DELIVERY) && 
               <Grid container alignItems='center' justify='center'>
                  <img src={image} alt="" className={classes.image} />
               </Grid>}
            </Grid>
            <Grid item xs={6}>
               <Grid container direction="column" className={classes.titleSizeContainer}>
                  <Grid item className={classes.title}>
                     {name}
                  </Grid>
                  <Grid item>
                     {size}
                  </Grid>
                  <Grid item className={classes.remove}>
                     {!(reference === PAID_DELIVERY || reference === FREE_DELIVERY) && <Typography>
                        <Link underline="always" onClick={() => dispatch(removeProductOnShoppingCart(reference))}>
                           {t('remove-checkout')}
                        </Link>
                     </Typography>}
                  </Grid>
               </Grid>
            </Grid>
            <Grid item xs={2}>

               {!(reference === PAID_DELIVERY || reference === FREE_DELIVERY || category === 'YOUPi! FEELING') &&
                  <Grid container direction='row' justify='center' alignItems='center' spacing={2}>
                     <Grid item xs={12} style={{ textAlign: 'center', paddingBottom: 0 }}>
                        <Typography variant='body2'>
                           Quantidade
                        </Typography>
                     </Grid>
                     <Grid item xs={12}>

                        <Grid container direction="row" justify="center" alignItems="center" spacing={2}>
                           <Grid item xs={4} alignItems="flex-end" justify="flex-end">
                              <Button onClick={() => dispatch(decrementProductOnShoppingCart(reference))}>
                                 -
                              </Button>
                           </Grid>
                           <Grid item xs={4} alignItems="center" justify="center">
                              <InputBase
                                 value={amount}
                                 disabled={true}
                              />
                           </Grid>
                           <Grid item xs={4}>
                              <Button onClick={() => dispatch(incrementProductOnShoppingCart(reference))}>
                                 +
                              </Button>
                           </Grid>
                        </Grid>
                     </Grid>
                  </Grid>
               }

            </Grid>
            <Grid item xs={2} className={classes.price}>
               <Grid container direction='row' justify='center' alignItems='center' spacing={2}>
                  <Grid item xs={12} style={{ paddingTop: 0 }}>
                     <Typography variant='body2'>
                        {t('price-checkout')}
                     </Typography>
                  </Grid>
                  <Grid item xs={12}>
                  {(reference === PAID_DELIVERY || reference === FREE_DELIVERY) ? formatNumber(price_base*amount) : formatNumber(price_promo*amount)}€
            </Grid>
               </Grid>
            </Grid>
         </MuiThemeProvider>
      </Grid>
   )
}

export default ProductCheckoutCard;


const theme = createMuiTheme({
   overrides: {
      MuiLink: {
         root: {
            color: colors.primary,
            fontSize: 14,
            cursor: 'pointer',
            transition: '0.3s',
            '&:hover': {
               color: colors.linkDark
            }
         },
         underlineNone: {
            color: colors.htmlGrey,
            fontSize: 18,
         }
      },
      MuiInputBase: {
         input: {
            textAlign: 'center',
            backgroundColor: 'white',
            width: '40px',
            height: '40px',
            borderRadius: '50%',
            padding: 0,
         }
      },
      MuiButton: {
         root: {
            minWidth: '40px',
         }
      }
   }
});

const useStyles = makeStyles(theme => ({
   card: {
      height: 120,
      backgroundColor: colors.backgroundGrey,//'#f4f4f5',
      marginBottom: 30,
      boxShadow: "2px 2px 4px #9E9E9E"
   },
   imageWrapper: {
      //backgroundColor: 'red'
   },
   image: {
      maxWidth: '100px',
      maxHeight: '100px'
   },
   titleSizeContainer: {
      //paddingLeft: 20
      //backgroundColor: 'orange'
   },
   title: {
      fontSize: 14,
      textAlign: 'left'
      // textTransform: 'uppercase'
   },
   remove: {
      marginTop: 10,
      textAlign: 'left'
   },
   price: {
      fontSize: 14,
      textAlign: 'center'
   }
}))
