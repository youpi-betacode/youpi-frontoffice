import React from 'react';
import { Grid, Hidden, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { colors } from '../../../../../constants';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { defaultProductPhoto } from "../../../../../constants";
import { push } from 'connected-react-router';

import ProductCheckoutCard from './ProductCheckoutCard';
import ProductCheckoutSmallCard from './ProductCheckoutSmallCard';

function ProductCheckoutList() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const cart = useSelector(state => state.shoppingCart.cart);

  const { t } = useTranslation();

  return (
    <Grid container alignItems='center' justify='center'>
      {
        cart && cart.products && cart.products.length > 0 &&
        cart.products.map((product, index) => {

          return (
            <React.Fragment>
              <Hidden smDown>
                <ProductCheckoutCard
                  key={index}
                  product={product}
                />
              </Hidden>
              <Hidden mdUp>
                <ProductCheckoutSmallCard
                  key={index}
                  product={product}
                />
              </Hidden>
            </React.Fragment>
          )
        }
        )
      }
      {
        !cart || !cart.products || cart.products.length === 0 &&
        <Grid container>
          <Grid item xs={12} className={classes.noProductsDiv}>
            <Typography variant='h5'>
              {t('no-products-in-shopping-cart')}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Button color='primary' className={classes.button} onClick={() => dispatch(push('/store'))}>
              {t('back-to-store')}
            </Button>
          </Grid>
        </Grid>
      }
    </Grid>
  )
}

export default ProductCheckoutList;

const useStyles = makeStyles(theme => ({
  header: {
    marginBottom: 25
  },
  headerTitle: {
    textAlign: 'center',
    color: colors.primary,
    fontWeight: 600
  },
  noProductsDiv: {
    marginBottom: 30
  }
}))
