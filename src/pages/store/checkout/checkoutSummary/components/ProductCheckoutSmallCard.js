import React from 'react';
import { Grid, Typography, Button, InputBase, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import clsx from 'clsx';

import { colors, PAID_DELIVERY, FREE_DELIVERY } from '../../../../../constants';
import {
   decrementProductOnShoppingCart,
   incrementProductOnShoppingCart, removeProductOnShoppingCart
} from "../../../../../redux/actions/ShoppingCartActions";
import { formatNumber } from '../../../../../common/utils';

function ProductCheckoutSmallCard({ product }) {
   const classes = useStyles();
   const dispatch = useDispatch();
   const { t } = useTranslation();

   const {
      reference,
      name,
      category,
      size,
      amount,
      price_base,
      price_promo
   } = product

   let image = null;

   if (category === "YOUPi!") {
     if(product.reference === 'YB_BL_LX_30_2_UV')
      image = `/images/store/youpiis/YBBLLX_2_UV/0.jpg`
     else{
        const image_reference = product.reference.replace(/_20|_30/, "")
        image = `/images/store/youpiis/${image_reference}/0.jpg`;
     }
   } 
   else if (category === "VELAS" || category === "AMBIENTADORES") {
      image = `/images/store/fragrances/${reference}/0.jpg`;
   }

   if (product.extra && product.extra["simulation_data"]) {
      image = product.extra["simulation_data"]["small_filtered_photo"];
   }
   return (
      <MuiThemeProvider theme={theme}>
         <Grid container direction='row' justify='center' alignItems='center' className={classes.card}>
            <Grid item xs={12} className={classes.grids} style={{ margin: 30 }}>
               {!(reference === PAID_DELIVERY || reference === FREE_DELIVERY) && <img src={image} alt='product image' className={classes.image} />}
            </Grid>
            <Grid item xs={12}>
               <Typography variant='body2'>
                  {name}
               </Typography>
            </Grid>
            {!(reference === PAID_DELIVERY || reference === FREE_DELIVERY) && <Grid item xs={6} className={classes.grids}>
               <Typography variant='body2'>
                  Quantidade
               </Typography>
               <Grid item xs={12}>
                  <Grid container direction="row" justify="center" alignItems="center" spacing={1}>
                     <Grid item xs={4} alignItems="flex-end" justify="flex-end">
                        <Button onClick={() => dispatch(decrementProductOnShoppingCart(reference))}>
                           -
                     </Button>
                     </Grid>
                     <Grid item xs={4} alignItems="center" justify="center">
                        <InputBase
                           value={amount}
                           disabled={true}
                        />
                     </Grid>
                     <Grid item xs={4}>
                        <Button onClick={() => dispatch(incrementProductOnShoppingCart(reference))}>
                           +
                        </Button>
                     </Grid>
                  </Grid>
               </Grid>
            </Grid>}
            {!(reference === PAID_DELIVERY || reference === FREE_DELIVERY) && <Grid item xs={6} className={classes.grids}>
               <Typography variant='body2'>
                  Preço
               </Typography>
               <Typography variant='body2'>
                  {(reference === PAID_DELIVERY || reference === FREE_DELIVERY) ? formatNumber(price_base*amount) : formatNumber(price_promo*amount)}€
               </Typography>
            </Grid>}
            {(reference === PAID_DELIVERY || reference === FREE_DELIVERY) && 
            <Grid xs={12} className={classes.grids}>
               <Typography variant='body2'>
                  Preço
               </Typography>
               <Typography variant='body2'>
                  {formatNumber(price_base)}€
               </Typography>
            </Grid>}
            <Grid item xs={12} className={clsx(classes.grids)}>
               {!(reference === PAID_DELIVERY || reference === FREE_DELIVERY) && <Typography>
                  <Link underline="always" onClick={() => dispatch(removeProductOnShoppingCart(reference))}>
                     {t('remove-checkout')}
                  </Link>
               </Typography>}
            </Grid>
         </Grid>
      </MuiThemeProvider>
   )
}

export default ProductCheckoutSmallCard;

const theme = createMuiTheme({
   overrides: {
      MuiLink: {
         root: {
            color: colors.primary,
            fontSize: 14,
            cursor: 'pointer',
            transition: '0.3s',
            '&:hover': {
               color: colors.linkDark
            }
         },
         underlineNone: {
            color: colors.htmlGrey,
            fontSize: 18,
         }
      },
      MuiInputBase: {
         input: {
            textAlign: 'center',
            backgroundColor: 'white',
            width: '40px',
            height: '40px',
            borderRadius: '50%',
            padding: 0,
         }
      },
      MuiButton: {
         root: {
            minWidth: '40px',
         }
      }
   }
});

const useStyles = makeStyles(theme => ({
   card: {
      backgroundColor: colors.backgroundGrey,//'#f4f4f5',
      marginBottom: 30,
      boxShadow: "2px 2px 4px #9E9E9E",
   },
   image: {
      maxWidth: '100px',
      maxHeight: '100px',
   },
   grids: {
      marginTop: 20,
      marginBottom: 10
   }
}))
