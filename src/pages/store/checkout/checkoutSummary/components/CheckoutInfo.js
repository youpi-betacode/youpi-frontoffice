import React from 'react';
import { Grid, Typography, Link, InputBase } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { colors } from '../../../../../constants';

function CheckoutInfo() {
  const classes = useStyles();

  return (
    <Grid container direction='column' spacing={1} className={classes.info}>
      <Grid item xs={12}>
        • O Pagamento é 100% seguro e criptografado.
      </Grid>
      <Grid item xs={12}>
        • Os nossos produtos são enviados em embalagens especiais para garantir a sua proteção durante o transporte.
      </Grid>
      <Grid item xs={12}>
        • 99% dos nossos clientes estão satisfeitos!
        (Estatistica retirada de mais de 20.000 revisões realizadas por meio de uma pesquisa de satisfação)
      </Grid>
      <Grid item xs={12}>
        • Prazo de Entrega até 5 dias úteis.
      </Grid>
    </Grid>
   
  )
}

export default CheckoutInfo;


const useStyles = makeStyles(theme => ({
  info: {
    marginTop: theme.spacing(4),
    color: colors.textColor,
    fontStyle: 'italic',
  }
}))