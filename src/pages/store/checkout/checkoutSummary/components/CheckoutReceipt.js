import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { colors } from '../../../../../constants';
import { createMuiTheme } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

import { deliveryFee, deliveryFeeThreshold } from '../../../../../constants';
import { formatNumber } from '../../../../../common/utils';

function CheckoutReceipt() {
  const classes = useStyles();
  const cart = useSelector(state=>state.shoppingCart.cart);
  const { t } = useTranslation();
  const subtotal = cart.total_price;

  return (
    <Grid container alignItems='center' justify='center' className={classes.receipt}>
      <Grid item xs={12} className={classes.receiptCard}>
        <Grid container direction="column" spacing={2} >
          {/* <Grid item xs={12} >
            <Grid container className={classes.receiptItem}>
              <Grid item xs={8}>
                <Typography variant='body2' className={classes.labels}>
                  {t('subtotal')}
                </Typography>
              </Grid>
              <Grid item xs={4}>
                {subtotal}€
              </Grid>
            </Grid>
          </Grid> */}
          {/* <Grid item xs={12} >
            <Grid container className={classes.receiptItem}>
              <Grid item xs={8}>
                <Typography variant='body2'>
                  {t('delivery-fee')}
                </Typography>
              </Grid>
              <Grid item xs={4}>
                {subtotal > deliveryFeeThreshold ? 0 : deliveryFee}€
              </Grid>
            </Grid>
          </Grid> */}
          {/*<Grid item xs={12} >*/}
          {/*  <Grid container className={classes.receiptItem}>*/}
          {/*    <Grid item xs={8}>*/}
          {/*      YOUPi! Coins*/}
          {/*    </Grid>*/}
          {/*    <Grid item xs={4}>*/}
          {/*      {youpiCoins}€*/}
          {/*    </Grid>*/}
          {/*  </Grid>*/}
          {/*</Grid>*/}
          {/*<Grid item xs={12} >*/}
          {/*  <Grid container alignItems='center' justify='center' spacing={2} className={classes.discount}>*/}
          {/*    <Grid item xs={12} className={classes.discountText}>*/}
          {/*      Adicionar código promocional ou cartão presente*/}
          {/*    </Grid>*/}
          {/*    <Grid item xs={12}>*/}
          {/*      <Grid container alignItems='center' justify='center'>*/}
          {/*        <MuiThemeProvider theme={theme}>*/}
          {/*          <TextField*/}
          {/*            variant="outlined"*/}
          {/*            fullWidth*/}
          {/*          />*/}
          {/*        </MuiThemeProvider>*/}
          {/*      </Grid>*/}
          {/*    </Grid>*/}
          {/*  </Grid>*/}
          {/*</Grid>*/}
          <Grid item xs={12} >
            <Grid container className={classes.receiptItem}>
              <Grid item xs={8}>
                <Typography variant='body2' className={classes.labels}>
                  {t('total')}
                </Typography>
              </Grid>
              <Grid item xs={4}>
                {/*{subtotal + delivery + youpiCoins}€*/}
                {formatNumber(subtotal)} €
              </Grid>
            </Grid>
          </Grid>
        </Grid>

      </Grid>
      <Grid item xs={12} className={classes.checkoutNote}>
        {t('delivery-fee-threshold-info')}
      </Grid>
    </Grid>
  )
}

export default CheckoutReceipt;

const theme = createMuiTheme({
  // palette: {
  //   primary: colors.blue
  // },
  palette: {
    primary: {
      main: colors.blue
    },
    background: {
      default: colors.white
    }
  }
});


const useStyles = makeStyles(theme => ({
  receipt: {

    boxShadow: "2px 2px 4px #9E9E9E",
  },
  receiptCard: {
    padding: theme.spacing(3),
    backgroundColor: colors.backgroundGrey
  },
  receiptItem: {
    fontSize: 14,
    height: 16
  },
  discount: {
    // marginTop: theme.spacing(1),
    // marginBottom: theme.spacing(0.5)
  },
  discountText: {
    color: colors.blue,
    fontStyle: 'italic',
    textAlign: 'center'
  },
  checkoutNote: {
    backgroundColor: colors.blue,
    color: colors.white,
    fontStyle: 'italic',
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    textAlign: 'center'
  },
  labels: {
    textTransform: 'capitalize'
  }

}));
