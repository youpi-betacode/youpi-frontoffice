import React, { useEffect } from 'react';
import { Grid, Container, Button } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import { push } from 'connected-react-router';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import useTheme from "@material-ui/core/styles/useTheme";

import CheckoutStepper from '../common/Stepper';
import ProductCheckoutList from './components/ProductCheckoutList';
import CheckoutReceipt from './components/CheckoutReceipt';
import CheckoutInfo from './components/CheckoutInfo';
import { setCheckoutStep } from '../../../../redux/actions/CheckoutStepsActions';

function CheckoutSummary() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const theme = useTheme();
  const { t } = useTranslation();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));

  const cart = useSelector(state => state.shoppingCart.cart);
  const token = localStorage.getItem('token');

  const handleNextClick = () => {
    if (token) {
      dispatch(push('/checkout/addresses'));
    }
    else
      dispatch(push('/user/login?return=/checkout/summary'));
  };

  useEffect(() => {
    // dispatch(getCartProducts());
    dispatch(setCheckoutStep(0))
  }, []);

  return (
    <React.Fragment>
      <Container className={classes.container}>
        <Grid container direction='row' alignItems='flex-start'>
          <Grid item xs={11} md={8} style={{ margin: 'auto' }}>
            <Grid container direction='column' spacing={3}>
              <Grid item xs={12}>
                <CheckoutStepper />
              </Grid>
              <Grid item xs={12} style={{ textAlign: 'center' }}>
                <ProductCheckoutList />
              </Grid>
            </Grid>
          </Grid>
          {cart && cart.total_products > 0 &&
            <Grid item md={4}>
              <Grid container direction='column' spacing={3} className={isSmallDevice ? '' : classes.rightPanel}>
                <Grid item md={12}>
                  <CheckoutReceipt />
                </Grid>
                <Grid item xs={11} md={12} style={{ margin: 'auto' }}>
                  <Grid container direction='column' justify='center' alignItems='center' spacing={2}>
                    <Grid item xs={12}>
                      <Button color='primary' className={classes.button} onClick={() => dispatch(push('/store'))}>
                        {t('back-to-store')}
                      </Button>
                    </Grid>
                    <Grid item xs={12}>
                      <Button color='primary' className={classes.button} onClick={handleNextClick}>
                        {t('advance')}
                      </Button>
                    </Grid>
                    <Grid item xs={12}>
                      <CheckoutInfo />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          }
        </Grid>
      </Container>
    </React.Fragment >
  )
}

export default CheckoutSummary;

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 50,
    marginBottom: 50,
  },
  rightPanel: {
    marginLeft: theme.spacing(8),
  },
  button: {
    marginTop: 14,
    width: 250
  }
}))
