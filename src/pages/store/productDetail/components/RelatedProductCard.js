import React from 'react';
import { CardContent, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { push } from 'connected-react-router'
import { useDispatch } from 'react-redux';
import { colors } from '../../../../constants';
import { useTranslation } from 'react-i18next';
import { formatNumber } from '../../../../common/utils';

function RelatedProductCard({ product }) {
   const classes = useStyles();
   const dispatch = useDispatch();
   const { t } = useTranslation();

   const handleProductClick = reference => {
      dispatch(push(`/store/product/${reference}`))
   }

   let price = product.variants.length !== 0 ? product.variants[0].price_base : product.price_base;
   let promo_price = product.variants.length !== 0 ? product.variants[0].price_promo : product.price_promo;
   let percent = 100 - (promo_price * 100 / price)

   return (
      <CardContent className={classes.content} onClick={() => handleProductClick(product.reference)}>
         <Grid container>
            <Grid item xs={12}>
               {
                  product &&
                  product.category === "YOUPi!" &&
                  <img src={`/images/store/youpiis/${product.reference}/0.jpg`} alt='Todo' height='150' />
               }
               {
                  product &&
                  (product.category === "VELAS" || product.category === "AMBIENTADORES") &&
                  <img src={`/images/store/fragrances/${product.reference}/0.jpg`} alt='Todo' height='150' />
               }
            </Grid>
            <Grid item xs={12} className={classes.carouselItemText}>
               <Typography variant='body2' className={classes.itemDescription}>
                  <b>{product.name}</b>
               </Typography>
               <Typography variant='body2' style={{ fontFamily: 'ComfortaaRegular' }}>
                  <b>{formatNumber(price)}€</b>
               </Typography>
               {
                  promo_price !== price &&
                  <Typography variant='body2' className={classes.promotion}>
                     {`${t('promotion')} (${percent}%): ${formatNumber(promo_price)}€`}
                  </Typography>
               }
            </Grid>
         </Grid>
      </CardContent >
   )
}

export default RelatedProductCard;

const useStyles = makeStyles(theme => ({
   carouselItemText: {
      marginTop: 10,
      color: colors.darkerPrimary,
   },
   content: {
      textAlign: 'center',
      cursor: "pointer"
   },
   itemDescription: {
      marginBottom: 3,
      fontFamily: 'ComfortaaRegular'
   },
   promotion: {
      color: colors.blue,
      fontWeight: '700'
   }
}))
