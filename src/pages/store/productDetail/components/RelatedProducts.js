import React, { useState } from 'react';
import ItemCarousel from 'react-items-carousel';
import { Grid, Card, Typography } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import {colors} from '../../../../constants';
import { useSelector } from 'react-redux';
import RelatedProductCard from './RelatedProductCard';



function CarrouselProducts() {
  const classes = useStyles();
  const theme = useTheme();
  const [activeItemIndex, setActivItemIndex] = useState(0);
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
  const changeActiveItem = (activeItemIndex) => {
    setActivItemIndex(activeItemIndex);
  }

  const relatedProducts = useSelector(state => state.product.relatedProducts);

  if (isSmallDevice) {
    return (
      <ItemCarousel
        activeItemIndex={activeItemIndex}
        infiniteLoop={true}
        gutter={10}
        activePosition={'center'}
        chevronWidth={60}
        disableSwipe={false}
        alwaysShowChevrons={false}
        numberOfCards={1}
        slidesToScroll={1}
        outsideChevron={true}
        showSlither={false}
        firstAndLastGutter={false}
        requestToChangeActive={value => changeActiveItem(value)}
      >
        {
          relatedProducts.length > 0 &&
          relatedProducts.map((product)=>
            <Card className={classes.card}>
              <RelatedProductCard product={product}/>
            </Card>
          )
        }

      </ItemCarousel>
    );
  } else {
    return (
      <ItemCarousel
        activeItemIndex={activeItemIndex}
        infiniteLoop={true}
        gutter={10}
        activePosition={'center'}
        chevronWidth={60}
        disableSwipe={false}
        alwaysShowChevrons={false}
        numberOfCards={3}
        slidesToScroll={1}
        outsideChevron={true}
        showSlither={false}
        firstAndLastGutter={false}
        requestToChangeActive={value => changeActiveItem(value)}
        rightChevron={<p className={classes.chevrons}>&gt;</p>}
        leftChevron={<p className={classes.chevrons}>&lt;</p>}
      >
         {
          relatedProducts.length > 0 &&
          relatedProducts.map((product)=>
            <Card className={classes.card}>
              <RelatedProductCard product={product}/>
            </Card>
          )
        }
      </ItemCarousel>
    );
  }
}

function RelatedProducts() {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    // <div className={classes.mainDiv}>
    <Grid container direction='row'>
      <Grid item xs={12} className={classes.suggestionsGrid}>
        <Typography variant='body2' className={classes.suggestions}>
          <b>{t('product-sugestions')}</b>
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <CarrouselProducts />
      </Grid>
    </Grid>
    // </div>
  )
}

export default RelatedProducts;

const useStyles = makeStyles(theme => ({
  chevrons: {
    fontSize: 60,
    color: colors.darkerPrimary,
  },
  card: {
    boxShadow: 'none'
  },
  mainDiv: {
    padding: "0 60px",
    width: "100%",
    margin: "0 auto"
  },
  suggestions: {
    marginBottom: 10,
    color: colors.darkerPrimary,
    textTransform: 'uppercase'
  },
  suggestionsGrid: {
    marginTop: 40
  }
}))
