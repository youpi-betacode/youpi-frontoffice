import React from 'react';
import {useSelector} from 'react-redux';
import {makeStyles, useTheme} from '@material-ui/styles';
import {Grid} from '@material-ui/core';
import {useTranslation} from 'react-i18next';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";

import PhotoSmallCard from './photoSmallCard';

function PhotoColumn({photo1, photo2, photo3}) {
    const classes = useStyles();
    const theme = useTheme();
    const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
    const product = useSelector(state => state.product.product);

    return (
        <Grid container direction='column' justify='center' alignItems='center' spacing={2}>
            {
                <React.Fragment>
                    <Grid item xs={12} className={isSmallDevice ? classes.photoGridsSmallDevice : {}}>
                        {photo1 && <PhotoSmallCard image={photo1}/>}
                    </Grid>
                    <Grid item xs={12} className={isSmallDevice ? classes.photoGridsSmallDevice : {}}>
                        {photo2 && <PhotoSmallCard image={photo2}/>}
                    </Grid>
                    <Grid item xs={12} className={isSmallDevice ? classes.photoGridsSmallDevice : {}}>
                        {photo3 && <PhotoSmallCard image={photo3}/>}
                    </Grid>
                </React.Fragment>
            }

        </Grid>
    );
}

export default PhotoColumn;

const useStyles = makeStyles(theme => ({
    photoGridsSmallDevice: {
        width: '60%'
    }
}))
