import React from 'react';
import { Grid, Typography, TextField, InputAdornment, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import { Search } from '@material-ui/icons';

import { colors } from '../../../../constants';

function ProductDetailHeader() {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <Grid container direction='row' alignContent='center'>
      <Grid item xs={12} md={2}>
        <Typography variant='body2' className={classes.costumerSupportText}>
          <strong>{t('costumer-servie-support')}</strong>
        </Typography>
      </Grid>
      <Grid item xs={12} md={2} style={{ textAlign: 'left' }}>
        <Typography variant='body2' className={classes.newsletterText}>
          <strong>{t('newsletter')}</strong>
        </Typography>
      </Grid>
      <Grid item xs={12} md={4} style={{ textAlign: 'center' }}>
        <div>
          LOGO
        </div>
        <div>
          <Typography variant='body1' style={{ color: colors.darkerPrimary }}>
            <strong><u>YOUPi!</u> / AMBIENTADORES / VELAS</strong>
          </Typography>
        </div>
      </Grid>
      <Grid item xs={12} md={4} style={{ textAlign: 'right', paddingRight: 20 }}>
        <div>
          <Typography variant='body2' style={{ color: colors.darkerPrimary }}>
            LOG IN / SIGN UP
            </Typography>
        </div>
        <div style={{marginTop: '-16px !important'}}>
          <MuiThemeProvider theme={theme}>
            <TextField
              id="standard-name"
              label={t('search-products')}
              className={classes.textField}
              // value={values.name}
              // onChange={handleChange('name')}
              margin="normal"
              InputProps={{
                endAdornment: (
                  <InputAdornment position='end'>
                    <IconButton
                      edge='end'
                      //onClick={handleClickSearch}
                    >
                      <Search />
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />
          </MuiThemeProvider>
        </div>
      </Grid>
    </Grid>
  )
}

export default ProductDetailHeader;

const useStyles = makeStyles(theme => ({
  newsletterText: {
    textTransform: 'capitalize',
    color: colors.darkerPrimary,
    cursor: 'pointer'
  },
  costumerSupportText: {
    color: colors.darkerPrimary,
    cursor: 'pointer'
  }
}));

const theme = createMuiTheme({
  overrides: {
    MuiInput: {
      root: {
        color: colors.darkerPrimary,
      },
      underline: {
        '&:before': {
          borderBottomWidth: 2,
          borderBottomColor: colors.blue,
        },
        '&:hover:not($disabled):before': {
          borderBottomWidth: 2,
          borderBottomColor: colors.blue
        },
        '&:after': {
          borderBottomWidth: 2,
          borderBottomColor: colors.blue
        }
      }
    },
    MuiInputLabel: {
      root: {
        color: colors.darkerPrimary,
        "&$focused": {
          color: colors.darkerPrimary
        }
      }
    }
  }
});