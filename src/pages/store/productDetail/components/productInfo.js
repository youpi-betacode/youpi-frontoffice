import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import {
   Grid, Typography, Button, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails,
   List, ListItem, ListItemText, Icon
} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";
import { push } from 'connected-react-router';

import { colors } from '../../../../constants';
import { useTranslation } from 'react-i18next';
import { setSize, setCurrentPrice, setCurrentRef, setCurrentProduct } from '../../../../redux/actions/ProductActions';
import { incrementProductOnShoppingCart } from "../../../../redux/actions/ShoppingCartActions";
import { formatNumber } from '../../../../common/utils';

function PanelIcon(props) {
   const classes = useStyles();

   if (props.expanded === props.panel) {
      return <Icon className={classes.iconCarret}>keyboard_arrow_up</Icon>;
   } else {
      return <Icon className={classes.iconCarret}>keyboard_arrow_down</Icon>;
   }
}

function PanelHeading(props) {
   const classes = useStyles();

   const heading = props.expanded !== props.panel && props.selectedValue ? props.selectedValue : props.heading;

   if (props.expanded === props.panel || !props.selectedValue) {
      return (
         <React.Fragment>
            <Typography className={classes.heading}>{heading} <PanelIcon expanded={props.expanded}
               panel={props.panel} /></Typography>
         </React.Fragment>
      );
   } else {
      return (
         <React.Fragment>
            <Typography className={classes.heading}>{heading} <PanelIcon expanded={props.expanded}
               panel={props.panel} /></Typography>
         </React.Fragment>
      )
   }
}

function SelectBest() {
   const classes = useStyles();
   const dispatch = useDispatch();
   const { t } = useTranslation();

   const [expanded, setExpanded] = React.useState(false);
   const product = useSelector(state => state.product.product);
   const size = useSelector(state => state.product.size);
   const selectedSize = size ? size : null;

   const handleChange = panel => (event, isExpanded) => {
      setExpanded(isExpanded ? panel : false);
   };

   const handleSelectBest = variant => {
      setExpanded(false);
      dispatch(setCurrentProduct(variant))
      dispatch(setSize(variant.name));
      dispatch(setCurrentPrice(variant.price_base));
      dispatch(setCurrentRef(variant.reference));
   }

   useEffect(() => {
      if (product && product.variants.length !== 0) {
         handleSelectBest(product.variants[0])
      }
   }, [product])

   return (
      <div className={classes.root}>
         {product && product.variants.length !== 0 &&
            <ExpansionPanel expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
               <ExpansionPanelSummary>
                  <PanelHeading expanded={expanded} panel="panel1" selectedValue={size} heading={t('size')} />
               </ExpansionPanelSummary>
               <ExpansionPanelDetails>
                  <List className={classes.list}>
                     {
                        product &&
                        product.variants.map(variant =>
                           <ListItem
                              button
                              onClick={event => handleSelectBest(variant)}
                              selected={selectedSize && selectedSize === variant.name}
                           >
                              <ListItemText primary={variant.name} style={{ textAlign: "center" }} />
                           </ListItem>
                        )
                     }
                  </List>
               </ExpansionPanelDetails>
            </ExpansionPanel>
         }
      </div>
   );
}

function ProductInfo() {
   const classes = useStyles();
   const dispatch = useDispatch();
   const { t } = useTranslation();
   const [expanded, setExpanded] = React.useState(false);
   const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
   const currentProduct = useSelector(state => state.product.currentProduct);

   const handleChange = panel => (event, isExpanded) => {
      setExpanded(isExpanded ? panel : false);
   };

   const handleOnSimulatorClick = () => {
      dispatch(push('/simulator'))
   }

   return (
      <Grid container direction='column' alignItems='center' spacing={2}>
         <Grid item xs={12} className={isSmallDevice ? classes.summarySmallDevice : classes.summary}>
          
            <Typography variant='body1'>
               {
                  currentProduct &&
                  <b>{currentProduct.name}</b>
               }

            </Typography>
            <Typography variant='body1'>
               {
                  currentProduct &&
                  <b>{formatNumber(currentProduct.price_base)}€/UN.</b>
               }
            </Typography>

            {  
               currentProduct &&
               currentProduct.price_promo !== currentProduct.price_base &&
               <Typography variant='body1' className={classes.promotion}>
                  {`${t('promotion')} (${100 - (currentProduct.price_promo * 100 / currentProduct.price_base)}%): ${formatNumber(currentProduct.price_promo)}€/UN`}
               </Typography>
            }
         </Grid>
         <Grid item xs={12}>
            <MuiThemeProvider theme={theme}>
               <SelectBest expanded={expanded} handleChange={handleChange} setExpanded={setExpanded} />
            </MuiThemeProvider>
            <Button className={classes.btn} color="primary" onClick={handleOnSimulatorClick}>
               <img src="/images/icons/simulations_white.png" alt="icon" className={classes.icon} />
               {t('simulator')}
            </Button>
            {/*<Button className={classes.btn} color="primary">*/}
            {/*    <img src="/images/icons/wishlist_white.png" alt="icon" className={classes.icon}/>*/}
            {/*    Whishlist*/}
            {/*</Button>*/}
            <Button className={classes.btn} color="primary" onClick={() => dispatch(incrementProductOnShoppingCart(currentProduct.reference))}>
               {t('add-to-cart')}
            </Button>
            <Button className={classes.btn} color="primary" onClick={() => dispatch(push(`/store/`))}>
               {t('back')}
            </Button>
         </Grid>
         {currentProduct && currentProduct.description &&
            <Grid item xs={12}>
               <Typography className={classes.infoSub}>
                  {currentProduct.description}
               </Typography>
            </Grid>
         }
      </Grid>
   )
}

export default ProductInfo;

const useStyles = makeStyles(theme => ({
   promotion: {
      color: colors.blue,
      fontWeight: '700'
   },
   btn: {
      margin: '10px 0px 5px 0px',
      width: '100%',
      maxHeight: '192px',
      overflow: 'auto',
      textTransform: 'uppercase',
      textAlign: 'center'
   },
   icon: {
      width: '24px',
      height: '24px',
      marginRight: '10px'
   },
   summary: {
      color: colors.darkerPrimary,
      textAlign: 'left',
      width: '100%'
   },
   summarySmallDevice: {
      color: colors.darkerPrimary,
      width: '100%',
      textAlign: 'center'
   },
   list: {
      textAlign: 'center'
   },
   iconCarret: {
      position: "absolute",
      right: 10,
      top: 15,
      color: colors.primary
   },
   heading: {
      textTransform: "uppercase",
      textAlign: "center",
      width: "100%",
      color: colors.darkerPrimary
   },
   info: {
      margin: '20px 0px 20px 0px'
   },
   infoTitle: {
      color: colors.blue,
      fontSize: '16px'
   },
   infoSub: {
      margin: '10px 0px 0px 0px',
      color: colors.blue,
      fontSize: '12px'
   },
   infoText: {
      color: colors.blue,
      fontSize: '12px'
   }
}))

const theme = createMuiTheme({
   overrides: {
      MuiButton: {
         textPrimary: {
            padding: 0
         },
         root: {
            padding: 0
         }
      },
      MuiExpansionPanel: {
         root: {
            marginBottom: '10px',
            backgroundColor: colors.backgroundGrey,
            boxShadow: 'none',

            '&:before': {
               display: 'none',
            },
            '&$expanded': {
               margin: 'auto',
            },
         },
         expanded: {},
      },
      MuiExpansionPanelSummary: {
         root: {
            marginTop: '10px',
            border: '1px solid',
            borderColor: colors.primary,
            borderRadius: '5px',
            backgroundColor: colors.white,
            minHeight: 56,

            '&$expanded': {
               minHeight: 56,
            },
         },
         content: {
            '&$expanded': {
               padding: 0,
               margin: '12px 0',
            },
         },
      },
      MuiExpansionPanelDetails: {
         root: {
            padding: 0,
            backgroundColor: colors.backgroundGrey
         }
      },
      MuiList: {
         root: {
            margin: 0,
            width: '100%'
         },
         padding: {
            paddingTop: '0px',
            paddingBottom: '0px'
         }
      }
   }
});
