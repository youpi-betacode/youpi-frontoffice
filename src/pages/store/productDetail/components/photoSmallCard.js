import React from 'react';
import { Card, CardContent } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/styles';
import { useDispatch } from 'react-redux';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";

import { colors } from '../../../../constants';


function PhotoSmallCard({ image }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Card className={classes.card}>
      <CardContent className={isSmallDevice ? classes.smallContent : classes.content}>
        <img src={image} alt='YOUPI' className={classes.image}/>
      </CardContent>
    </Card>
  )
}

export default PhotoSmallCard;

const useStyles = makeStyles(theme => ({
  image: {
    maxWidth: '200px',
  },
  // wrapper: {
  //   width: '150px',
  //   height: '1025px',
  //   borderWidth: '1px',
  //   borderStyle: 'solid',
  //   borderColor: 'lightgrey',
  //   display: 'flex',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   marginBottom: '10px'
  // },
  content: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderStyle: 'solid',
    borderWidth: 3,
    borderColor: colors.backgroundGrey,
    padding: '16px !important'
  },
  smallContent: {
    textAlign: 'center',
    marginBottom: 10
  },
  card: {
    boxShadow: 'none',
    cursor: 'pointer'
  }
}))
