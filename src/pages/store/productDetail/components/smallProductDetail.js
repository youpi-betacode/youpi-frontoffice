import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Grid, Container } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/styles';

import { colors } from '../../../../constants';
import RelatedProducts from './RelatedProducts';
import ProductPhoto from './productPhoto';
import ProductInfo from './productInfo';
import Footer from '../../../../common/footer/Footer';
import Loader from '../../../../common/loading/Loading';
import SmallColumnCarousel from './smallColunCarousel';

import { getProduct, getRelatedProducts } from '../../../../redux/actions/ProductActions';

export default function SmallProductDetail({props}) {
  const classes = useStyles();
  const product = useSelector(state => state.product.product);
  const productId = props.match.params.id;
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(getProduct(productId))
  }, [productId])

  useEffect(()=>{
    if(product !== null){
      dispatch(getRelatedProducts(product.reference))
    }
  }, [product])

  return (
    <Loader loading='product'>
      <React.Fragment>
        <Container className={classes.container}>
          <Grid container direction='row' justify='center' alignItems='center'>
            <Grid item xs={12} md={7} style={{ marginTop: 30, marginBottom: 30 }}>
              {
                product &&
                product.category === "YOUPi!" &&
                <ProductPhoto photo={`/images/store/youpiis/${productId}/0.gif`} />
              }
              {
                product &&
                (product.category === "VELAS" || product.category === "AMBIENTADORES") &&
                <ProductPhoto photo={`/images/store/fragrances/${productId}/0.jpg`} />
              }
            </Grid>
            <Grid item xs={12}>
              {
                product &&
                product.category === "YOUPi!" &&
                <SmallColumnCarousel productId={productId} />
              }
            </Grid>
            <Grid item xs={12} md={3}>
              <ProductInfo />
            </Grid>
          </Grid>
          <Grid container direction='row' justify='center' alignItems='center'>
            <Grid item xs={12}>
              <RelatedProducts/>
            </Grid>
          </Grid>
        </Container>
        <Footer />
      </React.Fragment>
    </Loader>
  );
}


const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 50,
    marginBottom: 50,
  },
  pageTitle: {
    color: colors.darkerPrimary,
    textAlign: 'center'
  },
  extra: {
    height: '50px',
    position: 'relative'
  }
}))