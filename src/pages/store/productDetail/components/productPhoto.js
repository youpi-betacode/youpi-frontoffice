import React, { useEffect } from 'react';
import { Grid, Container, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useSelector } from 'react-redux'

import { colors } from '../../../../constants';


function ProductPhoto({photo}) {
  const classes = useStyles();

  return (
      <Grid container direction='row' alignItems='center' justify='center'>
        <Grid item xs={12} className={classes.imgWrapper}>
          <img src={photo} alt='YOUPI' className={classes.image}></img>
        </Grid>
      </Grid>
  )
}

export default ProductPhoto;

const useStyles = makeStyles(theme => ({
  imgWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  //Small Screen
  image: {
    width: '60%',
    height: '60%',
    //marginTop: 30
    //maxWidth: '300px',
    //minHeight: '300px'
  }
}))
