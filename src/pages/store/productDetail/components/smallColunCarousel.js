import React, { useState } from 'react';
import ItemCarousel from 'react-items-carousel';
import { Card } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import PhotoSmallCard from './photoSmallCard';
import { colors } from '../../../../constants';

function SmallColumnCarousel({productId}) {
   const classes = useStyles();
   const [activeItemIndex, setActivItemIndex] = useState(0);

   const changeActiveItem = (activeItemIndex) => {
      setActivItemIndex(activeItemIndex);
   }

   return (
      <ItemCarousel
         activeItemIndex={activeItemIndex}
         infiniteLoop={true}
         gutter={10}
         activePosition={'center'}
         chevronWidth={60}
         disableSwipe={false}
         alwaysShowChevrons={false}
         numberOfCards={1}
         slidesToScroll={1}
         outsideChevron={true}
         showSlither={false}
         firstAndLastGutter={false}
         requestToChangeActive={value => changeActiveItem(value)}
      >
         <PhotoSmallCard image={`/images/store/youpiis/${productId}/0.jpg`} />
         <PhotoSmallCard image={`/images/store/youpiis/${productId}/1.jpg`} />
         <PhotoSmallCard image={`/images/store/youpiis/${productId}/2.jpg`} />
      </ItemCarousel>
   );
}

export default SmallColumnCarousel;


const useStyles = makeStyles(theme => ({

}))