import React, { useEffect } from 'react';
import { Grid, Container } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";

import { colors } from '../../../constants';
import PhotoColumn from './components/photoColumn';
import RelatedProducts from './components/RelatedProducts';
import ProductPhoto from './components/productPhoto';
import ProductInfo from './components/productInfo';
import Footer from '../../../common/footer/Footer';
import Loader from '../../../common/loading/Loading';
import SmallProductDetail from './components/smallProductDetail'
import { getProduct, getRelatedProducts } from '../../../redux/actions/ProductActions';

function ProductDetail(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const theme = useTheme();
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'));
  const productId = props.match.params.id;
  const product = useSelector(state => state.product.product);

  useEffect(() => {
    dispatch(getProduct(productId))
  }, [productId])

  useEffect(()=>{
    if(product !== null){
      dispatch(getRelatedProducts(product.reference))
    }
  }, [product])

  if (isSmallDevice) {
    return (
      <SmallProductDetail props={props}/>
    )
  }

  return (
    <Loader loading='product'>
      <React.Fragment>
        <Container className={classes.container}>
          <Grid container direction='row' justify='center' alignItems='center'>
            <Grid item xs={12} md={2}>
              {
                product &&
                product.category === "YOUPi!" &&
                <PhotoColumn photo1={`/images/store/youpiis/${productId}/0.jpg`}
                             photo2={`/images/store/youpiis/${productId}/1.jpg`}
                             photo3={`/images/store/youpiis/${productId}/2.jpg`}
                />
              }
            </Grid>
            <Grid item xs={12} md={7} style={{ marginTop: 30, marginBottom: 30 }}>
              {
                product &&
                product.category === "YOUPi!" &&
                <ProductPhoto photo={`/images/store/youpiis/${productId}/0.gif`} />
              }
              {
                product &&
                (product.category === "VELAS" || product.category === "AMBIENTADORES") &&
                <ProductPhoto photo={`/images/store/fragrances/${productId}/0.jpg`}/>
              }
            </Grid>
            <Grid item xs={12} md={3}>
              <ProductInfo />
            </Grid>
          </Grid>
          <Grid container direction='row' justify='center' alignItems='center'>
            <Grid item xs={12}>
              <RelatedProducts />
            </Grid>
          </Grid>
        </Container>
        <Footer />
      </React.Fragment>
    </Loader>
  )
}

export default ProductDetail;

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 50,
    marginBottom: 50,
  },
  pageTitle: {
    color: colors.darkerPrimary,
    textAlign: 'center'
  },
  extra: {
    height: '50px',
    position: 'relative'
  }
}))
