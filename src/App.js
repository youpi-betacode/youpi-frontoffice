// The basics
import React, { useEffect } from 'react';
import { withRouter } from 'react-router';
// import Routes from './navigation/routes';
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import theme from "./theme";
import { CssBaseline, makeStyles } from "@material-ui/core";
import { SnackbarProvider } from "notistack";
import Notifier from "./common/Notifier";
import Navbar from "./common/navbar/Navbar";
import { colors } from "./constants";
import Routes from './navigation/routes';
import axios from 'axios';
import { useDispatch, useSelector } from "react-redux";
import { push } from "connected-react-router";
import { enqueueSnackbar } from "./redux/actions/NotificationsActions";
import { loadShoppingCart } from "./redux/actions/ShoppingCartActions";

function App() {
   const classes = useStyles();
   const dispatch = useDispatch();
   const router = useSelector(state => state.router.location.pathname);

   // Load shopping cart
   dispatch(loadShoppingCart());

   // Interceptors to detect 401 requests an redirect to loginReducer page
   axios.interceptors.response.use((response) => {
      // Do something with response data
      return response;
   }, (error) => {
      // Do something with response error

      // You can even test for a response code
      // and try a new request before rejecting the promise
      if (error.response.status === 401) {
         const requestConfig = error.config;
         localStorage.removeItem('token');
         const notification = {
            message: "token-expired",
            options: {
               variant: 'warning',
               anchorOrigin: {
                  vertical: 'top',
                  horizontal: 'right',
               },
               autoHideDuration: 5000
            },
         };
         const snackbar = enqueueSnackbar(
            notification,
            new Date().toString()
         );
         if (!router.startsWith("/user/login")) {
            dispatch(snackbar);
            dispatch(push(`/user/login`))
         }

      }
      return Promise.reject(error);
   });


   useEffect(() => {
      setTimeout(() => {
         // Disable loadinng on load
         document.getElementById("loader").style.display = 'none';
         document.getElementById("root").style.display = "inherit"
      }, 300)

   }, [])

   return (
      <div id="app">
         <MuiThemeProvider theme={theme}>
            <CssBaseline />
            <SnackbarProvider
               maxSnack={7}
               classes={{
                  variantError: classes.error,
                  variantSuccess: classes.success
               }}
            >
               <Notifier />
               <Navbar />
               <div id="content" style={{ marginTop: 30 }}>
                  <Routes />
                  {/* <AuthRouter history={history} /> */}
               </div>
               {/* <Footer /> */}
            </SnackbarProvider>
         </MuiThemeProvider>
      </div>
   );
}

export default withRouter(App);


const useStyles = makeStyles(theme => ({
   error: {
      backgroundColor: colors.errorPink
   },
   success: {
      backgroundColor: colors.successBlue
   }
}))
